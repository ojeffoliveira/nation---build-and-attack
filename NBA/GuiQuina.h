#pragma once
class GuiQuina
{
public:
	enum vertice {
		SUPESQ,
		SUPDIR,
		INFDIR,
		INFESQ,
	};
private:
	enum horiz{
		DIR,
		ESQ,
	};
	enum vert {
		INF,
		SUP,
	};
	sf::VertexArray m_pontos{ sf::LineStrip };
	int m_num_pontos;
	sf::Vector2f m_raio;
	sf::Vector2f m_corte;
	sf::Vector2f m_pos;
	float m_borda;
	
	sf::Color m_cor;
	sf::Color m_corDaBorda;

	vertice m_vert;
	horiz m_x;
	vert m_y;

protected:

	//Os updates do GuiQuina s�o diferentes do que estamos acostumados no GuiContainer
	//Eles recebem os valores e imediatamente atualizam a parte gr�fica de acordo.
	//O guiQuina deve ser entendido como um sf::RectangleShape ou um sf::CircleShape e 
	//n�o como algo similar ao GuiContainer
	
	//UPDATERS
	void update();
	void update(const sf::Vector2f& raio, const sf::Vector2f& corte, const sf::Vector2f& pos, const float& borda,
		const sf::Color& cor = sf::Color::Black, const sf::Color& corDaBorda = sf::Color::White);
	void update(vertice vert, const sf::Vector2f& raio, const sf::Vector2f& corte, const sf::Vector2f& pos, const float& borda,
		const sf::Color& cor = sf::Color::Black, const sf::Color& corDaBorda = sf::Color::White) {
		m_vert = vert;

		switch (m_vert) {
		case vertice::SUPESQ:
			m_x = horiz::ESQ;
			m_y = vert::SUP;
			break;
		case vertice::SUPDIR:
			m_x = horiz::DIR;
			m_y = vert::SUP;
			break;
		case vertice::INFDIR:
			m_x = horiz::DIR;
			m_y = vert::INF;
			break;
		case vertice::INFESQ:
			m_x = horiz::ESQ;
			m_y = vert::INF;
			break;
		}


		update(raio, corte, pos, borda, cor, corDaBorda);

	}


public:
				//SETTERS
	void setTamanho(const sf::Vector2f& raio, const sf::Vector2f& corte) { m_raio = raio; m_corte = corte; }
	void setBorda(float borda) { m_borda = borda; }
	void setPos(const sf::Vector2f& pos) { m_pos = pos; }
	void setCor(const sf::Color& cor, const sf::Color& corDaBorda) { m_cor = cor; m_corDaBorda = corDaBorda; }
	
				//GETTERS
	sf::Vector2f getRaio() { return m_raio; }
	sf::Vector2f getCorte() { return m_corte; }
	sf::Vector2f getPos() { return m_pos; }
	vertice getVertice() { return m_vert; }
	float getBorda() { return m_borda; }
	sf::Color getCor() { return m_cor; }
	sf::Color getCorBorda() { return m_corDaBorda; }
	sf::VertexArray& getPontos() { return m_pontos; }


			//CONSTRUTORES
	GuiQuina(vertice vert, const sf::Vector2f &raio, const sf::Vector2f &corte, const sf::Vector2f &pos,
			float borda, const sf::Color &cor, const sf::Color &corDaBorda): m_num_pontos(0), m_vert(vert)
	{

		switch (m_vert) {
		case vertice::SUPESQ:
			m_x = horiz::ESQ;
			m_y = vert::SUP;
			break;
		case vertice::SUPDIR:
			m_x = horiz::DIR;
			m_y = vert::SUP;
			break;
		case vertice::INFDIR:
			m_x = horiz::DIR;
			m_y = vert::INF;
			break;
		case vertice::INFESQ:
			m_x = horiz::ESQ;
			m_y = vert::INF;
			break;
		}

		update(raio, corte, pos, borda, cor, corDaBorda);

	}

	GuiQuina(vertice vert) :m_vert(vert) {
		m_raio = sf::Vector2f(0, 0);
		m_pos = sf::Vector2f(0, 0);
		m_corte = sf::Vector2f(0, 0);
		m_borda = 0;
		m_cor = sf::Color::Red;
		m_corDaBorda = sf::Color::Blue;
		m_num_pontos = 0;

		switch (m_vert) {
			case vertice::SUPESQ:
				m_x = horiz::ESQ;
				m_y = vert::SUP;
				break;
			case vertice::SUPDIR:
				m_x = horiz::DIR;
				m_y = vert::SUP;
				break;
			case vertice::INFDIR:
				m_x = horiz::DIR;
				m_y = vert::INF;
				break;
			case vertice::INFESQ:
				m_x = horiz::ESQ;
				m_y = vert::INF;
				break;
		}
	}
	
	GuiQuina();
	~GuiQuina();
	
	friend class GuiContainer;

};

