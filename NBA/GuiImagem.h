//////////////////////////////////////////////////////////////
//
//  Em d�vida se uso isso ou se uso Sprite de forma direta! 
//	
//
//
//////////////////////////////////////////////////////////////
#pragma once
#ifndef GUIIMAGEM_H
#define GUIIMAGEM_H

#include <SFML/Graphics.hpp>
#include "GuiContainer.h"
#include "GuiComponente.h"
#include <math.h>

class GuiImagem : public GuiComponente
{

	
	sf::Texture m_Texture;
	sf::Image m_imagem;
	float m_textureInicioX;
	float m_textureInicioY;
	float m_textureFimX;
	float m_textureFimY;
	std::vector<sf::CircleShape> m_quina;

private:
	//Pelo visto essas fun��es deveriam ajeitar as quinas. O que vou fazer � 
	//Dar um override no criarQuinas tendo em vista que ele n�o vai me servir de nada
	//e/ou criar um novo update para quinas no GuiQuinas que recebe a imagem e trata
	//adequadamente

	/*virtual sf::VertexArray QuinaSuperiorDireita(float x, float y) override final;
	virtual sf::VertexArray QuinaSuperiorEsquerda(float x, float y) override final;
	virtual sf::VertexArray QuinaInferiorEsquerda(float x, float y) override final;
	virtual sf::VertexArray QuinaInferiorDireita(float x, float y) override final;
	*///Fun��o recebe um ponto inicial e final de uma linha horizontal. Deve
	//Ent�o pintar os pixels da tela de acordo com as cores dos pixels da m_imagem 
	//carregada. N�o implementada.
	void pintarPixels(float x1, float x2, float y);

public:

	void setTextureLimites();
	virtual void criarComponente();
	
	//virtual void posicionarComponente();

	GuiImagem();
	GuiImagem(std::string filename, GuiContainer *pai);
	GuiImagem(GuiContainer *pai, std::string filename, sf::Color corDaBorda,
		float tamanhoX, float tamanhoY, float quina, float borda);
	GuiImagem(std::string filename);
	GuiImagem(std::string filename, sf::Color corDaBorda,
		float tamanhoX, float tamanhoY, float quina, float borda);

	void teste() {
		;
	}



	~GuiImagem();
};
#endif // !GUIIMAGEM_H

