#pragma once
class GuiComBorda
{
protected:

	float m_bordaGrossura = 0;
	float m_raioDaQuina = 0;

	//Define a cor das bordas
	sf::Color m_corDaBorda = sf::Color::Transparent;

	//Define a cor de todo o container
	sf::Color m_cor = sf::Color::Transparent;

	//Esses vectors guardam os retangulos e circulos que definem o
	//Container
	std::vector<sf::RectangleShape> m_retangulos;
	std::vector<sf::CircleShape> m_quinas;
	
	GuiComBorda(sf::Color cor, sf::Color corDaBorda, float borda, float quina)
		:m_cor(cor), m_corDaBorda(corDaBorda), m_bordaGrossura(borda), m_raioDaQuina(quina)
	{;}
	GuiComBorda();
	~GuiComBorda();
public:
	void setColor(sf::Color cor, sf::Color corDaBorda);
};

