#include "stdafx.h"
#include "GuiDesign.h"

// TODO: inserir instru��o de retorno aqui

void GuiDesign::updateDesignReal(const GuiDesign& d)
{
	if (this == &d)
		return;

	m_design_nome = "DESIGN_REAL";

	if (d.m_tamanho != NULL)
		m_tamanho = new sf::Vector2f(*d.m_tamanho);
	if (d.m_quinaSuperior != NULL)
		m_quinaSuperior = new float(*d.m_quinaSuperior);
	if (d.m_quinaDireita != NULL)
		m_quinaDireita = new float(*d.m_quinaDireita);
	if (d.m_quinaInferior != NULL)
		m_quinaInferior = new float(*d.m_quinaInferior);
	if (d.m_quinaEsquerda != NULL)
		m_quinaEsquerda = new float(*d.m_quinaEsquerda);
	if (d.m_bordaGrossura != NULL)
		m_bordaGrossura = new float(*d.m_bordaGrossura);
	if (d.m_cor != NULL)
		m_cor = new sf::Color(*d.m_cor);
	if (d.m_corDaBorda != NULL)
		m_corDaBorda = new sf::Color(*d.m_corDaBorda);
	if (d.m_transparencia != NULL)
		m_transparencia = new unsigned char(*d.m_transparencia);
	if (d.m_espacoEntreMembros != NULL)
		m_espacoEntreMembros = new float(*d.m_espacoEntreMembros);
	if (d.m_margemVertical != NULL)
		m_margemVertical = new float(*d.m_margemVertical);
	if (d.m_margemHorizontal != NULL)
		m_margemHorizontal = new float(*d.m_margemHorizontal);
}

GuiDesign& GuiDesign::operator=(const GuiDesign& d)
{
	if (this == &d)
		return *this;
	
	m_design_nome = d.m_design_nome;
	
	if (d.m_tamanho != NULL)
		m_tamanho = new sf::Vector2f(*d.m_tamanho);
	if (d.m_quinaSuperior != NULL)
		m_quinaSuperior = new float(*d.m_quinaSuperior);
	if (d.m_quinaDireita!= NULL)
		m_quinaDireita = new float(*d.m_quinaDireita);
	if (d.m_quinaInferior!= NULL)
		m_quinaInferior = new float(*d.m_quinaInferior);
	if (d.m_quinaEsquerda != NULL)
		m_quinaEsquerda = new float(*d.m_quinaEsquerda);
	if (d.m_bordaGrossura != NULL)
		m_bordaGrossura = new float(*d.m_bordaGrossura);
	if (d.m_cor != NULL)
		m_cor = new sf::Color(*d.m_cor);
	if (d.m_corDaBorda!= NULL)
		m_corDaBorda = new sf::Color(*d.m_corDaBorda);
	if (d.m_transparencia != NULL)
		m_transparencia = new unsigned char(*d.m_transparencia);
	if (d.m_espacoEntreMembros != NULL)
		m_espacoEntreMembros = new float(*d.m_espacoEntreMembros);
	if (d.m_margemVertical!= NULL)
		m_margemVertical = new float(*d.m_margemVertical);
	if (d.m_margemHorizontal != NULL)
		m_margemHorizontal = new float(*d.m_margemHorizontal);
}

GuiDesign::GuiDesign(const std::string& nome, const sf::Vector2f& Tamanho, const float& QuinaSup,
	const float& QuinaDir, const float& QuinaInf, const float& QuinaEsq, const float& borda, 
	const sf::Color& cor, const sf::Color& corDaBorda, const float& espacoEntreMembros, 
	const float& margemVert, const float& margemHoriz): m_design_nome(nome), m_tamanho(new sf::Vector2f(Tamanho)),
	m_quinaSuperior(new float(QuinaSup)), m_quinaDireita(new float(QuinaDir)), m_quinaInferior(new float(QuinaInf)),
	m_quinaEsquerda(new float(QuinaEsq)), m_bordaGrossura(new float(borda)),m_cor(new sf::Color(cor)), 
	m_corDaBorda(new sf::Color(corDaBorda)), m_espacoEntreMembros(new float(espacoEntreMembros)), 
	m_margemVertical(new float(margemVert)), m_margemHorizontal(new float(margemHoriz))
{
}

GuiDesign::GuiDesign(const std::string& nome, const sf::Vector2f& Tamanho, const float& quinas, 
	const float& borda, const sf::Color& cor, const sf::Color& corDaBorda, const float& espacoEntreMembros, 
	const float& margemVert, const float& margemHoriz):m_design_nome(nome), m_tamanho(new sf::Vector2f(Tamanho)),
	m_quinaSuperior(new float(quinas)), m_quinaDireita(new float(quinas)), m_quinaInferior(new float(quinas)),
	m_quinaEsquerda(new float(quinas)), m_bordaGrossura(new float(borda)), m_cor(new sf::Color(cor)),
	m_corDaBorda(new sf::Color(corDaBorda)), m_espacoEntreMembros(new float(espacoEntreMembros)),
	m_margemVertical(new float(margemVert)), m_margemHorizontal(new float(margemHoriz))
{
}

GuiDesign::GuiDesign()
{
}


GuiDesign::~GuiDesign()
{
}
