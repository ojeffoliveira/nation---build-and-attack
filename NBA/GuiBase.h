#pragma once
#ifndef GUIBASE_H
#define GUIBASE_H
#include "SFML/Graphics.hpp"

class GuiContainer;

class GuiBase
{

public:
	enum ReacaoMedidas{
		FIXA,
		FLEXIVEL_ABSOLUTO,
		FLEXIVEL_PERCENTUAL,
		PREENCHER,
		INFLADO,
		MAX_REACAO,
	};

protected:
	
	float m_bordaGrossura = 0;
	float m_raioDaQuina = 0;
	
	float m_quinaSuperiorEsquerda = 0;
	float m_quinaSuperiorDireita = 0;
	float m_quinaInferiorEsquerda = 0;
	float m_quinaInferiorDireita = 0;

	//Define a cor das bordas
	sf::Color m_corDaBorda = sf::Color::Transparent;

	//Define a cor de todo o container
	sf::Color m_cor = sf::Color::Transparent;

	unsigned char m_transparencia = 255;
	unsigned char m_transparenciaBorda = 255;
	//Esses vectors guardam os retangulos e circulos que definem o
	//Container
	std::vector<sf::RectangleShape> m_retangulos;
	std::vector<sf::VertexArray> m_teste{ sf::LineStrip };
	std::vector<sf::CircleShape> m_quinas;
	GuiContainer* m_pai;

	float m_PosicaoX = 0;
	float m_PosicaoY = 0;
	float m_TamanhoX = 0;
	float m_TamanhoY = 0;
	ReacaoMedidas m_reacaoMedidas = ReacaoMedidas::FIXA;

public:
	void setCor();
	sf::Color addColors(const sf::Color &bg, const sf::Color &emcima);

	//VIRTUAL FUNCTIONS - PRECISAM SER OVERRIDED

	virtual void setTransparencia(const unsigned char &trans, const unsigned char &tranBorda = 255) { ; }
	virtual void drawMe(sf::RenderWindow *myWindow) const { 
		; }
	
	virtual void updateMe() { ; }
	//VIRTUAL FUNCTIONS - ALGUMS FILHOS PRECISAM OVERRIDE
	virtual void updatePosicao(int index) { ; }
	virtual void PosicionarRet();
	virtual void criarPartes();
	virtual void updateMembers() { ; }
	virtual bool estaDentro(sf::Vector2i posicao);
	
	void addPai(GuiContainer *pai) { m_pai = pai; }
	void setColor(sf::Color cor, sf::Color corDaBorda);
	void setX(float x) { m_PosicaoX = x; }
	void setY(float y) { m_PosicaoY = y; }
	void setTamanhoX(float tamanhoX) { m_TamanhoX = tamanhoX; }
	void setTamanhoY(float tamanhoY) { m_TamanhoY = tamanhoY; }	
	float getX() const { return m_PosicaoX; }
	float getY() const { return m_PosicaoY; }
	float getTamanhoX() const { return m_TamanhoX; }
	float getTamanhoY() const { return m_TamanhoY; }
	//void resize(float x, float y, float TamanhoX, float TamanhoY);
	
	float distanceFrom(sf::Vector2f a, sf::Vector2f b);
	
	GuiBase(float x, float y, float tamanhoX, float tamanhoY);
	GuiBase(sf::Color cor, sf::Color corDaBorda, float x, float y, float TamanhoX, float TamanhoY,
			float borda, float quina);
	GuiBase();
	~GuiBase();
};

#endif
