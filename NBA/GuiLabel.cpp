#include "stdafx.h"
#include "GuiLabel.h"
#include <SFML/Graphics/Text.hpp>


void GuiLabel::updatePosicao(){
	m_label.setPosition(m_PosicaoX, m_PosicaoY);
}

void GuiLabel::drawMe(sf::RenderWindow *myWindow) {
	GuiContainer::drawMe(myWindow);
	myWindow->draw(m_label);
}

GuiLabel::GuiLabel()
{
}
GuiLabel::GuiLabel(const sf::String &texto, const sf::Color &cor, const sf::Font &fonte, const unsigned int &TamanhoY)
	:  GuiComponente(sf::Color::White, sf::Color::White,GuiContainer::Ordenacao::ORDEM_SOBREPOSTA,
					0,TamanhoY,0,0)
{
	m_label.setString(texto);
	m_label.setFont(fonte);
	m_label.setCharacterSize(TamanhoY);
	m_label.setFillColor(cor);
	setTamanhoX(m_label.getLocalBounds().width);
	m_label.setPosition(m_PosicaoX, m_PosicaoY);
	
}


GuiLabel::~GuiLabel()
{
}
