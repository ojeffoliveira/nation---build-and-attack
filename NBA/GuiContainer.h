//////////////////////////////////////////////////////////////
//
// Container � o building block b�sico aqui. Eles podem ser
// conter seus membros de forma vertical, horizontal ou sobreposta.
//	O container principal contem pelo menos dois containers sobrepostos
//	o background e o da frente. Pop ups surgem por adc mais um container
//	sobreposto.
//
//////////////////////////////////////////////////////////////

#pragma once
#ifndef GUICONTAINER_H
#define GUICONTAINER_H
#include <vector>
#include "SFML/System.hpp"
#include "SFML/Graphics.hpp"
#include "GuiQuina.h"
//#include "GuiImagem.h"
//#include "GuiBotao.h"
//#include "GuiLabel.h"
//
#include "GuiDesign.h"
class GuiImagem;
class GuiLabel;
class GuiBotao;
class GuiInteratividade;

enum ReacoesMedidas {
	FIXA,
	//DELETEI O FLEXIVEL ABSOLUTO // Um ou outro n�o faz diferen�a pro c�digo, somente no construtor
	FLEXIVEL_PERCENTUAL,	// No construtor caso seja o absoluto ele calcula o valor percentual e coloca l� 
	PEGAR_DO_PAI,				
	PEGAR_DOS_FILHOS,
	//
	MAX_REACAO,
};



class GuiContainer
{
	//Define como o container ordena seu membros, de forma
	// vertical, horizontal ou sobreposta.
public:
	//VARIAVEIS RELACIONADAS A MEMBROS
	enum Ordenacao {
		ORDEM_SOBREPOSTA,
		ORDEM_VERTICAL,
		ORDEM_HORIZONTAL,
	};
	enum AlinharHorizon {
		ALINHAR_DIREITA,
		ALINHAR_ESQUERDA,
		ALINHAR_CENTRAL_HORIZONTAL,
	};
	enum AlinharVert {
		ALINHAR_ACIMA,
		ALINHAR_ABAIXO,
		ALINHAR_CENTRAL_VERTICAL,
	};


protected:
	std::string m_nome{ "" };

	std::vector<GuiInteratividade*> m_interacao;
	GuiDesign* m_design_atual;

	//Deve ser deletado pelos componentes ou 
	//usado como os membros
	std::vector<GuiDesign*> m_meus_designs;

	//Aqui ficam os valores que realmente demonstram as caracter�sticas
	//reais do container. Durante uma interatividade esses valores podem
	//ser diferentes dos valores em m_design_atual
	//Al�m disso, o m_Tamanho pode ser diferene do tamanho de fato
	//por se tratar mudan�as on place
	GuiDesign m_design_real;

	sf::Vector2f m_Posicao_Membro;
	//Esses valores v�o representar os tamanhos
	//reais deles, mesmo quando on place
	//Os valores no guiDesign_real representam
	//Os valores de membros
	sf::Vector2f m_Posicao;
	sf::Vector2f m_Tamanho;
	float m_quinaSuperior = 0;
	float m_quinaDireita = 0;
	float m_quinaEsquerda = 0;
	float m_quinaInferior = 0;

	ReacoesMedidas m_ReacaoTamanhoX = ReacoesMedidas::FIXA;
	ReacoesMedidas m_ReacaoTamanhoY= ReacoesMedidas::FIXA;
	ReacoesMedidas m_ReacaoQuinaSup= ReacoesMedidas::FIXA;
	ReacoesMedidas m_ReacaoQuinaDir= ReacoesMedidas::FIXA;
	ReacoesMedidas m_ReacaoQuinaInf= ReacoesMedidas::FIXA;
	ReacoesMedidas m_ReacaoQuinaEsq= ReacoesMedidas::FIXA;
	//MedidaFlexivel m_flexivel;

	//Esses vectors guardam os retangulos e quinas que definem o
	//Container
	std::vector<sf::RectangleShape> m_retangulos;
	std::vector<sf::RectangleShape> m_bordas;
	std::vector<GuiQuina> m_quinas;

	GuiContainer* m_pai;

					//VARIAVEIS RELACIONADAS A MEMBROS//

	//Usado para facilitar a adi��o de membros
	//Soma dos membros de tamanho fixo
	float m_somaMembrosHorizontal = 0;
	float m_somaMembrosVertical = 0;
	//Fazer um somaMembrosFixo_FlexivelHorizontal/Vert
	//numeroMembrosHerdeirosHorizontal/Vert

	AlinharVert m_alinhamentoVertical= ALINHAR_ABAIXO;
	AlinharHorizon m_alinhamentoHorizontal = ALINHAR_CENTRAL_HORIZONTAL;
	Ordenacao m_ordem = ORDEM_SOBREPOSTA;
	int m_numeroDeMembros = 0;
	std::vector<GuiContainer*> m_membros;
	
	
	//////////////////////////////////////
	//A ser deletadas:
	float m_PosicaoX = 0;
	float m_PosicaoY = 0;
	//Depracated preciso atualizar GuiImagem e 
	//fun��es do Container  - incluindo operator = - para apagar ela. 
	float m_raioDaQuina = 0;
	bool m_TamanhoXConst = true;
	bool m_TamanhoYConst = true;

	float m_espacoEntreMembros = 0;
	float m_margemVertical = 0;
	float m_margemHorizontal = 0;
	
	float m_bordaGrossura = 0;
	sf::Color m_corDaBorda = sf::Color::Transparent;
	sf::Color m_cor = sf::Color::Transparent;
	unsigned char m_transparencia = 255;

	float m_TamanhoX = 0;
	float m_TamanhoY = 0;

private:
	
		virtual void updateTransparencia(unsigned char acumulado);
public:
	//FUN��ES DE COMPONENTES

	virtual void appendText(sf::Uint32 texto) { ; } //Funcionar com GuiLabel, GuiTexto, GuiTextField etc
	virtual void flibBool() { ; }

	//ADDING MEMBERS
	void FinalizarAdd(int indexNovo);
	void addImagem(const GuiImagem& NovoMembro);
	void addLabel(const GuiLabel& NovoMembro);
	void addBotao(const GuiBotao& NovoMembro);
	void addContainer(const GuiContainer& NovoMembro);
	void addPai(GuiContainer* pai) { this->m_pai = pai; }

	virtual void updateDesign(const std::string& designDestino="");

	virtual void updateTransparencia();
	virtual void setDesignAtual(const std::string& nome);
	void updateMembers();
	void updatePosicaoDoMembro(int index);
	virtual void updateCor();
	virtual void drawMe(sf::RenderWindow* myWindow);
	virtual void finalizarUpdate();
	virtual void updatePosicao();

	virtual void updateTamanho();

	//Preciso atualizar...ou n�o. 
	virtual void criarPartes();
	void criarQuinas();
	
	//////////////////////////////////////////////////////////////////////////


	virtual bool estaDentro(sf::Vector2i posicao);
		
						//GETTERS AND SETTERS

	
	sf::Color addColors(const sf::Color &bg, const sf::Color &emcima);
	void setColor(sf::Color cor, sf::Color corDaBorda);
	sf::Color getCor() const { return m_cor; }
	sf::Color getCorBorda() const { return m_corDaBorda; }

	void setX(float x) { m_PosicaoX = x; }
	void setY(float y) { m_PosicaoY = y; }
	float getX() const { return m_PosicaoX; }
	float getY() const { return m_PosicaoY; }

	void setTamanhoX(float tamanhoX) { m_TamanhoX = tamanhoX; }
	void setTamanhoY(float tamanhoY) { m_TamanhoY = tamanhoY; }
	float getTamanhoX() const { return m_TamanhoX; }
	float getTamanhoY() const { return m_TamanhoY; }

	GuiDesign* getDesignAtual() { return m_design_atual; }
	std::vector<GuiContainer*> getMembros() { return m_membros; }
	//void resize(float x, float y, float TamanhoX, float TamanhoY);

	float distanceFrom(sf::Vector2f a, sf::Vector2f b);

	void setTransparencia(const unsigned char &trans)
	{
		m_transparencia = trans;
	}
	

						//UPDATING

	void updateMe();
	void updateMyTamanho();
						
						//PARTE DA INICIALIZA��O

	
						//CONSTRUTORES
	
	GuiContainer(const GuiContainer& container);
	GuiContainer* operator[](const std::string &name);
	GuiContainer& operator=(const GuiContainer& container);

	//CONSTRUTOR NAO IMPLEMENTADO QUE SER� UM DOS �NICOS NO FUTURO
	GuiContainer(Ordenacao ordem, std::vector<GuiDesign*> designs);
	
	GuiContainer(sf::Color cor, sf::Color corDaBorda, Ordenacao ordem, 
				float TamanhoX, float TamanhoY, float quina = 0, float borda = 0);

	GuiContainer(sf::Color cor, sf::Color corDaBorda, Ordenacao ordem,
				float TamanhoX, float TamanhoY, float borda, 
				float quinaSup, float quinaDir, float quinaInf,
				float quinaEsq);

	void setVarMembros(float margemVertical, float margemHorizontal, 
						AlinharHorizon alinhamentoHorizontal, AlinharVert alinhamentoVertical,
						float espacoentre);
	
	void setVarMedidas(const ReacoesMedidas &tamanhox, const ReacoesMedidas& tamanhoy, const ReacoesMedidas &quinaSup = ReacoesMedidas::FIXA,
						const ReacoesMedidas &quinaDir = ReacoesMedidas::FIXA,
						const ReacoesMedidas &quinaEsq = ReacoesMedidas::FIXA,
						const ReacoesMedidas &quinaInf = ReacoesMedidas::FIXA);
	GuiContainer();
	~GuiContainer();
};

#endif