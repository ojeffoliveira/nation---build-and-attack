//////////////////////////////////////////////////////////////
//
// NEXT UP
//	Criar vers�es para os componentes das fun��es criarpartes
//  posicionarRet draw(?) 
//
//////////////////////////////////////////////////////////////
#include "stdafx.h"
#include "GuiComponente.h"


GuiComponente::GuiComponente()
{
}

GuiComponente::GuiComponente(GuiContainer *pai, float tamanhoX, float tamanhoY)
	: GuiContainer(sf::Color::Transparent, sf::Color::Transparent, GuiContainer::Ordenacao::ORDEM_HORIZONTAL,
		tamanhoX, tamanhoY, 0.0, 0.0)
{
	addPai(pai);
}


GuiComponente::GuiComponente(GuiContainer *pai, sf::Color cor, sf::Color corDaBorda, GuiContainer::Ordenacao ordem, 
	float tamanhoX, float tamanhoY, float borda, float quina)
	: GuiContainer(cor, corDaBorda, ordem, tamanhoX, tamanhoY, quina, borda)
{
	addPai(pai);
}

GuiComponente::GuiComponente(GuiContainer *pai, sf::Color cor, sf::Color corDaBorda, GuiContainer::Ordenacao ordem,
	float tamanhoX, float tamanhoY, float borda, float quinaSup, float quinaDir,
	float quinaEsq, float quinaInf)
	: GuiContainer(cor, corDaBorda, ordem, tamanhoX, tamanhoY, borda, quinaSup, quinaDir, quinaInf, quinaEsq)
{
	addPai(pai);
}


GuiComponente::GuiComponente(GuiContainer *pai, sf::Color cor, sf::Color corDaBorda, 
	float tamanhoX, float tamanhoY, float borda, float quina)
	: GuiContainer(cor, corDaBorda, GuiContainer::Ordenacao::ORDEM_HORIZONTAL, tamanhoX, tamanhoY, quina, borda)
{
	addPai(pai);
}


GuiComponente::GuiComponente(float tamanhoX, float tamanhoY)
	: GuiContainer(sf::Color::Transparent, sf::Color::Transparent, GuiContainer::Ordenacao::ORDEM_HORIZONTAL, 
					tamanhoX, tamanhoY, 0.0, 0.0)
{

}

GuiComponente::GuiComponente(sf::Color cor, sf::Color corDaBorda, GuiContainer::Ordenacao ordem,
							float tamanhoX, float tamanhoY, float borda, float quina)
	: GuiContainer(cor, corDaBorda, ordem, tamanhoX, tamanhoY, quina, borda)
{

}

GuiComponente::GuiComponente(sf::Color cor, sf::Color corDaBorda,
							float tamanhoX, float tamanhoY, float borda, float quina)
	: GuiContainer(cor, corDaBorda, GuiContainer::Ordenacao::ORDEM_HORIZONTAL, tamanhoX, tamanhoY, quina, borda)
{

}

GuiComponente::GuiComponente(sf::Color cor, sf::Color corDaBorda, GuiContainer::Ordenacao ordem,
	float tamanhoX, float tamanhoY, float borda, float quinaSup, float quinaDir,
	float quinaEsq, float quinaInf)
	: GuiContainer(cor, corDaBorda, ordem, tamanhoX, tamanhoY, borda, quinaSup, quinaDir, quinaInf, quinaEsq)
{

}

GuiComponente::~GuiComponente()
{
}
