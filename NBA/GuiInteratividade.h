//////////////////////////////////////////////////////////////
//
// GuiInteratividade define as rea��es do programa frente a 
// intera��o do usu�rio. Cada inst�ncia desse objeto requer uma 
// causa, dentro de um dado Container ou componente e ent�o define
// quantos efeitos forem necess�rios. 
//
// Ele � adicionado atrav�s de uma fun��o no GuiTela mas mantido
// de fato dentro de cada GuiContainer. 
//
//////////////////////////////////////////////////////////////
#pragma once

#include <chrono>
class GuiContainer;
class GuiControl;
enum causa {
	SEMPRE_TRUE,					//

	SEM_INTERACAO_POR,				//

	CLICK_ESQUERDO,					//
	CLICK_DIREITO,					//
	CLICK_MEIO,						//

	PONTEIRO_MOVEU,					//
	PONTEIRO_SOBRE,					//
	PONTEIRO_NAO_SOBRE,				//

	MOUSE_SCROLL_UP,				//
	MOUSE_SCROLL_DOWN,				//

	CONTAINER_FORA_DO_PAI,			//Essas duas s�o ut�is para criar scrolls.
	CONTAINER_FORA_MARGENS_DO_PAI,  //

	TEXTO_ESCRITO,					//
	KEY_PRESSED,					//KEYS
	KEY_RELEASED,					//

	IF_TRUE_FUNCTION,				//
};

enum efeito {
	MUDAR_DESIGN,
	MUDAR_COR,
	MUDAR_COR_BORDA,
	MUDAR_TRANSPARENCIA,
	MUDAR_BORDA_GROSSURA,
	MUDAR_TAMANHO,
	MUDAR_POS,


	MUDAR_METADADOS, //Needs to be implemented through a function provided by the user which will receive the metadata in turn
	ADC_CONTAINER,
	ADC_COMPONENTE, //Incluir um pra cada?
	MUDAR_POS_EM_M_MEMBROS,

	DELETAR,
	MOVER_PARA,
	CLONAR_PARA,

	APPEND_TEXTO,
	FLIP_BOOL_VALUE,

	FUNCAO,
	FECHAR_JANELA,
	RESTABELECER_DESIGN,
	MUDAR_CURSOR, //Precisa de config
};
//Ser� adicionado diretamente em cada GuiContainer
class GuiInteratividade
{
public:
	

	
	

private:

	std::chrono::time_point<std::chrono::system_clock> m_inicio;
	int m_tempo_p_efeito;			//Se setado em 0 ir� ocorrer automaticamente
	int m_tempo_transcorrido = -1;	//O -1 indica que a interatividade est� inativa, n�o foi iniciada.
	


				//CAUSAS

	causa m_causa;

	GuiContainer* m_container;		//Manter nulo para causa em qualquer lugar
	GuiDesign* m_origem;
	
	GuiContainer* m_container_sec; 
	GuiDesign* m_design_secundario;

	int m_tempo_sem_interacao;
	sf::Event::KeyEvent m_tecla;

	bool (*m_funcao)(const sf::Event &evento, const GuiControl &control);

	
				//EFEITOS

	std::vector<efeito> m_efeitos;
	
	//Se todos as condi��es forem atendidas essa fun��o � chamada e se retornar true
	//a interatividade come�a de fato. Caso o usu�rio n�o necessite checar nada antes
	//ele pode apenas n�o prover fu���o alguma
	bool (*m_callmebefore)(const sf::Event &evento, const GuiControl& control);
	GuiContainer* m_container_afetado;
	GuiDesign* m_destino;
	

	GuiComponente* m_componente_p_adc;
	GuiContainer* m_container_p_adc;
	
	GuiContainer* m_mover_para_aqui;
	int m_pos_para_mover;
	GuiContainer* m_clonar_para_aqui;
	int m_pos_para_clonar;

	int m_mudar_pos_em_membros;


	float m_bordaGrossura;
	sf::Vector2f m_pos_desvio{0,0};
	bool m_on_place_pos = false;
	sf::Vector2f m_tam_desvio{0,0};
	bool m_on_place_tam = false;

	unsigned char m_transparencia_absoluta{ 0 };
	int m_desvio_transparencia{ 0 };
	sf::Color m_cor_absoluta;
	sf::Color m_corDaBorda_absoluta;
	
	//std::array<INT, 4> m_desvio_cor;
	//std::array<INT, 4> m_desvio_cor_borda;

	void (*m_callmeafter)(const GuiControl& control);
	void (*m_mudar_metadados)(const GuiContainer& container_afetado);
	void (*m_efeito_funcao)(const GuiContainer& container_afetado);

	GuiInteratividade* m_start_at_the_end;

	//Implementam os efeitos
	
	void efeito_mudarDesign();
	

public:
	
	bool esta_rodando() { return (m_tempo_transcorrido == -1 ? false: true ); }
	//Retorna true se a interatividade terminou para que caso sim ela seja retirada
	//da lista l� no gui control -- troque bool por void para que a checagem seja feita
	// com a var m_esta_rodando
	void rodarInteratividade(GuiControl& control);

	//Checa se as condi��es s�o verdadeiras, caso sejam a interatividade � iniciada.
	//� chamada caso a causa em quest�o aconte�a (usando o pollevent.(event)
	// retorna bool caso tenha sido iniciada para ser adc ao vector respectivo

	bool checarInicio(const sf::Event &evento, const GuiControl &control);

	
	
							//CONFIG PARA CAUSA
	void config_causa_semInteracao(int milisegundos) { m_tempo_sem_interacao = milisegundos; }
	void config_causa_keyPressed(const sf::Keyboard::Key &tecla);	//precisa converter dekeyboard key para event::keyevent
	void config_causa_keyReleased(const sf::Keyboard::Key& tecla);
	void config_causa_ifTrueFunction(bool (*funct)(const sf::Event &e, const GuiControl &c)) { m_funcao = funct; }				//Checar se est� certo a sintaxe


							//CONFIG PARA EFEITOS

	void config_efeito_mudarDesign(GuiDesign* design_destino) { m_destino = design_destino; }
	void config_efeito_mudarCor(const sf::Color& cor) { m_cor_absoluta = cor;}						//Faltam as cores relativas 
	void config_efeito_mudarCorBorda(const sf::Color corBorda) { m_corDaBorda_absoluta = corBorda;	}				//ou devo tiralas?
	void config_efeito_mudarTransparencia(const unsigned int trans) { m_transparencia_absoluta = trans;  }
	void config_efeito_mudarBordaGrossura(float bordaGrossura) { m_bordaGrossura = bordaGrossura; }
	void config_efeito_mudarTamanho(sf::Vector2f desviotamanho, bool on_place) { m_tam_desvio = desviotamanho; m_on_place_tam = on_place; }
	void config_efeito_mudarPosicao(sf::Vector2f desvioPos, bool on_place) { m_pos_desvio = desvioPos; m_on_place_pos = on_place; }
	void config_efeito_mudarMetadados(void (*mudarMetadados)(const GuiContainer& container_afetado)) { m_mudar_metadados = mudarMetadados; }
	void config_efeito_adcContainer(GuiContainer *adceste) { m_container_p_adc = adceste; }
	void config_efeito_adcComponente(GuiComponente* adceste) { m_componente_p_adc = adceste; }
	void config_efeito_mudarPosEmMembros(int pos) { ; }
	void config_efeito_moverPara(GuiContainer* aqui, int pos) { m_mover_para_aqui = aqui; m_pos_para_mover = pos; }
	void config_efeito_clonarPara(GuiContainer* aqui, int pos) { m_clonar_para_aqui = aqui; m_pos_para_clonar = pos; }
	void config_efeito_funcao(void (*func)(const GuiContainer& c)) { m_efeito_funcao = func; }

	
				//CONSTRUTORES
	
	//Um construtor simplificado para interatividade de troca de designs
	GuiInteratividade(causa porque, efeito oque, GuiContainer* m_container, GuiDesign* origem, GuiContainer* afetado,
						float TempoParaEfeito_milisegundos, GuiContainer* cont_secund = NULL, GuiDesign* design_secund = NULL);
	
	//O construtor completo para uma interatividade de um efeito que n�o envolve design
	GuiInteratividade(causa porque, efeito oque, GuiContainer* container, GuiContainer* afetado,	float TempoParaEfeito_milisegundos, 
		bool (*callMeBefore)(const sf::Event& evento, const GuiControl& control) = NULL,
		void (*callmeafter)(const GuiControl& control) = NULL, GuiInteratividade* start_at_the_end = NULL,
		GuiContainer* cont_secund = NULL);
	
	//O constrtuor completo para uma interatividade de um efeito
	GuiInteratividade(causa porque, efeito oque, GuiContainer* container, GuiDesign* origem, GuiContainer* afetado,
			float TempoParaEfeito_milisegundos, bool (*callMeBefore)(const sf::Event& evento, const GuiControl& control) = NULL,
			void (*callmeafter)(const GuiControl& control) = NULL, GuiInteratividade* start_at_the_end = NULL, 
			GuiContainer* cont_secund = NULL, GuiDesign* design_secund = NULL);

	GuiInteratividade(causa porque, std::vector<efeito> oques, GuiContainer* container, GuiDesign* origem, GuiContainer* afetado,
			float TempoParaEfeito_milisegundos, bool (*callMeBefore)(const sf::Event& evento, const GuiControl& control) = NULL,
			void (*callmeafter)(const GuiControl& control) = NULL, GuiInteratividade* start_at_the_end = NULL,
			GuiContainer* cont_secund = NULL, GuiDesign* design_secund = NULL);

	GuiInteratividade(causa porque, std::vector<efeito> oques, GuiContainer* container, GuiContainer* afetado,
		float TempoParaEfeito_milisegundos, bool (*callMeBefore)(const sf::Event& evento, const GuiControl& control) = NULL,
		void (*callmeafter)(const GuiControl& control) = NULL, GuiInteratividade* start_at_the_end = NULL,
		GuiContainer* cont_secund = NULL);


	GuiInteratividade();
	~GuiInteratividade();
};

