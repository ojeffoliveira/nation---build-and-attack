//////////////////////////////////////////////////////////////
//
// Componentes s�o items espec�ficos que possuem uma parte
//	gr�fica apresenta ao usu�rio. Incluem: caixa de texto
//	radio buttons, scrollers, labels, etc, textos, etc.
//	Todo componente precisa estar dentro de um container e 
//	tem sua posi��o lidada por ele. 
//
//////////////////////////////////////////////////////////////
#pragma once
#ifndef GUICOMPONENTE_H
#define GUICOMPONENTE_H

#include "GuiContainer.h"

class GuiComponente: public GuiContainer

{
	
public:
	//Esse papo de estado vai ser jogado no lixow
	enum Estado {
		NORMAL,
		HOVER, // O mouse est� sobre ele, alguns componentes tem esse estado e o anterior igual
		CLICADO, //Costuma ser inst�ntaneo mas em alguns casos � vis�vel - um bot�o que desce e sobe ao ser
		//clicado
		DESLIGADO, //Normalmente � um camada cinza e desliga toda a interatividade do componente
		SELECIONADO, //� diferente de focus pois mais de um pode estar selecionado. Acontece
		// por exemplo quando � preciso escolher mais de uma op��o para fazer algo. Nao usado na maior parte
		// das vezes
		ESTADO_MAX,
	};
protected:
	
	
	//Criar outra classe tipo GuiSelecionavel? Quando escrevi isso? N�o seria gui clicavel?
	Estado m_meuEstado = Estado::NORMAL;
	short m_tempoTSelecionado = 0;
	short m_tempoTClicado = 250;
	short m_tempoTHover = 500;
	float m_transicao = -1;

	//Acho que esse vou manter!
	bool m_estaSelecionado = false;
public:
	
	//Fun��es de componentes
	virtual void appendText(sf::Uint32 texto) { ; }

	//FUNCAO que cria a parte grafica espec�fica do componente
	virtual void criarComponente() { ; }
	virtual void posicionarComponente() { ; }

	//FUNCAO VIRTUAL que cada filho deve override para definir 
	//os efeitos da mudan�a de estado
	virtual void mudarEstado(GuiComponente::Estado estado) { ; }
	
	bool estaSelecionado() { return m_estaSelecionado; }
	void mudarSelecionado() {
		if (m_estaSelecionado)
			m_estaSelecionado = false;
		else
			m_estaSelecionado = true;
	}
	void ficarClicado() {
		if (m_estaSelecionado)
			return;
		
		m_estaSelecionado = true;
		//efeitoSelecionado();
	}
	

	
	

	//CONSTRUTORES SEM E COM PAI

	GuiComponente( GuiContainer *pai,sf::Color cor, sf::Color corDaBorda, GuiContainer::Ordenacao ordem,
					float tamanhoX, float tamanhoY, float borda, float quina);
	GuiComponente(GuiContainer *pai, sf::Color cor, sf::Color corDaBorda, GuiContainer::Ordenacao ordem,
					float tamanhoX, float tamanhoY, float borda, float quinaSup, float quinaDir,
					float quinaEsq, float quinaInf);
	GuiComponente( GuiContainer *pai,sf::Color cor, sf::Color corDaBorda, 
					float tamanhoX, float tamanhoY, float borda, float quina);
	GuiComponente(GuiContainer *pai, float tamanhoX, float tamanhoY);
	
	GuiComponente(sf::Color cor, sf::Color corDaBorda, GuiContainer::Ordenacao ordem,
					float tamanhoX, float tamanhoY, float borda, float quinaSup, float quinaDir,
					float quinaEsq, float quinaInf);
	GuiComponente(sf::Color cor, sf::Color corDaBorda, GuiContainer::Ordenacao ordem, 
					float tamanhoX, float tamanhoY, float borda, float quina);
	GuiComponente(sf::Color cor, sf::Color corDaBorda,
					float tamanhoX, float tamanhoY, float borda, float quina);

	GuiComponente(float tamanhoX, float tamanhoY);
	GuiComponente();
	~GuiComponente();
};

#endif
