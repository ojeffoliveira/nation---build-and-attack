#pragma once

class GuiContainer;

class GuiTransicao
{
public:
	enum causa {
		CLICK_ESQUERDO,
		CLICK_DIREITO,
		CLICK_MEIO,
		MOUSE_HOVER,
		MOUSE_NAO_HOVER,
		MOUSE_SCROLL_UP,
		MOUSE_SCROLL_DOWN,
				
		KEY_DOWN_ESC, //KEYS
		KEY_DOWN_SPACE,
		
		IF_TRUE_FUNCTION,

	};

	enum estilo {
		DESLIZA_P_ESQ,
		DESLIZA_P_DIR,
		DESLIZA_P_CIMA,
		DESLIZA_P_BAIXO,
		TRANSPARENCIA,
		estilo_MAX,
	};


	enum efeito {

	};


private:

	int m_Tela_Origem;
	int m_Tela_Destino;
	causa m_causa_Transicao;
	GuiContainer* m_container_clique_hover; //Manter nulo para clique em qualquer lugar
	int m_unidadesScroll;
	int m_Tempo_Transicao;
	//func* m_func;
	
	bool m_estaRodando = false;
	int m_tempo_Transcorrido= -1;
	estilo m_estilo;

public:
	GuiTransicao();
	~GuiTransicao();
};

