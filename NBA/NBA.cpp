//////////////////////////////////////////////////////////////
//
// NEXT UUUUUUP
// 
// OK Corrigir problemas no GuiContainer em algumas config			~~~~~~
// de quinas.  
// FALTAM TESTAR TODAS ^ Porem, acredito que est�o funcionais sim. 
// 
// STARTING Finalizar a atualiza��o da GuiImagem
//		Checar as fun��es criarComponentes
//		e criar PosicionarComponentes p/ os circulos!!
// 
// Introduzir reacao medida
// (colocar reacao minmax?) 
//
// Finalizar GuiControl e GuiTelas
//
// Terminar GuiTransicao
// Terminar GuiInteratividade
// 
// Finalizar o GuiStates. Resolver se terei nos containers.
// Tornar o GuiStates uma forma do usu�rio implementar temas
// de forma f�cil. Talvez atrav�s de um GuiTema que seja um
// wrapper para GuiStates. O usu�rio poderia ent�o instanciar 
// seus GuiTemas em um arquivo a parte e usar eles para aplicar 
// designs de forma mais r�pida e f�cil aos seus objetos. 
//
/////////////// COmponentes
// Atualizar o bot�o para introdu��o de um container em cima dele.
// Depois, text boxes, Check boxes e radio buttons, 
// 
// Implementar novos componentes Avatares, tags, scroller, 
// cards, menus
//
//////////////////////////////////////////////////////////////

#include "stdafx.h"
#include <SFML/Main.hpp>
#include <SFML/Window.hpp>
#include "IncluirGui.h"
#include <math.h>
#include "GuiQuina.h"

/*
//
// incluir o zoom como par�metro
// precisar� ser passsado para os construtores do container
//
*/;

sf::VertexArray QuinaSuperiorDireita(sf::Vector2f raio, sf::Vector2f pos, float m_bordaGrossura, sf::Vector2f corte) {
	//float pixel = 1.0f / zoom;
	
	sf::VertexArray retornar(sf::LineStrip);
	
	if (m_bordaGrossura == 0) {
		//(raio.y - corte.y - (raio.y - (raio.y) * sin(acos(corte.x / raio.x)))) * 4 + (corte.x == 0 ? 0 : 1);

		int index = 0;

		for (int i = 0; i < (raio.y - corte.y); i++) {

			float largura = (raio.y)*cos(asin((raio.y - 0.5 - i) / raio.y));
			largura = (largura*raio.x) / raio.y;

			if ((largura - corte.x) < 0)
				continue;

			else {
				retornar.resize(index+4);
				
				retornar[index] = (sf::Vertex(sf::Vector2f(pos.x + corte.x,
					pos.y + raio.y - (raio.y - 0.5 - i)), sf::Color::Transparent));

				retornar[index + 1] = (sf::Vertex(sf::Vector2f(pos.x + corte.x,
					pos.y + raio.y - (raio.y - 0.5 - i)), sf::Color::Blue));

				retornar[index + 2] = (sf::Vertex(sf::Vector2f(pos.x + largura,
					pos.y + raio.y - (raio.y - 0.5 - i)), sf::Color::Blue));

				retornar[index + 3] = (sf::Vertex(sf::Vector2f(pos.x + largura,
					pos.y + raio.y - (raio.y - 0.5 - i)), sf::Color::Transparent));
				
				index+=4;
			}
		}

		

	}

	else {
		float raioMaior = raio.y + m_bordaGrossura;  
		int index = 0;
		float limiteDaBorda = raioMaior - (raio.y)*sin(acos(corte.x / raio.x));
		//retornar.resize(((raioMaior - limiteDaBorda - corte.y) * 6) + ((m_bordaGrossura * 4)));

		for (int i = 0; i < (raioMaior - corte.y); i++) {

			float larguraBorda = (raioMaior)*cos(asin((raioMaior - 0.5 - i) / raioMaior));
			larguraBorda = (larguraBorda*(raio.x + m_bordaGrossura)) / (raio.y + m_bordaGrossura);

			if ((larguraBorda - corte.x) < 0)
				continue;

			if (i >= limiteDaBorda) {

				retornar.resize(index + 6);

				float largura = (raio.y)*cos(asin((raio.y - 0.5 - (i - m_bordaGrossura)) / raio.y));
				largura = (largura*raio.x) / raio.y;

				retornar[index] = (sf::Vertex(sf::Vector2f(pos.x  + larguraBorda, pos.y + raio.y -
					(raioMaior - 0.5 - i)), sf::Color::Transparent));

				retornar[index + 1] = (sf::Vertex(sf::Vector2f(pos.x + larguraBorda, pos.y + raio.y -
					(raioMaior - 0.5 - i)), sf::Color::Red));

				retornar[index + 2] = (sf::Vertex(sf::Vector2f((pos.x + largura),
					pos.y + raio.y - (raioMaior - 0.5 - i)), sf::Color::Red));

				retornar[index + 3] = (sf::Vertex(sf::Vector2f((pos.x + largura),
					pos.y + raio.y - (raioMaior - 0.5 - i)), sf::Color::Blue));

				retornar[index + 4] = (sf::Vertex(sf::Vector2f((pos.x + corte.x),
					pos.y + raio.y - (raioMaior - 0.5 - i)), sf::Color::Blue));

				retornar[index + 5] = (sf::Vertex(sf::Vector2f((pos.x + corte.x),
					pos.y + raio.y - (raioMaior - 0.5 - i)), sf::Color::Transparent));
				
				index += 6;

			}
			else {
				retornar.resize(index + 4);
				
				retornar[index] = (sf::Vertex(sf::Vector2f(pos.x + corte.x, pos.y + raio.y -
					(raioMaior - 0.5 - i)), sf::Color::Transparent));

				retornar[index + 1] = (sf::Vertex(sf::Vector2f(pos.x + corte.x, pos.y + raio.y -
					(raioMaior - 0.5 - i)), sf::Color::Red));

				retornar[index + 2] = (sf::Vertex(sf::Vector2f((pos.x + larguraBorda), pos.y + raio.y -
					(raioMaior - 0.5 - i)), sf::Color::Red));

				retornar[index + 3] = (sf::Vertex(sf::Vector2f(pos.x + larguraBorda, pos.y + raio.y -
					(raioMaior - 0.5 - i)), sf::Color::Transparent));

				index += 4;
			}
		
		}
	}

	return retornar;
}

sf::VertexArray QuinaSuperiorEsquerda(sf::Vector2f raio, sf::Vector2f pos, float m_bordaGrossura, sf::Vector2f corte) {
	sf::VertexArray retornar(sf::LineStrip);
	
	if (m_bordaGrossura == 0) {

		//retornar.resize((raio.y - corte.y - (raio.y - (raio.y)*sin(acos(corte.x / raio.x)))) * 4 + ((corte.x == 0) ? 0 : -1));
		int index=0;

		for (int i = 0; i < (raio.y-corte.y) ; i++) {
			
			float largura = (raio.y)*cos(asin((raio.y - 0.5 - i) / raio.y));
			largura = (largura*raio.x) / raio.y;
			
			if ((largura - corte.x) <= 0)
				continue;
			
			else {
				retornar.resize(index + 4);
				retornar[index ] = (sf::Vertex(sf::Vector2f(pos.x + raio.x - largura,
					pos.y + raio.y - (raio.y - 0.5 - i)), sf::Color::Transparent));

				retornar[index + 1] = (sf::Vertex(sf::Vector2f(pos.x + raio.x - largura,
					pos.y + raio.y - (raio.y - 0.5 - i)), sf::Color::Blue));

				retornar[index + 2] = (sf::Vertex(sf::Vector2f(pos.x + raio.x - corte.x,
					pos.y + raio.y - (raio.y - 0.5 - i)), sf::Color::Blue));

				retornar[index + 3] = (sf::Vertex(sf::Vector2f(pos.x + raio.x - corte.x,
					pos.y + raio.y - (raio.y - 0.5 - i)), sf::Color::Transparent));
				
				index += 4;
			}
		}
		
	}

	else {
		float raioMaior = raio.y + m_bordaGrossura;
		int index = 0;
		float limiteDaBorda = raioMaior - (raio.y)*sin(acos(corte.x / raio.x));
		//retornar.resize(((raioMaior - limiteDaBorda - corte.y) * 6) + ((m_bordaGrossura * 4)));
		

		for (int i = 0; i < (raioMaior - corte.y); i++) {
			

			float larguraBorda = (raioMaior)*cos(asin((raioMaior - 0.5 - i) / raioMaior));
			larguraBorda = (larguraBorda*(raio.x + m_bordaGrossura)) / (raio.y + m_bordaGrossura);

			if ((larguraBorda - corte.x) < 0 )
				continue;

			if (i >= limiteDaBorda) {
				retornar.resize(index + 6);
				float largura = (raio.y)*cos(asin((raio.y - 0.5 - (i - m_bordaGrossura)) / raio.y));
				largura = (largura*raio.x) / raio.y;

				retornar[index] = (sf::Vertex(sf::Vector2f(pos.x + raio.x - larguraBorda, pos.y + raio.y -
					(raioMaior - 0.5 - i)), sf::Color::Transparent));

				retornar[index + 1] = (sf::Vertex(sf::Vector2f(pos.x + raio.x - larguraBorda, pos.y + raio.y -
					(raioMaior - 0.5 - i)), sf::Color::Red));

				retornar[index + 2] = (sf::Vertex(sf::Vector2f((pos.x + raio.x - largura),
					pos.y + raio.y - (raioMaior - 0.5 - i)), sf::Color::Red));

				retornar[index + 3] = (sf::Vertex(sf::Vector2f((pos.x + raio.x - largura),
					pos.y + raio.y - (raioMaior - 0.5 - i)), sf::Color::Blue));

				retornar[index + 4] = (sf::Vertex(sf::Vector2f((pos.x + raio.x - corte.x),
					pos.y + raio.y - (raioMaior - 0.5 - i)), sf::Color::Blue));

				retornar[index + 5] = (sf::Vertex(sf::Vector2f(pos.x + raio.x - corte.x,
					pos.y + raio.y - (raioMaior - 0.5 - i)), sf::Color::Transparent));
					
				index += 6;
				
			}
			else {
				retornar.resize(index + 4);

				retornar[index] = (sf::Vertex(sf::Vector2f(pos.x + raio.x - larguraBorda, pos.y + raio.y -
									(raioMaior - 0.5 - i)), sf::Color::Transparent));
				
				retornar[index + 1] = (sf::Vertex(sf::Vector2f(pos.x + raio.x - larguraBorda, pos.y + raio.y -
					(raioMaior - 0.5 - i)), sf::Color::Red));
				
				retornar[index + 2] = (sf::Vertex(sf::Vector2f((pos.x + raio.x - corte.x), pos.y + raio.y -
					(raioMaior - 0.5 - i)), sf::Color::Red));
				
				retornar[index + 3] = (sf::Vertex(sf::Vector2f(pos.x + raio.x - corte.x, pos.y + raio.y -
					(raioMaior - 0.5 - i)), sf::Color::Transparent));
				
				index += 4;
			}
		}
	}

	return retornar;
}

sf::VertexArray QuinaInferiorEsquerda(sf::Vector2f raio, sf::Vector2f pos, float m_bordaGrossura, sf::Vector2f corte) {
	sf::VertexArray retornar(sf::LineStrip);

	if (m_bordaGrossura == 0) {

		//retornar.resize((raio.y - corte.y - (raio.y - (raio.y) * sin(acos(corte.x / raio.x)))) * 4 + ((corte.x == 0) ? 0 : 2));
		int index = 0;
		

		for (int i = 0; i < (raio.y - corte.y); i++) {

			float largura = (raio.y) * cos(asin((raio.y - 0.5 - i) / raio.y));
			largura = (largura * raio.x) / raio.y;

			if ((largura - corte.x) <= 0)
				continue;

			else {
				retornar.resize(index + 4);
				retornar[index] = (sf::Vertex(sf::Vector2f(pos.x + raio.x - largura,
					pos.y + (raio.y - 0.5 - i)), sf::Color::Transparent));

				retornar[index + 1] = (sf::Vertex(sf::Vector2f(pos.x + raio.x - largura,
					pos.y +  (raio.y - 0.5 - i)), sf::Color::Blue));

				retornar[index + 2] = (sf::Vertex(sf::Vector2f(pos.x + raio.x - corte.x,
					pos.y +  (raio.y - 0.5 - i)), sf::Color::Blue));

				retornar[index + 3] = (sf::Vertex(sf::Vector2f(pos.x + raio.x - corte.x,
					pos.y +  (raio.y - 0.5 - i)), sf::Color::Transparent));

				index+=4;
			}
		}

	}

	else {
		float raioMaior = raio.y + m_bordaGrossura;
		int index = 0;
		float limiteDaBorda = raioMaior - (raio.y) * sin(acos(corte.x / raio.x));
		//retornar.resize(((raioMaior - limiteDaBorda - corte.y) * 6) + ((m_bordaGrossura * 4)));


		for (int i = 0; i < (raioMaior - corte.y); i++) {


			float larguraBorda = (raioMaior)* cos(asin((raioMaior - 0.5 - i) / raioMaior));
			larguraBorda = (larguraBorda * (raio.x + m_bordaGrossura)) / (raio.y + m_bordaGrossura);

			if ((larguraBorda - corte.x) < 0)
				continue;

			if (i >= limiteDaBorda) {
				retornar.resize(index + 6);
				float largura = (raio.y) * cos(asin((raio.y - 0.5 - (i - m_bordaGrossura)) / raio.y));
				largura = (largura * raio.x) / raio.y;

				retornar[index] = (sf::Vertex(sf::Vector2f(pos.x + raio.x - larguraBorda, pos.y + 
					(raioMaior - 0.5 - i)), sf::Color::Transparent));

				retornar[index + 1] = (sf::Vertex(sf::Vector2f(pos.x + raio.x - larguraBorda, pos.y + 
					(raioMaior - 0.5 - i)), sf::Color::Red));

				retornar[index + 2] = (sf::Vertex(sf::Vector2f((pos.x + raio.x - largura),
					pos.y +  (raioMaior - 0.5 - i)), sf::Color::Red));

				retornar[index + 3] = (sf::Vertex(sf::Vector2f((pos.x + raio.x - largura),
					pos.y +  (raioMaior - 0.5 - i)), sf::Color::Blue));

				retornar[index + 4] = (sf::Vertex(sf::Vector2f((pos.x + raio.x - corte.x),
					pos.y +  (raioMaior - 0.5 - i)), sf::Color::Blue));

				retornar[index + 5] = (sf::Vertex(sf::Vector2f(pos.x + raio.x - corte.x,
					pos.y + (raioMaior - 0.5 - i)), sf::Color::Transparent));

				index += 6;

			}
			else {
				retornar.resize(index + 4);
				retornar[index] = (sf::Vertex(sf::Vector2f(pos.x + raio.x - larguraBorda, pos.y +
					(raioMaior - 0.5 - i)), sf::Color::Transparent));

				retornar[index + 1] = (sf::Vertex(sf::Vector2f(pos.x + raio.x - larguraBorda, pos.y + 
					(raioMaior - 0.5 - i)), sf::Color::Red));

				retornar[index + 2] = (sf::Vertex(sf::Vector2f((pos.x + raio.x - corte.x), pos.y + 
					(raioMaior - 0.5 - i)), sf::Color::Red));

				retornar[index + 3] = (sf::Vertex(sf::Vector2f(pos.x + raio.x - corte.x, pos.y + 
					(raioMaior - 0.5 - i)), sf::Color::Transparent));

				index += 4;
			}
		}
	}

	return retornar;
}


sf::VertexArray QuinaInferiorDireita(sf::Vector2f raio, sf::Vector2f pos, float m_bordaGrossura, sf::Vector2f corte) {
	sf::VertexArray retornar(sf::LineStrip);

	if (m_bordaGrossura == 0) {

		//retornar.resize((raio.y - corte.y - (raio.y - (raio.y) * sin(acos(corte.x / raio.x)))) * 4 + ((corte.x == 0) ? 0 : 2));
		int index = 0;

		for (int i = 0; i < (raio.y - corte.y); i++) {

			float largura = (raio.y) * cos(asin((raio.y - 0.5 - i) / raio.y));
			largura = (largura * raio.x) / raio.y;

			if ((largura - corte.x) < 0)
				continue;

			else {
				retornar.resize(index + 4);
				retornar[index] = (sf::Vertex(sf::Vector2f(pos.x + corte.x,
					pos.y +  (raio.y - 0.5 - i)), sf::Color::Transparent));

				retornar[index + 1] = (sf::Vertex(sf::Vector2f(pos.x + corte.x,
					pos.y +  (raio.y - 0.5 - i)), sf::Color::Blue));

				retornar[index + 2] = (sf::Vertex(sf::Vector2f(pos.x + largura,
					pos.y +  (raio.y - 0.5 - i)), sf::Color::Blue));

				retornar[index + 3] = (sf::Vertex(sf::Vector2f(pos.x + largura,
					pos.y +  (raio.y - 0.5 - i)), sf::Color::Transparent));

				index+=4;
			}
		}

	}

	else {
		float raioMaior = raio.y + m_bordaGrossura;
		int index = 0;
		float limiteDaBorda = raioMaior - (raio.y) * sin(acos(corte.x / raio.x));
		//retornar.resize(((raioMaior - limiteDaBorda - corte.y) * 6) + ((m_bordaGrossura * 4)));

		for (int i = 0; i < (raioMaior - corte.y); i++) {

			float larguraBorda = (raioMaior)* cos(asin((raioMaior - 0.5 - i) / raioMaior));
			larguraBorda = (larguraBorda * (raio.x + m_bordaGrossura)) / (raio.y + m_bordaGrossura);

			if ((larguraBorda - corte.x) < 0)
				continue;

			if (i >= limiteDaBorda) {
				retornar.resize(index + 6);
				float largura = (raio.y) * cos(asin((raio.y - 0.5 - (i - m_bordaGrossura)) / raio.y));
				largura = (largura * raio.x) / raio.y;

				retornar[index] = (sf::Vertex(sf::Vector2f(pos.x + larguraBorda, pos.y + 
					(raioMaior - 0.5 - i)), sf::Color::Transparent));

				retornar[index + 1] = (sf::Vertex(sf::Vector2f(pos.x + larguraBorda, pos.y + 
					(raioMaior - 0.5 - i)), sf::Color::Red));

				retornar[index + 2] = (sf::Vertex(sf::Vector2f((pos.x + largura),
					pos.y +  (raioMaior - 0.5 - i)), sf::Color::Red));

				retornar[index + 3] = (sf::Vertex(sf::Vector2f((pos.x + largura),
					pos.y +  (raioMaior - 0.5 - i)), sf::Color::Blue));

				retornar[index + 4] = (sf::Vertex(sf::Vector2f((pos.x + corte.x),
					pos.y +  (raioMaior - 0.5 - i)), sf::Color::Blue));

				retornar[index + 5] = (sf::Vertex(sf::Vector2f((pos.x + corte.x),
					pos.y + (raioMaior - 0.5 - i)), sf::Color::Transparent));

				index += 6;

			}
			else {
				retornar.resize(index + 4);

				retornar[index] = (sf::Vertex(sf::Vector2f(pos.x + corte.x, pos.y + 
					(raioMaior - 0.5 - i)), sf::Color::Transparent));

				retornar[index + 1] = (sf::Vertex(sf::Vector2f(pos.x + corte.x, pos.y + 
					(raioMaior - 0.5 - i)), sf::Color::Red));

				retornar[index + 2] = (sf::Vertex(sf::Vector2f((pos.x + larguraBorda), pos.y + 
					(raioMaior - 0.5 - i)), sf::Color::Red));

				retornar[index + 3] = (sf::Vertex(sf::Vector2f(pos.x + larguraBorda, pos.y + 
					(raioMaior - 0.5 - i)), sf::Color::Transparent));

				index += 4;
			}

		}
	}

	return retornar;

	
}


class GuiQuina;

int main() {
	sf::ContextSettings settings;
	settings.antialiasingLevel = 1000;

	sf::RenderWindow window(sf::VideoMode(800, 600), "Nation: Build & Attack!", sf::Style::Default, settings);
	sf::View myview(sf::Vector2f(400, 300), sf::Vector2f(800, 600));
	window.setView(myview);

	window.

	//GuiQuina quinaSE(GuiQuina::vertice::INFDIR);
	//quinaSE.update(GuiQuina::vertice::INFDIR, sf::Vector2f(200, 300), sf::Vector2f(50, 0), sf::Vector2f(50, 200), 20, sf::Color::Blue, sf::Color::Green);
	
	
	
	sf::Font font; 
	font.loadFromFile("arial.ttf");
	GuiImagem pic("a.png", sf::Color::Black, 200,100,10,2);
	GuiLabel text( "TITULO", sf::Color::Black, font,20);
	GuiBotao botao(sf::Color::Yellow, sf::Color::Black, sf::Color::Black, sf::Text("HitMe", font, 20), 100, 40, 2, 5);
	
	GuiContainer a(sf::Color::White,sf::Color::Red, GuiContainer::Ordenacao::ORDEM_VERTICAL, 800,600,5,10,10,10,10);
	a.setVarMembros(2, 0, GuiContainer::AlinharHorizon::ALINHAR_CENTRAL_HORIZONTAL, GuiContainer::AlinharVert::ALINHAR_ACIMA, 20);

	a.addContainer(GuiContainer(sf::Color::White, sf::Color::Blue, GuiContainer::Ordenacao::ORDEM_VERTICAL, 796, 150, 2, 10, 10, 15, 10));
	a.getMembros()[0]->setVarMembros(10, 0, GuiContainer::AlinharHorizon::ALINHAR_ESQUERDA, GuiContainer::AlinharVert::ALINHAR_ACIMA, 20);
	a.getMembros()[0]->addContainer(GuiContainer(sf::Color(0,0,0,64), sf::Color::Black, GuiContainer::Ordenacao::ORDEM_HORIZONTAL, 796, 38, 0,0));
	a.getMembros()[0]->getMembros()[0]->setVarMembros(10, 10, GuiContainer::AlinharHorizon::ALINHAR_DIREITA, GuiContainer::AlinharVert::ALINHAR_CENTRAL_VERTICAL, 20);
	a.getMembros()[0]->getMembros()[0]->addLabel(GuiLabel("HOME", sf::Color::Black, font, 20));
	a.getMembros()[0]->getMembros()[0]->addLabel(GuiLabel("EPIS�DIOS", sf::Color::Black, font, 20));
	a.getMembros()[0]->getMembros()[0]->addLabel(GuiLabel("CONTATO", sf::Color::Black, font, 20));
	a.getMembros()[0]->getMembros()[0]->addLabel(GuiLabel("SOBRE", sf::Color::Black, font, 20));

	a.getMembros()[0]->addContainer(GuiContainer(sf::Color::White,sf::Color::White, GuiContainer::Ordenacao::ORDEM_HORIZONTAL, 796, 50,0,0));
	a.getMembros()[0]->getMembros()[1]->setVarMembros(0, 0, GuiContainer::AlinharHorizon::ALINHAR_CENTRAL_HORIZONTAL, GuiContainer::AlinharVert::ALINHAR_CENTRAL_VERTICAL, 2);
	a.getMembros()[0]->getMembros()[1]->addContainer(GuiContainer(sf::Color::White,sf::Color::Black,GuiContainer::Ordenacao::ORDEM_SOBREPOSTA,150,30,4,2));
	a.getMembros()[0]->getMembros()[1]->getMembros()[0]->setVarMembros(0, 0, GuiContainer::AlinharHorizon::ALINHAR_CENTRAL_HORIZONTAL, GuiContainer::AlinharVert::ALINHAR_CENTRAL_VERTICAL, 0);
	a.getMembros()[0]->getMembros()[1]->addBotao(GuiBotao(sf::Color(0,0,255,199), sf::Color::Black, sf::Color::Black,sf::Text("Hit",font,25),30.0f,30.0f,2.0f,4.0f));
	a.getMembros()[0]->getMembros()[1]->getMembros()[0]->addLabel(GuiLabel("a", sf::Color::Black, font, 25));


	//GuiContainer b(sf::Color(0,0,0,255), sf::Color(0, 0,255, 0), GuiContainer::Ordenacao::ORDEM_VERTICAL, 200, 400, 0, 25, 25, 25, 25);
	//b.setVarMembros(10, 10, GuiContainer::AlinharHorizon::ALINHAR_DIREITA, GuiContainer::AlinharVert::ALINHAR_ACIMA, 10);
	//GuiContainer c(sf::Color(255, 255, 0, 128), sf::Color::Blue, GuiContainer::Ordenacao::ORDEM_VERTICAL, 200, 400, 2, sf::Vector2f(40,30), sf::Vector2f(40, 30), sf::Vector2f(40, 30), sf::Vector2f(40, 30));
	//c.setVarMembros(0, 20, GuiContainer::AlinharHorizon::ALINHAR_CENTRAL_HORIZONTAL, GuiContainer::AlinharVert::ALINHAR_ACIMA, 10);
	//GuiContainer d(sf::Color(255,0,0,32), sf::Color::Black, GuiContainer::ORDEM_VERTICAL, 160, 40, 2, 10,0,0,0);
	//d.setVarMembros(10, 10, GuiContainer::AlinharHorizon::ALINHAR_DIREITA, GuiContainer::AlinharVert::ALINHAR_ACIMA, 0);
	//GuiContainer e(sf::Color::Green, sf::Color::Black, GuiContainer::ORDEM_SOBREPOSTA, 50.0f, 50.0f, 25.0f, 4.0f);
	
	//d.addLabel(text);
	//b.addContainer(d); 
	//b.addContainer(d);
	//
	//b.addContainer(d);
	//	
	//c.addLabel(text);
	//c.addContainer(e);
	//
	//b.addContainer(text);
	//a.addContainer(c);
	//
	////c.addContainer(pic);

	//a.addContainer(c);
	//a.addContainer(b);
	//a.addBotao(botao);
	//
	//a.getMembros()[2]->setTransparencia(64);
	///*a.addBotao(botao);*/


	int valor = 0;
	
	while (window.isOpen())
	{
		sf::Event event;
		while (window.pollEvent(event))
		{
			if (event.type == sf::Event::TextEntered) {
				a.getMembros()[0]->getMembros()[1]->getMembros()[0]->getMembros()[0]->appendText(event.key.code);			
			}
			if (event.type == sf::Event::Closed)
				window.close();
			
			if (event.type == sf::Event::Resized) {

				myview.setSize(event.size.width, event.size.height);
				window.setView(myview);
				//a.getMembros()[0]->addContainer(e);

				a.setTamanhoX(event.size.width);
				a.setTamanhoY(event.size.height);
				a.updateMyTamanho();


				/*
				a.setTamanhoX(window.getSize().x);
				a.setTamanhoY(window.getSize().y);
				a.setX(window.mapPixelToCoords(window.getPosition()).x);
				a.setY(window.mapPixelToCoords(window.getPosition()).y);
				a.PosicionarRet()
				a.updateMembers();
				*/
			}

			if (sf::Mouse::isButtonPressed(sf::Mouse::Button::Left)) {
				
				
				//myview.zoom(0.9);
				//window.setView(myview);
				
				///*valor -= 10;
				//a.updateMembers();
				//if(a.getMembros()[2]->getCor().a==255)
				//	a.getMembros()[2]->setTransparencia(a.getMembros()[2]->getCor().a-128);
				//else
				//	a.getMembros()[2]->setTransparencia(255);*/				
			}
			if (sf::Mouse::isButtonPressed(sf::Mouse::Button::Right)) {


				//a.getMembros()[0]->addContainer(d);
				
				/*window.setView(myview);*/
				
				/*static int aa = 1;
				if (aa == 1 && false) {
					a.setColor(sf::Color::Blue, sf::Color::Blue);
					aa = 0;
				}
				else {
					a.setColor(sf::Color::White, sf::Color::White);
					aa = 1;
				}*/
			}

			
		}
		
		
		window.clear();
		a.drawMe(&window);
		//window.draw(quinaSE.getPontos());
		//window.draw(sd);
		//window.draw(se);
		//window.draw(ie);
		//window.draw(id);

		window.display();
	}
	return 0;
}