#pragma once

#include "GuiInteratividade.h"

class GuiTela;
class GuiContainer;
class GuiTransicao;
class GuiDesign;
//enum causa;


class GuiControl
{
	sf::RenderWindow* m_janela;
	sf::View* m_view; // A view da tela principal
	//Outros containers podem possuir outras views 
	//para implementas scrolls ou similares

	std::vector<GuiTela*> m_telas;
	std::vector<GuiTransicao*> m_transicoes;
	std::vector<GuiInteratividade*> m_intera_rodando;

	GuiContainer m_background;
	GuiContainer* m_mae;//Est� contido no background

	//Container que vai conter os containers pai de cada tela
	//Ele tem seu tamanho setado como ser preenchido 
	//Pode ser "desligado" setando ele como invis�vel
	
	std::string m_estilo;
	sf::Vector2f m_tamanho_inicial; //-1 para fullscreen
	sf::Vector2f m_tamanho_minimo;

	//Eu poderia mudar para o nome n� 

	int m_tela_atual = 0;
	int m_tela_inicial = 0;

	int m_tela_destino = 0; //Se eu implementar guitrans usando guiintera acho que n�o vou precisar!

	bool m_esta_em_transicao=false;  //Se eu implementar guitrans usando guiintera acho que n�o vou precisar!

	//////////////////////////////////////////////////////////
	//////////////////FUN��ES PRIVADAS////////////////////////
	
	//FUNCAO AUX USADA PARA MANTER AS TRANSICOES ORDENADAS PARA FACIL ACESSO
	void ordenarTransicoes();
	
	// Funcao usada para rodar a transicao, enquanto ela acontece o programa ficaria irresponsivo, preciso checar isso.
	// posso por exemplo colocar dentro do loop dela uma checagem para eventos que checaria tanto na lista de interatividade 
	// da tela origem como da tela destino
	void rodarTransicao(const GuiTransicao& trans);

	//Roda as interatividades em curso que s�o guardadas em m_intera_rodando
	//Chamada a cada loop dentro da Loop() para mover, mudar cor e tamanho de acordo comm as interatividades. 
	//Precisa ser chamada dentro de rodarTransicao tbm
	void rodarInteratividades(); 
	
	//Checa as interatividades da tela atual que sejam correspondentes 
	// a essa causa
	void checarInteratividades(causa c); 

	//Convert um evento para a causa correspondente
	causa convertToCausa(const sf::Event &tipo);
	
public:


	//O LOOP PRINCIPAL A SER CHAMADO PARA ABRIR A JANELA E RODAR O PROGRAMA
	//Deve ser chamado ap�s toda a configura��o do programa ser feito
	void Loop();

				//MUDANDO O BACKGROUND
	void addImagemBackground(const GuiImagem& imagem) { m_background.addImagem(imagem); }
	void setarCorBackground(const sf::Color& cor) { m_background.setColor(cor,cor); }
	//Acho que vou deixar a possibilidade do usu�rio adicionar interatividade no 
	//background, mas n�o outros containers al�m do m�e

				//ADICIONANDO TELAS E TRANSICOES
	void addTela(const GuiTela& tela);
	void addTransicao(const GuiTransicao& transicao);
	void criarHeader();
	void criarFooter();
	void mudarTelaInicial(std::string nome);

	GuiTela* getTelaPorNome(std::string nome);


				//CONSTRUTORES
	/* devo incluir tamb�m o estilo,*/
	GuiControl(const sf::RenderWindow& window, const sf::View& view, bool possuiBackground,
		const sf::Vector2f& tamanhoInicial, const sf::Vector2f& tamanhoMinimo, const std::string& TelaInicial,
		const sf::Color& corBordaMae, const sf::Color& corMae, float bordaMae);

	GuiControl();
	~GuiControl();
};

