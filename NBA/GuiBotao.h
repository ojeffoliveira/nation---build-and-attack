//////////////////////////////////////////////////////////////
//
// Define um bot�o que pode ter tanto apenas texto como um
// texto e uma imagem ou apenas uma imagem. O botao � customizavel
// de tal forma que o design dele quando nao esta e quando 
// esta selecionado s�o definiveis.
//
//////////////////////////////////////////////////////////////
#pragma once
#include "GuiComponente.h"
#include "GuiLabel.h"
#include "GuiImagem.h"


class GuiBotao: public GuiComponente
{
	
	bool m_possuiImagem=false;
	float m_espacoTextoEImagem = 0;

	sf::Color m_corTexto;
	sf::Color m_corTextoSelecionado;
	sf::Color m_corSelecionado;
	sf::Color m_corBordaSelecionado;
	sf::Vector2f m_deslocamentoSelecionado = {0.0,0.0};

public:
	/*virtual void drawMe(sf::RenderWindow *myWindow) const;
	virtual void updatePosicao(int index);
	virtual void criarPartes() override final;
	virtual void PosicionarRet();
	virtual void updateMembers();*/
	//virtual bool estaDentro(sf::Vector2i) override { ; }
	virtual void mudarEstado(GuiComponente::Estado estado) override final;
	//virtual void criarComponente(sf::String TextureFilename, sf::String label) override;
	//virtual void posicionarComponente() override final;
	
	//ALINHAMENTO AUTOMATICO: AMBOS CENTRAIS

		//SEM NADA: Acesso direto ao container
	
	//Construtor nao implementado para criar um botao sem membros. O proprio usu�rio
	//podera adicionar membros ou usar como um container que responde a cliques
	GuiBotao(sf::Color cor, sf::Color corDaBorda, float TamanhoX, float TamanhoY,
		float borda = 0, float quina = 0, bool AcessoDireto = true);

		//COM TEXTO SEM IMAGEM
	GuiBotao(sf::Color cor, sf::Color corDaBorda, sf::Color corTexto, sf::Text texto,
			float TamanhoX, float TamanhoY,  float borda = 0, float quina = 0);
		//COM TEXTO COM IMAGEM, IMAGEM PRIMEIRO
	GuiBotao(sf::Color cor, sf::Color corDaBorda, sf::Color corTexto, sf::String filename,
			sf::Text texto , GuiContainer::Ordenacao ordem, float TamanhoX,
			float TamanhoY, float espacoEntre, float borda = 0, 
			float quina = 0);
		//COM TEXTO COM IMAGEM, TEXTO PRIMEIRO
	GuiBotao(sf::Color cor, sf::Color corDaBorda, sf::Color corTexto, sf::Text texto,
		sf::String filename, GuiContainer::Ordenacao ordem, float TamanhoX,
		float TamanhoY, float espacoEntre, float borda = 0,
		float quina = 0);
		//SEM TEXTO COM IMAGEM
	GuiBotao(sf::Color cor, sf::Color corDaBorda, sf::String filename,
			float TamanhoX, float TamanhoY, float borda = 0, float quina = 0);
	
	//DEFINI��O PELO USUARIO DOS ALINHAMENTOS

	GuiBotao(AlinharHorizon horizontal, AlinharVert Vertical, sf::Color cor, float margemHorizontal,
			float margemVertical, sf::Color corDaBorda, sf::Color corTexto, sf::Text texto,
			float TamanhoX, float TamanhoY, float borda = 0, float quina = 0);
	
	GuiBotao(AlinharHorizon horizontal, AlinharVert Vertical, float margemHorizontal, 
			float margemVertical, sf::Color cor, sf::Color corDaBorda, 
			sf::Color corTexto, sf::String filename, sf::Text texto, 
			GuiContainer::Ordenacao ordem, float TamanhoX,
			float TamanhoY, float espacoEntre,
			float borda = 0, float quina = 0);

	GuiBotao(AlinharHorizon horizontal, AlinharVert Vertical, float margemHorizontal,
			float margemVertical, sf::Color cor, sf::Color corDaBorda, 
			sf::Color corTexto, sf::Text texto, sf::String filename, 
			GuiContainer::Ordenacao ordem, float TamanhoX,
			float TamanhoY, float espacoEntre,
			float borda = 0, float quina = 0);

	GuiBotao(AlinharHorizon horizontal, AlinharVert Vertical, float margemHorizontal,
			float margemVertical, sf::Color cor, sf::Color corDaBorda, sf::String filename,
			float TamanhoX, float TamanhoY, float borda = 0, float quina = 0);

	/*void quandoSelecionado(sf::Vector2f deslocamento);
	void quandoSelecionado(sf::Vector2f deslocamento, sf::Color cor, sf::Color m_corDaBorda);*/
	//GuiBotao();
	~GuiBotao();
};

