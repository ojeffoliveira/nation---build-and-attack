#include "stdafx.h"
#include "GuiInteratividade.h"
#include <ctime>



//S� falta setar o do ESTA FORA DO PAI e ESTA FORA DAS MARGENS  DO PAI
//Acho que vou excluir esses dois ^^
//Ao iniciar eu deveria tamb�m fazer uma c�pia do design real do container afetado
//
bool GuiInteratividade::checarInicio(const sf::Event &evento, const GuiControl& control) {
	if (m_origem == NULL || m_container->getDesignAtual() == m_origem) {

		switch (m_causa) {

			case causa::SEMPRE_TRUE:
				m_tempo_transcorrido = 0;
				if (&m_callmebefore != NULL)
					m_callmebefore(evento, control);

				break;
			case causa::SEM_INTERACAO_POR:
				m_tempo_transcorrido = 0;
				if (&m_callmebefore != NULL)
					m_callmebefore(evento, control);
				break;
			case causa::CLICK_ESQUERDO:
				if (m_container->estaDentro(sf::Vector2i(evento.mouseButton.x, evento.mouseButton.y))) {
					m_tempo_transcorrido = 0;
					if (&m_callmebefore != NULL)
						m_callmebefore(evento, control);
				}
				break;
			case causa::CLICK_DIREITO:
				if (m_container->estaDentro(sf::Vector2i(evento.mouseButton.x, evento.mouseButton.y))) {
					m_tempo_transcorrido = 0;
					if (&m_callmebefore != NULL)
						m_callmebefore(evento, control);
				}
				break;
			case causa::CLICK_MEIO:
				if (m_container->estaDentro(sf::Vector2i(evento.mouseButton.x, evento.mouseButton.y))) {
					m_tempo_transcorrido = 0;
					if (&m_callmebefore != NULL)
						m_callmebefore(evento, control);
				}
				break;
			case causa::PONTEIRO_MOVEU:
				m_tempo_transcorrido = 0;
				if (&m_callmebefore != NULL)
					m_callmebefore(evento, control);
				break;
			case causa::PONTEIRO_SOBRE:
				if (m_container->estaDentro(sf::Vector2i(sf::Mouse::getPosition()))){
					m_tempo_transcorrido = 0;
					if (&m_callmebefore != NULL)
						m_callmebefore(evento, control);
				}
				break;
			case causa::PONTEIRO_NAO_SOBRE:
				if (!(m_container->estaDentro(sf::Vector2i(sf::Mouse::getPosition())))) {
					m_tempo_transcorrido = 0;
					if (&m_callmebefore != NULL)
						m_callmebefore(evento, control);
				}
				break;
			case causa::MOUSE_SCROLL_UP:
				m_tempo_transcorrido = 0;
				if (&m_callmebefore != NULL)
					m_callmebefore(evento, control);
				break;
			case causa::MOUSE_SCROLL_DOWN:
				m_tempo_transcorrido = 0;
				if (&m_callmebefore != NULL)
					m_callmebefore(evento, control);
				break;
			case causa::CONTAINER_FORA_DO_PAI:

				break;
			case causa::CONTAINER_FORA_MARGENS_DO_PAI:	//Acho que vou precisar de umafun��o para esse e o de cima
			
				break;
			case causa::TEXTO_ESCRITO:
				m_tempo_transcorrido = 0;
				if (&m_callmebefore != NULL)
					m_callmebefore(evento, control);
				break;
			case causa::KEY_PRESSED:
				if (evento.key.code == (m_tecla.code)) {
					m_tempo_transcorrido = 0;
					if (&m_callmebefore != NULL)
						m_callmebefore(evento, control);
				}
				break;
			case causa::KEY_RELEASED:	//Em d�vida se funcionar� com alt ctrl system e shift
				if (evento.key.code == (m_tecla.code)) {
					m_tempo_transcorrido = 0;
					if (&m_callmebefore != NULL)
						m_callmebefore(evento, control);
				}
				break;
			case causa::IF_TRUE_FUNCTION:
				if (m_funcao(evento,control)) {
					m_tempo_transcorrido = 0;
					if (&m_callmebefore != NULL)
						m_callmebefore(evento, control);
				}
				break;
			default:
				break;
		}
	}

	return esta_rodando();
}

void GuiInteratividade::rodarInteratividade(GuiControl &control) {
	

	//Eu poderia colocar esse if no checar
	if (m_tempo_transcorrido == 0) {
		m_inicio = (std::chrono::system_clock::now());
	}
	std::chrono::time_point<std::chrono::system_clock> agora;
	agora = (std::chrono::system_clock::now());

	m_tempo_transcorrido  = std::chrono::duration<double>(m_inicio - agora).count();


	if (m_tempo_transcorrido>m_tempo_p_efeito)
		m_tempo_transcorrido = m_tempo_p_efeito;
	
	float andamento;
	
	if (m_tempo_p_efeito == 0)
		andamento = 100;
	else
		andamento = m_tempo_transcorrido / m_tempo_p_efeito;

	//Acho que vou criar uma fun��o para cada efeito e chamala aqui passando andamento para cada uma 
	int tam = (m_efeitos.size());
	for (int i = 0; i < tam; i++) {
		switch (m_efeitos[i]) {
		case efeito::MUDAR_DESIGN:

			break;
		case efeito::MUDAR_COR:
			
			break;
		case efeito::MUDAR_COR_BORDA:

			break;
		case efeito::MUDAR_TRANSPARENCIA:

			break;
		case efeito::MUDAR_BORDA_GROSSURA:

			break;
		case efeito::MUDAR_TAMANHO:

			break;
		case efeito::MUDAR_POS:

			break;
		case efeito::MUDAR_METADADOS: //Instant�neo, s� ocorre no fim do tempo

			break;
		case efeito::ADC_CONTAINER: //Instant�neo, s� ocorre no fim do tempo

			break;
		case efeito::ADC_COMPONENTE: //Instant�neo, s� ocorre no fim do tempo

			break;
		case efeito::MUDAR_POS_EM_M_MEMBROS: //Instant�neo, s� ocorre no fim do tempo

			break;
		case efeito::DELETAR: //Instant�neo, s� ocorre no fim do tempo

			break;
		case efeito::MOVER_PARA: //Instant�neo, s� ocorre no fim do tempo

			break;
		case efeito::CLONAR_PARA: //Instant�neo, s� ocorre no fim do tempo

			break;
		case efeito::APPEND_TEXTO: //Instant�neo, s� ocorre no fim do tempo

			break;
		case efeito::FLIP_BOOL_VALUE: //Instant�neo, s� ocorre no fim do tempo

			break;
		case efeito::FUNCAO: //Instant�neo, s� ocorre no fim do tempo

			break;
		default:
			break;
		}

	}
	if (&m_callmeafter != NULL) {
		m_callmeafter(control);
	}

	if (m_tempo_transcorrido == m_tempo_p_efeito) {
		m_tempo_transcorrido = -1;
		
	}

	if (m_start_at_the_end != NULL) {
		//Iniciar ela e adicionar no vector do GuiControl
		//m_start_at_the_end;
	}
}




GuiInteratividade::GuiInteratividade(causa porque, efeito oque, GuiContainer* container, GuiDesign* origem, 
		GuiContainer* afetado, float TempoParaEfeito_milisegundos, GuiContainer* cont_secund, GuiDesign* design_secund):
	m_causa(porque), m_efeitos(oque), m_container(container), m_origem(origem),
	m_container_afetado(afetado), m_tempo_p_efeito(TempoParaEfeito_milisegundos), m_container_sec(cont_secund),
	m_design_secundario(design_secund)
{
}

//
//O construtor completo para uma interatividade de um efeito que n�o envolve design
//
GuiInteratividade::GuiInteratividade(causa porque, efeito oque, GuiContainer* container, GuiContainer* afetado, 
		float TempoParaEfeito_milisegundos, bool(*callMeBefore)(const sf::Event& evento, const GuiControl& control), 
		void(*callmeafter)(const GuiControl& control), GuiInteratividade* start_at_the_end, GuiContainer* cont_secund):
	m_causa(porque), m_efeitos(oque), m_container(container), m_container_afetado(afetado), 
	m_tempo_p_efeito(TempoParaEfeito_milisegundos), m_callmebefore(callMeBefore),
	m_callmeafter(callmeafter), m_start_at_the_end(start_at_the_end), m_container_sec(cont_secund)
{
}

GuiInteratividade::GuiInteratividade(causa porque, efeito oque, GuiContainer* container, GuiDesign* origem, GuiContainer* afetado,
		float TempoParaEfeito_milisegundos, bool(*callMeBefore)(const sf::Event& evento, const GuiControl& control), 
		void(*callmeafter)(const GuiControl& control), GuiInteratividade* start_at_the_end, GuiContainer* cont_secund, 
		GuiDesign* design_secund): m_causa(porque), m_efeitos(oque), m_container(container), m_origem(origem),
		m_container_afetado(afetado), m_tempo_p_efeito(TempoParaEfeito_milisegundos), m_callmebefore(callMeBefore),
		m_callmeafter(callmeafter), m_start_at_the_end(start_at_the_end), m_container_sec(cont_secund), 
		m_design_secundario(design_secund)
{
}

//
//O constrtuor completo para uma interatividade de mais de um efeito
//
GuiInteratividade::GuiInteratividade(causa porque, std::vector<efeito> oques, GuiContainer* container,GuiDesign* origem, 
	GuiContainer* afetado, float TempoParaEfeito_milisegundos, 
	bool(*callMeBefore)(const sf::Event& evento, const GuiControl& control), void(*callmeafter)(const GuiControl& control), 
	GuiInteratividade* start_at_the_end, GuiContainer* cont_secund, GuiDesign* design_secund): 
	m_causa(porque), m_efeitos(oques), m_container(container), m_origem(origem),
	m_container_afetado(afetado), m_tempo_p_efeito(TempoParaEfeito_milisegundos), m_callmebefore(callMeBefore),
	m_callmeafter(callmeafter), m_start_at_the_end(start_at_the_end), m_container_sec(cont_secund),
	m_design_secundario(design_secund)
{

}

//
//O constrtuor completo para uma interatividade de mais de um efeito sem designs
//
GuiInteratividade::GuiInteratividade(causa porque, std::vector<efeito> oques, GuiContainer* container, 
	GuiContainer* afetado, float TempoParaEfeito_milisegundos, 
	bool(*callMeBefore)(const sf::Event& evento, const GuiControl& control), void(*callmeafter)(const GuiControl& control),
	GuiInteratividade* start_at_the_end, GuiContainer* cont_secund):m_causa(porque), m_efeitos(oques), 
	m_container(container), m_container_afetado(afetado), m_tempo_p_efeito(TempoParaEfeito_milisegundos), 
	m_callmebefore(callMeBefore), m_callmeafter(callmeafter), m_start_at_the_end(start_at_the_end),
	m_container_sec(cont_secund)
{
}

GuiInteratividade::GuiInteratividade()
{
}


GuiInteratividade::~GuiInteratividade()
{
}
