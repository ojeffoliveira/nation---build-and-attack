#include "stdafx.h"
#include "GuiQuina.h"






void GuiQuina::update()
{
	m_pontos = sf::VertexArray{ sf::LineStrip };

	if ((m_raio.x == 0 && m_raio.y == 0) || (m_cor.a == 0 && m_corDaBorda.a == 0)) {
		m_num_pontos = 0;
		return;
	}

	int auxX = 1;
	int auxY = 1;

	//(1-m_x*2)
	if (m_x == 1)
		auxX = -1;
	if (m_y == 1)
		auxY = -1;

	if (m_borda == 0) {

		//retornar.resize((m_raio.y - m_corte.y - (m_raio.y - (m_raio.y)*sin(acos(m_corte.x / m_raio.x)))) * 4 + ((m_corte.x == 0) ? 0 : -1));
		int index = 0;

		for (int i = 0; i < (m_raio.y - m_corte.y); i++) {

			float largura = (m_raio.y) * cos(asin((m_raio.y - 0.5 - i) / m_raio.y));
			largura = (largura * m_raio.x) / m_raio.y;

			if ((largura - m_corte.x) <= 0)
				continue;


			else {
				m_pontos.resize(index + 4);
				m_pontos[index] = (sf::Vertex(sf::Vector2f(m_pos.x + (m_x * m_raio.x) + auxX * largura,
					m_pos.y + (m_y * m_raio.y) + auxY * (m_raio.y - 0.5 - i)), sf::Color::Transparent));

				m_pontos[index + 1] = (sf::Vertex(sf::Vector2f(m_pos.x + (m_x * m_raio.x) + auxX * largura,
					m_pos.y + (m_y * m_raio.y) + auxY * (m_raio.y - 0.5 - i)), m_cor));

				m_pontos[index + 2] = (sf::Vertex(sf::Vector2f(m_pos.x + (m_x * m_raio.x) + auxX * m_corte.x,
					m_pos.y + (m_y * m_raio.y) + auxY * (m_raio.y - 0.5 - i)), m_cor));

				m_pontos[index + 3] = (sf::Vertex(sf::Vector2f(m_pos.x + (m_x * m_raio.x) + auxX * m_corte.x,
					m_pos.y + (m_y * m_raio.y) + auxY * (m_raio.y - 0.5 - i)), sf::Color::Transparent));

				index += 4;
			}
		}

		m_num_pontos = index;
	}

	else {
		float m_raioMaior = m_raio.y + m_borda;
		int index = 0;
		float limiteDaBorda = m_raioMaior - (m_raio.y) * sin(acos(m_corte.x / m_raio.x));
		//m_pontos.resize(((m_raioMaior - limiteDaBorda - m_corte.y) * 6) + ((m_borda * 4)));


		for (int i = 0; i < (m_raioMaior - m_corte.y); i++) {


			float larguraBorda = (m_raioMaior)* cos(asin((m_raioMaior - 0.5 - i) / m_raioMaior));
			larguraBorda = (larguraBorda * (m_raio.x + m_borda)) / (m_raio.y + m_borda);

			if ((larguraBorda - m_corte.x) < 0)
				continue;

			if (i >= limiteDaBorda) {
				m_pontos.resize(index + 6);
				float largura = (m_raio.y) * cos(asin((m_raio.y - 0.5 - (i - m_borda)) / m_raio.y));
				largura = (largura * m_raio.x) / m_raio.y;

				m_pontos[index] = (sf::Vertex(sf::Vector2f(m_pos.x + (m_x * m_raio.x) + auxX * larguraBorda,
					m_pos.y + (m_y * m_raio.y) + auxY * (m_raioMaior - 0.5 - i)), sf::Color::Transparent));

				m_pontos[index + 1] = (sf::Vertex(sf::Vector2f(m_pos.x + (m_x * m_raio.x) + auxX * larguraBorda,
					m_pos.y + (m_y * m_raio.y) + auxY * (m_raioMaior - 0.5 - i)), m_corDaBorda));

				m_pontos[index + 2] = (sf::Vertex(sf::Vector2f((m_pos.x + (m_x * m_raio.x) + auxX * largura),
					m_pos.y + (m_y * m_raio.y) + auxY * (m_raioMaior - 0.5 - i)), m_corDaBorda));

				m_pontos[index + 3] = (sf::Vertex(sf::Vector2f((m_pos.x + (m_x * m_raio.x) + auxX * largura),
					m_pos.y + (m_y * m_raio.y) + auxY * (m_raioMaior - 0.5 - i)), m_cor));

				m_pontos[index + 4] = (sf::Vertex(sf::Vector2f((m_pos.x + (m_x * m_raio.x) + auxX * m_corte.x),
					m_pos.y + (m_y * m_raio.y) + auxY * (m_raioMaior - 0.5 - i)), m_cor));

				m_pontos[index + 5] = (sf::Vertex(sf::Vector2f(m_pos.x + (m_x * m_raio.x) + auxX * m_corte.x,
					m_pos.y + (m_y * m_raio.y) + auxY * (m_raioMaior - 0.5 - i)), sf::Color::Transparent));

				index += 6;

			}
			else {
				m_pontos.resize(index + 4);

				m_pontos[index] = (sf::Vertex(sf::Vector2f(m_pos.x + (m_x * m_raio.x) + auxX * larguraBorda,
					m_pos.y + (m_y * m_raio.y) + auxY * (m_raioMaior - 0.5 - i)), sf::Color::Transparent));

				m_pontos[index + 1] = (sf::Vertex(sf::Vector2f(m_pos.x + (m_x * m_raio.x) + auxX * larguraBorda,
					m_pos.y + (m_y * m_raio.y) + auxY * (m_raioMaior - 0.5 - i)), m_corDaBorda));

				m_pontos[index + 2] = (sf::Vertex(sf::Vector2f((m_pos.x + (m_x * m_raio.x) + auxX * m_corte.x),
					m_pos.y + (m_y * m_raio.y) + auxY * (m_raioMaior - 0.5 - i)), m_corDaBorda));

				m_pontos[index + 3] = (sf::Vertex(sf::Vector2f(m_pos.x + (m_x * m_raio.x) + auxX * m_corte.x,
					m_pos.y + (m_y * m_raio.y) + auxY * (m_raioMaior - 0.5 - i)), sf::Color::Transparent));

				index += 4;
			}
		}
		m_num_pontos = index;
	}



}

void GuiQuina::update(const sf::Vector2f& raio, const sf::Vector2f& corte, const sf::Vector2f& pos, const float& borda,
	const sf::Color& cor, const sf::Color& corDaBorda) {
	
	if (m_raio == raio && m_corte == corte && m_pos == pos && m_borda == borda &&
		m_cor == cor && m_corDaBorda == corDaBorda)
		return;

	m_raio = raio;
	m_corte = corte;
	m_borda = borda;
	m_pos = pos;
	m_cor = cor;
	m_corDaBorda = corDaBorda;

	m_pontos = sf::VertexArray{ sf::LineStrip };

	if ((m_raio.x == 0 && m_raio.y == 0) || (m_cor.a == 0 && m_corDaBorda.a == 0)){
		m_num_pontos = 0;
		return;
	}

	int auxX=1;
	int auxY=1;
	
	//(1-m_x*2)
	if (m_x == 1)
		auxX=-1;
	if (m_y == 1)
		auxY=-1;
	
	if (m_borda == 0) {

		//retornar.resize((m_raio.y - m_corte.y - (m_raio.y - (m_raio.y)*sin(acos(m_corte.x / m_raio.x)))) * 4 + ((m_corte.x == 0) ? 0 : -1));
		int index = 0;

		for (int i = 0; i < (m_raio.y - m_corte.y); i++) {

			float largura = (m_raio.y) * cos(asin((m_raio.y - 0.5 - i) / m_raio.y));
			largura = (largura * m_raio.x) / m_raio.y;

			if ((largura - m_corte.x) <= 0)
				continue;
			

			else {
				m_pontos.resize(index + 4);
				m_pontos[index] = (sf::Vertex(sf::Vector2f(m_pos.x +(m_x*m_raio.x) +auxX * largura,
					m_pos.y + (m_y* m_raio.y) + auxY*(m_raio.y - 0.5 - i)), sf::Color::Transparent));

				m_pontos[index + 1] = (sf::Vertex(sf::Vector2f(m_pos.x + (m_x * m_raio.x) + auxX * largura,
					m_pos.y + (m_y * m_raio.y) + auxY * (m_raio.y - 0.5 - i)), m_cor));

				m_pontos[index + 2] = (sf::Vertex(sf::Vector2f(m_pos.x + (m_x * m_raio.x) + auxX * m_corte.x,
					m_pos.y + (m_y * m_raio.y) + auxY * (m_raio.y - 0.5 - i)), m_cor));

				m_pontos[index + 3] = (sf::Vertex(sf::Vector2f(m_pos.x + (m_x * m_raio.x) + auxX * m_corte.x,
					m_pos.y + (m_y * m_raio.y) + auxY * (m_raio.y - 0.5 - i)), sf::Color::Transparent));

				index += 4;
			}
		}

		m_num_pontos = index;
	}

	else {
		float m_raioMaior = m_raio.y + m_borda;
		int index = 0;
		float limiteDaBorda = m_raioMaior - (m_raio.y) * sin(acos(m_corte.x / m_raio.x));
		//m_pontos.resize(((m_raioMaior - limiteDaBorda - m_corte.y) * 6) + ((m_borda * 4)));


		for (int i = 0; i < (m_raioMaior - m_corte.y); i++) {


			float larguraBorda = (m_raioMaior)* cos(asin((m_raioMaior - 0.5 - i) / m_raioMaior));
			larguraBorda = (larguraBorda * (m_raio.x + m_borda)) / (m_raio.y + m_borda);

			if ((larguraBorda - m_corte.x) < 0)
				continue;

			if (i >= limiteDaBorda) {
				m_pontos.resize(index + 6);
				float largura = (m_raio.y) * cos(asin((m_raio.y - 0.5 - (i - m_borda)) / m_raio.y));
				largura = (largura * m_raio.x) / m_raio.y;

				m_pontos[index] = (sf::Vertex(sf::Vector2f(m_pos.x + (m_x*m_raio.x) + auxX*larguraBorda, 
					m_pos.y + (m_y*m_raio.y) + auxY* (m_raioMaior - 0.5 - i)), sf::Color::Transparent));

				m_pontos[index + 1] = (sf::Vertex(sf::Vector2f(m_pos.x + (m_x * m_raio.x) + auxX * larguraBorda, 
					m_pos.y + (m_y * m_raio.y) + auxY * (m_raioMaior - 0.5 - i)), m_corDaBorda));

				m_pontos[index + 2] = (sf::Vertex(sf::Vector2f((m_pos.x + (m_x * m_raio.x) + auxX * largura),
					m_pos.y + (m_y * m_raio.y) + auxY * (m_raioMaior - 0.5 - i)), m_corDaBorda));

				m_pontos[index + 3] = (sf::Vertex(sf::Vector2f((m_pos.x + (m_x * m_raio.x) + auxX * largura),
					m_pos.y + (m_y * m_raio.y) + auxY * (m_raioMaior - 0.5 - i)), m_cor));

				m_pontos[index + 4] = (sf::Vertex(sf::Vector2f((m_pos.x + (m_x * m_raio.x) + auxX * m_corte.x),
					m_pos.y + (m_y * m_raio.y) + auxY * (m_raioMaior - 0.5 - i)), m_cor));

				m_pontos[index + 5] = (sf::Vertex(sf::Vector2f(m_pos.x + (m_x * m_raio.x) + auxX * m_corte.x,
					m_pos.y + (m_y * m_raio.y) + auxY * (m_raioMaior - 0.5 - i)), sf::Color::Transparent));

				index += 6;

			}
			else {
				m_pontos.resize(index + 4);

				m_pontos[index] = (sf::Vertex(sf::Vector2f(m_pos.x + (m_x * m_raio.x) + auxX * larguraBorda, 
					m_pos.y + (m_y* m_raio.y) + auxY * (m_raioMaior - 0.5 - i)), sf::Color::Transparent));

				m_pontos[index + 1] = (sf::Vertex(sf::Vector2f(m_pos.x + (m_x * m_raio.x) + auxX * larguraBorda,
					m_pos.y + (m_y * m_raio.y) + auxY * (m_raioMaior - 0.5 - i)), m_corDaBorda));

				m_pontos[index + 2] = (sf::Vertex(sf::Vector2f((m_pos.x + (m_x * m_raio.x) + auxX * m_corte.x),
					m_pos.y + (m_y* m_raio.y) + auxY * (m_raioMaior - 0.5 - i)), m_corDaBorda));

				m_pontos[index + 3] = (sf::Vertex(sf::Vector2f(m_pos.x + (m_x * m_raio.x) + auxX * m_corte.x,
					m_pos.y + (m_y* m_raio.y) + auxY * (m_raioMaior - 0.5 - i)), sf::Color::Transparent));

				index += 4;
			}
		}
		m_num_pontos = index;
	}

	
}

GuiQuina::GuiQuina() {
	m_vert = vertice::SUPDIR;
	m_x = horiz::DIR;
	m_y = vert::SUP;
	m_raio = sf::Vector2f(0, 0);
	m_pos = sf::Vector2f(0, 0);
	m_corte = sf::Vector2f(0, 0);
	m_borda = 0;
	m_cor = sf::Color::Red;
	m_corDaBorda = sf::Color::Blue;
	m_num_pontos = 0;
}


GuiQuina::~GuiQuina()
{
}
