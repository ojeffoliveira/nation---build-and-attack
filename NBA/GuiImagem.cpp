#include "stdafx.h"
#include "GuiImagem.h"


//
//sf::VertexArray GuiImagem::QuinaSuperiorDireita(float x, float y) {
//	sf::VertexArray retornar(sf::LineStrip);
//	/*
//	float raio = m_quinaSuperiorEsquerda;
//	if (m_bordaGrossura == 0) {
//		retornar.resize(raio * 4);
//		for (int i = 0; i < raio; i++) {
//			float largura = (raio)*cos(asin((raio - 0.5 - i) / raio));
//			
//			{
//				retornar[i * 4] = (sf::Vertex(sf::Vector2f(x + raio - largura, y + raio - (raio - 0.5 - i)), sf::Color::Transparent));
//				retornar[i * 4 + 1] = (sf::Vertex(sf::Vector2f(x + raio - largura, y + raio - (raio - 0.5 - i)), m_cor));
//				retornar[i * 4 + 2] = (sf::Vertex(sf::Vector2f(x + raio, y + raio - (raio - 0.5 - i)), m_cor));
//				retornar[i * 4 + 3] = (sf::Vertex(sf::Vector2f(x + raio, y + raio - (raio - 0.5 - i)), sf::Color::Transparent));
//			}
//
//		}
//
//	}
//
//	else {
//		retornar.resize((raio * 6) + (m_bordaGrossura * 4));
//		float raioMaior = raio + m_bordaGrossura;
//
//		for (int i = 0; i < raioMaior; i++) {
//
//			float larguraBorda = (raioMaior)*cos(asin((raioMaior - 0.5 - i) / raioMaior));
//
//			if (i >= m_bordaGrossura) {
//
//				int aux = i - m_bordaGrossura;
//				int ultimoPonto = (m_bordaGrossura * 4) + (i - m_bordaGrossura) * 6 - 1;
//				float largura = (raio)*cos(asin((raio - 0.5 - (i - m_bordaGrossura)) / raio));
//				{
//
//					retornar[ultimoPonto + 1] = (sf::Vertex(sf::Vector2f(x + raio - larguraBorda, y + raio -
//						(raioMaior - 0.5 - i)), sf::Color::Transparent));
//					retornar[ultimoPonto + 2] = (sf::Vertex(sf::Vector2f(x + raio - larguraBorda, y + raio -
//						(raioMaior - 0.5 - i)), m_corDaBorda));
//					retornar[ultimoPonto + 3] = (sf::Vertex(sf::Vector2f(x + raio - largura, y + raio - (raioMaior - 0.5 - i)), m_corDaBorda));
//					retornar[ultimoPonto + 4] = (sf::Vertex(sf::Vector2f(x + raio - largura, y + raio - (raioMaior - 0.5 - i)), m_cor));
//					retornar[ultimoPonto + 5] = (sf::Vertex(sf::Vector2f(x + raio, y + raio - (raioMaior - 0.5 - i)), m_cor));
//					retornar[ultimoPonto + 6] = (sf::Vertex(sf::Vector2f(x + raio, y + raio - (raioMaior - 0.5 - i)), sf::Color::Transparent));
//				}
//
//			}
//			else {
//
//
//				retornar[i * 4] = (sf::Vertex(sf::Vector2f(x + raio - larguraBorda, y + raio -
//					(raioMaior - 0.5 - i)), sf::Color::Transparent));
//				retornar[i * 4 + 1] = (sf::Vertex(sf::Vector2f(x + raio - larguraBorda, y + raio -
//					(raioMaior - 0.5 - i)), m_corDaBorda));
//				retornar[i * 4 + 2] = (sf::Vertex(sf::Vector2f(x + raio, y + raio -
//					(raioMaior - 0.5 - i)), m_corDaBorda));
//				retornar[i * 4 + 3] = (sf::Vertex(sf::Vector2f(x + raio, y + raio -
//					(raioMaior - 0.5 - i)), sf::Color::Transparent));
//			}
//		}
//
//
//	}*/
//	return retornar;
//
//}
//sf::VertexArray GuiImagem::QuinaSuperiorEsquerda(float x, float y) {
//	sf::VertexArray retornar(sf::LineStrip);
//	return retornar;
//}
//sf::VertexArray GuiImagem::QuinaInferiorEsquerda(float x, float y) {
//	sf::VertexArray retornar(sf::LineStrip);
//	return retornar;
//
//}
//sf::VertexArray GuiImagem::QuinaInferiorDireita(float x, float y) {
//	sf::VertexArray retornar(sf::LineStrip);
//	return retornar;
//
//}


//Essa fun��o define o retangulo interno da nossa textura que deve ser mostrado
//Ela � chamada em criarcomponentes() que com esses valores atribui a cada rentagulo(s)
// e circulo a sua parte devida. Quando o tamanho da imagem muda ela precisa ser chamada
void GuiImagem::setTextureLimites() {
	
	if (m_Texture.getSize().x != m_TamanhoX && m_Texture.getSize().y == m_TamanhoY) {
		m_textureInicioX = (((m_Texture.getSize().x) / 2) - (m_TamanhoX / 2));
		m_textureInicioY = 0;
		m_textureFimX = (((m_Texture.getSize().x) / 2) + (m_TamanhoX / 2));
		m_textureFimY = (m_Texture.getSize().y);
	}
	
	else if (m_Texture.getSize().x == m_TamanhoX && m_Texture.getSize().y != m_TamanhoY) {
		m_textureInicioX = 0;
		m_textureInicioY = (((m_Texture.getSize().y) / 2) - (m_TamanhoY / 2));
		m_textureFimX = (m_Texture.getSize().x);
		m_textureFimY = (((m_Texture.getSize().y) / 2) + (m_TamanhoY / 2));
	}
	else if (m_Texture.getSize().x != m_TamanhoX && m_Texture.getSize().y != m_TamanhoY) {
		m_textureInicioX = (((m_Texture.getSize().x) / 2) - (m_TamanhoX / 2));
		m_textureInicioY = (((m_Texture.getSize().y) / 2) - (m_TamanhoY / 2));
		m_textureFimX = (((m_Texture.getSize().x) / 2) + (m_TamanhoX / 2));
		m_textureFimY = (((m_Texture.getSize().y) / 2) + (m_TamanhoY / 2));

	}
	else {
		m_textureInicioX = 0;
		m_textureInicioY = 0;
		m_textureFimX = m_Texture.getSize().x;
		m_textureFimY = m_Texture.getSize().y;
	}
}

//Cria as partes (retangulo(s) e circulos em casos de borda curva) do container 
// e seta suas cores e bordas
void GuiImagem::criarComponente() {

}


//void GuiImagem::criarComponente() {
//	setTextureLimites();
//
//	//if (false) {
//	//	//Quinas retas e sem bordas
//	//	if (m_raioDaQuina == 0 && m_bordaGrossura == 0) {
//	//		m_retangulos.resize(1, sf::RectangleShape(sf::Vector2f(m_TamanhoX, m_TamanhoY)));
//	//		m_retangulos[0].setFillColor(m_cor);
//	//	}
//	//	//Quinas retas e com bordas
//	//	else if (m_raioDaQuina == 0 && m_bordaGrossura > 0) {
//	//		m_retangulos.resize(1, sf::RectangleShape(sf::Vector2f(m_TamanhoX, m_TamanhoY)));
//	//		m_retangulos[0].setFillColor(m_cor);
//	//		m_retangulos[0].setOutlineThickness(m_bordaGrossura);
//	//		m_retangulos[0].setOutlineColor(m_corDaBorda);
//	//	}
//	//	//Quinas em circulo e sem bordas
//	//	else if (m_raioDaQuina > 0 && m_bordaGrossura == 0) {
//	//		m_retangulos.resize(3);
//	//		m_quinas.resize(4);
//
//	//		//Definindo raio e cor das quinas
//	//		for (int i = 0; i < 4; i++) {
//	//			m_quinas[i].setRadius(m_raioDaQuina);
//	//			m_quinas[i].setFillColor(m_cor);
//
//	//		}
//
//	//		m_retangulos[0].setSize(sf::Vector2f(m_TamanhoX - 2 * m_raioDaQuina, m_TamanhoY));
//	//		for (int i = 1; i < 3; i++)
//	//			m_retangulos[i].setSize(sf::Vector2f(m_raioDaQuina, m_TamanhoY - 2 * m_raioDaQuina));
//	//		//Cores
//
//	//		for (int i = 0; i < 3; i++)
//	//			m_retangulos[i].setFillColor(m_cor);
//	//	}
//	//	//Quinas em circulo e com bordas
//	//	else if (m_raioDaQuina > 0 && m_bordaGrossura > 0) {
//	//		m_retangulos.resize(7);
//	//		m_quinas.resize(4);
//
//	//		//Definindo raio cor e bordas das quinas
//	//		for (int i = 0; i < 4; i++) {
//	//			m_quinas[i].setRadius(m_raioDaQuina);
//	//			m_quinas[i].setFillColor(m_cor);
//	//			m_quinas[i].setOutlineThickness(m_bordaGrossura);
//	//			m_quinas[i].setOutlineColor(m_corDaBorda);
//	//		}
//
//
//	//		//Setando tamanho e cor das bordas e retangulos interiores
//	//		for (int i = 0; i < 7; i++) {
//	//			if (i == 0)
//	//				m_retangulos[i].setSize(sf::Vector2f(m_TamanhoX - 2 * m_raioDaQuina, m_TamanhoY));
//	//			else if (i == 1 || i == 2)
//	//				m_retangulos[i].setSize(sf::Vector2f(m_raioDaQuina, m_TamanhoY - 2 * m_raioDaQuina));
//	//			else if (i == 3 || i == 5)
//	//				m_retangulos[i].setSize(sf::Vector2f(m_TamanhoX - 2 * m_raioDaQuina, m_bordaGrossura));
//	//			else if (i == 4 || i == 6)
//	//				m_retangulos[i].setSize(sf::Vector2f(m_bordaGrossura, m_TamanhoY - 2 * m_raioDaQuina));
//
//	//			if (i < 3)
//	//				m_retangulos[i].setFillColor(m_cor);
//	//			else
//	//				m_retangulos[i].setFillColor(m_corDaBorda);
//	//		}
//
//	//	}
//
//
//
//	//Cortando a imagem original de acordo com o tamanho setado da GuiImagem
//
//	//Quinas retas 
//	/*
//	if (m_raioDaQuina == 0 ) {
//		m_retangulos[0].setTexture(&m_Texture,false);
//		m_retangulos[0].setTextureRect(sf::IntRect(m_textureInicioX,m_textureInicioY,
//			m_textureFimX-m_textureInicioX,m_textureFimY-m_textureInicioY));
//	}
//	//Quinas em circulo 
//	else if (m_raioDaQuina > 0) {
//
//		m_retangulos[0].setTexture(&m_Texture, false);
//		m_retangulos[0].setTextureRect(sf::IntRect((m_textureInicioX+m_raioDaQuina), 
//									m_textureInicioY,(m_textureFimX - m_textureInicioX-2*m_raioDaQuina), 
//									m_textureFimY - m_textureInicioY));
//
//		for (int i = 0; i < 2; i++) {
//			m_retangulos[i+1].setTexture(&m_Texture, false);
//			m_retangulos[i+1].setTextureRect(sf::IntRect(m_textureInicioX+(m_TamanhoX-m_raioDaQuina)*i,
//														m_textureInicioY+m_raioDaQuina,
//														m_raioDaQuina,
//														(m_textureFimY - m_textureInicioY)-2*m_raioDaQuina));
//
//		}
//		
//		m_quina[0].setTexture(&m_Texture, false);
//		m_quina[0].setTextureRect(sf::IntRect(m_textureInicioX, m_textureInicioY,
//									2*m_raioDaQuina, 2 * m_raioDaQuina));
//
//		m_quina[2].setTexture(&m_Texture, false);
//		m_quina[2].setTextureRect(sf::IntRect(m_textureInicioX+(m_textureFimX- m_textureInicioX-2*m_raioDaQuina),
//									m_textureInicioY,
//									2 * m_raioDaQuina, 2 * m_raioDaQuina));
//
//		m_quina[1].setTexture(&m_Texture, false);
//		m_quina[1].setTextureRect(sf::IntRect(m_textureInicioX, m_textureInicioY+(m_textureFimY-m_textureInicioX- 2 * m_raioDaQuina),
//												2 * m_raioDaQuina, 2 * m_raioDaQuina));
//
//		m_quina[3].setTexture(&m_Texture, false);
//		m_quina[3].setTextureRect(sf::IntRect(m_textureInicioX + (m_textureFimX - m_textureInicioX - 2 * m_raioDaQuina),
//									m_textureInicioY + (m_textureFimY - m_textureInicioX - 2 * m_raioDaQuina),
//									2 * m_raioDaQuina, 2 * m_raioDaQuina));
//	}
//	*/
//	PosicionarRet();
//	int a = 2;
//	return NULL;
//}



GuiImagem::GuiImagem(std::string filename, sf::Color corDaBorda,
					float tamanhoX, float tamanhoY, float quina, float borda) 
	: GuiComponente(sf::Color::White, corDaBorda, GuiContainer::Ordenacao::ORDEM_SOBREPOSTA, tamanhoX, tamanhoY, borda, quina)
{
	m_Texture.loadFromFile(filename);
	criarPartes();
	criarComponente();
}

GuiImagem::GuiImagem(std::string filename):
	GuiComponente(sf::Color::Transparent, sf::Color::Transparent, GuiContainer::Ordenacao::ORDEM_SOBREPOSTA, 0,0,0,0)
{
	m_imagem.loadFromFile(filename);
	m_Texture.loadFromFile(filename);
	setTamanhoX(m_Texture.getSize().x);
	setTamanhoY(m_Texture.getSize().y);
	criarPartes();
	criarComponente();
}




GuiImagem::GuiImagem(GuiContainer *pai,std::string filename, sf::Color corDaBorda, 
	float tamanhoX, float tamanhoY, float quina, float borda):
	GuiComponente(pai, sf::Color::Transparent, corDaBorda, GuiContainer::Ordenacao::ORDEM_SOBREPOSTA, tamanhoX, tamanhoY, borda, quina)
{
	m_imagem.loadFromFile(filename);
	m_Texture.loadFromFile(filename);
	
	criarPartes();
	criarComponente();
}

GuiImagem::GuiImagem(std::string filename, GuiContainer *pai) :
	GuiComponente(pai, sf::Color::Transparent, sf::Color::Transparent, GuiContainer::Ordenacao::ORDEM_SOBREPOSTA, 0, 0, 0, 0)
{
	m_imagem.loadFromFile(filename);
	m_Texture.loadFromFile(filename);
	
	setTamanhoX(m_Texture.getSize().x);
	setTamanhoY(m_Texture.getSize().y);
	criarPartes();
	criarComponente();
}


GuiImagem::GuiImagem()
{
}


GuiImagem::~GuiImagem()
{
}
