#pragma once
#include "GuiComponente.h"
#include <SFML/Graphics/Text.hpp>


class GuiLabel: public GuiComponente
{
	sf::Text m_label;
	unsigned int m_size;

public:

	void ChageText(sf::Uint32 texto) { m_label.setString(texto); }
	void appendText(sf::Uint32 texto) { m_label.setString(m_label.getString() + texto); }

	virtual void drawMe(sf::RenderWindow *myWindow) override;
	//virtual void posicionarComponente();
	virtual void updatePosicao() override ;
	//virtual bool estaDentro(sf::Vector2i);
	virtual void efeitoSelecionado() { ; }
	GuiLabel(const sf::String &texto, const sf::Color &cor, const sf::Font &fonte, const unsigned int &TamanhoY);
	GuiLabel();
	~GuiLabel();
};

