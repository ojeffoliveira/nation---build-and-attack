#pragma once


class GuiContainer;

class GuiInteratividade;

class GuiTela
{
	std::string m_tela_nome{ "" };
	//Posso mudar isso para pointers e deixar os objetos no 
	//proprio container que recebe a causa, ai, os que estivessem aqui seriam adicionados
	//na realidade no como se fossem do m_pai abaixo ou do background l� no gui control
	std::vector<GuiInteratividade*> m_todas_interatividades;


	GuiContainer m_pai;
	
	//??

public:
	GuiContainer* getContainerPeloNome(std::string nome);
	GuiContainer* getPai();
	GuiContainer* getBody(); // retornar o pai caso n�o haja header e ou footer
	bool estaDentro();
	GuiTela();
	~GuiTela();
};

