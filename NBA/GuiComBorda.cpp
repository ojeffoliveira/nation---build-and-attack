#include "stdafx.h"
#include "GuiComBorda.h"



void GuiComBorda::setColor(sf::Color cor, sf::Color corDaBorda) {
	m_cor = cor;
	m_corDaBorda = cor;

	if (m_raioDaQuina == 0 && m_bordaGrossura == 0) {
		m_retangulos[0].setFillColor(m_cor);
	}
	//Quinas retas e com bordas
	else if (m_raioDaQuina == 0 && m_bordaGrossura > 0) {
		m_retangulos[0].setFillColor(m_cor);
		m_retangulos[0].setOutlineColor(m_corDaBorda);
	}
	//Quinas em circulo e sem bordas
	else if (m_raioDaQuina > 0 && m_bordaGrossura == 0) {

		//Definindo cor das quinas
		for (int i = 0; i < 4; i++) {
			m_quinas[i].setFillColor(m_cor);
		}

		for (int i = 1; i < 3; i++)
			m_retangulos[i].setFillColor(m_cor);
	}

	//Quinas em circulo e com bordas
	else if (m_raioDaQuina > 0 && m_bordaGrossura > 0) {

		//Definindo cor e bordas das quinas
		for (int i = 0; i < 4; i++) {
			m_quinas[i].setFillColor(m_cor);
			m_quinas[i].setOutlineColor(m_corDaBorda);
		}

		//Setando tamanho e cor das bordas e retangulos interiores
		for (int i = 0; i < 7; i++) {
			if (i < 3)
				m_retangulos[i].setFillColor(m_cor);
			else
				m_retangulos[i].setFillColor(m_corDaBorda);
		}

	}
}


GuiComBorda::GuiComBorda()
{
}


GuiComBorda::~GuiComBorda()
{
}
