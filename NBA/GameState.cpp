#include "stdafx.h"
#include "GameState.h"


void GameState::addComoClicavel(GuiComponente *clicavel) {
	m_clicaveis.resize(m_clicaveis.size() + 1, clicavel);
}

void GameState::cliqueEsquerDoMouse(sf::Vector2i MousePosition) {
	for (int i = 0; i < m_clicaveis.size();i++) {
		if (m_clicaveis[i]->estaDentro(MousePosition))
			m_clicaveis[i]->ficarClicado();
	}
}
void cliqueDireitoDoMouse(sf::Vector2i MousePosition);
void botaoPressionado(sf::Keyboard::Key Botao);

GameState::GameState(float X, float Y)
	: m_container(GuiContainer(sf::Color::Transparent, sf::Color::Transparent, GuiContainer::Ordenacao::ORDEM_SOBREPOSTA,
				 0,0,X, Y,0,0,0))
{

}


GameState::~GameState()
{
}
