
//////////////////////////////////////////////////////////////
//
//	A classe que define os gamestates do jogo. Consiste de 
//	arrays de containers que criam a GUI do jogo. Tamb�m possui
//	um array de pointers para todos os itens clic�veis e um pointers
//	para o items selecionado no momento (para quando o usu�rio
//	usar o teclado) ou quando estiver escrevendo.
//	Entre seus m�todos est� lida com cliques do mouse, um que lida
//	com bot�es pressionados e com intera��es com a janela. 
//	
//////////////////////////////////////////////////////////////

#pragma once
#include <vector>
#include <SFML/Window.hpp>
#include "GuiComponente.h"

class GameState
{
	
	GuiContainer m_container;
	std::vector <GuiComponente*>  m_clicaveis;

public:
	
	void addComoClicavel(GuiComponente *clicavel);
	
	void drawMe(sf::RenderWindow *myWindow) { m_container.drawMe(myWindow); }
	//As 3 func abaixo nn deveriam existir
	void cliqueEsquerDoMouse(sf::Vector2i MousePosition);
	void cliqueDireitoDoMouse(sf::Vector2i MousePosition);
	//void (sf::Keyboard::Key Botao);

	void botaoPressionado(sf::Keyboard::Key Botao);
	
	GameState(float X, float Y, GuiContainer m_container):
		m_container(m_container)	
	{
		m_container.setX(X);
		m_container.setY(Y);
		m_container.updatePosicao();
		m_container.updateMembers();
	}
	GameState(const GuiContainer &container) :
		m_container(container)
	{
	}
	GameState(float X, float Y);

	~GameState();
};

