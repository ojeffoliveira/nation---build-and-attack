#include "stdafx.h"
#include "GuiBase.h"
#include <SFML/Graphics/Color.hpp>

//Precisa ser mudada
bool GuiBase::estaDentro(sf::Vector2i posicao) {
	if (m_raioDaQuina == 0)
		if (m_PosicaoX < posicao.x && posicao.x < m_PosicaoX + m_TamanhoX
			&& m_PosicaoY < posicao.y && posicao.y < m_PosicaoY + m_TamanhoY)
			return true;

		else {
			if (m_PosicaoX + m_raioDaQuina <= posicao.x && posicao.x <= m_PosicaoX + m_TamanhoX - m_raioDaQuina
				&& m_PosicaoY < posicao.y && posicao.y < m_PosicaoY + m_TamanhoY)
				return true;
			else if (m_PosicaoX < posicao.x && posicao.x <= m_PosicaoX + m_raioDaQuina
				&& m_PosicaoY + m_raioDaQuina <= posicao.y && posicao.y <= m_PosicaoY + m_TamanhoY - m_raioDaQuina)
				return true;
			else if (m_PosicaoX < posicao.x && posicao.x <= m_PosicaoX + m_raioDaQuina
				&& m_PosicaoY + m_raioDaQuina <= posicao.y && posicao.y <= m_PosicaoY + m_TamanhoY - m_raioDaQuina)
				return true;
			else if (m_PosicaoX + m_TamanhoX - m_raioDaQuina <= posicao.x && posicao.x < m_PosicaoX + m_TamanhoX
				&& m_PosicaoY + m_raioDaQuina <= posicao.y && posicao.y <= m_PosicaoY + m_TamanhoY - m_raioDaQuina)
				return true;
			else 
				for (int i{ 0 }; i < 2; i++) {
					for (int j{ 0 }; j < 2; j++) {
						if (distanceFrom(static_cast<sf::Vector2f>(posicao),
							sf::Vector2f(m_PosicaoX + m_raioDaQuina + (i*(m_TamanhoX - 2 * m_raioDaQuina)),
								m_PosicaoY + m_raioDaQuina + (j*(m_TamanhoY - 2 * m_raioDaQuina)))) < m_raioDaQuina) {
							return true;
						}
					}
				}
		}
}

float GuiBase::distanceFrom(sf::Vector2f a, sf::Vector2f b) {
	return sqrtf((pow((b.x-a.x),2))+(pow((b.y-a.y), 2)));
}


void GuiBase::setCor() {

	if (m_raioDaQuina == 0) {
		m_retangulos[0].setFillColor(m_cor);
		m_retangulos[0].setOutlineColor(m_corDaBorda);
	}
	//Quinas em circulo
	else {

		for (int i = 0; i < 4; i++) {
			m_quinas[i].setFillColor(m_cor);

			m_quinas[i].setOutlineColor(m_corDaBorda);
		}
		for (int i = 0; i < 3; i++)
			m_retangulos[i].setFillColor(m_cor);
		for (int i = 3; i < m_retangulos.size(); i++)
			m_retangulos[i].setOutlineColor(m_corDaBorda);
	}
}

sf::Color GuiBase::addColors(const sf::Color &bg,const sf::Color &emcima) {
	sf::Color r;
	
	r.a = (1 - (1 - (static_cast<float>(emcima.a)/255)) * (1 - (static_cast<float>(bg.a) / 255))) * 255;
	if (((r.a)/255) < 1.0e-6) return r; // Fully transparent -- R,G,B not important
	r.r = ((static_cast<float>(emcima.r) / 255) * (static_cast<float>(emcima.a) / 255) / (static_cast<float>(r.a) / 255)
		+ (static_cast<float>(bg.r) / 255) * (static_cast<float>(bg.a) / 255) * (1 - (static_cast<float>(emcima.a) / 255)) 
		/ (static_cast<float>(r.a) / 255))*255;
	r.g = ((static_cast<float>(emcima.g) / 255) * (static_cast<float>(emcima.a) / 255) / (static_cast<float>(r.a) / 255) 
		+ (static_cast<float>(bg.g) / 255) * (static_cast<float>(bg.a) / 255) * (1 - (static_cast<float>(emcima.a) / 255)) 
		/ (static_cast<float>(r.a) / 255))*255;
	r.b = ((static_cast<float>(emcima.b) / 255) * (static_cast<float>(emcima.a) / 255) / (static_cast<float>(r.a) / 255) 
		+ (static_cast<float>(bg.b) / 255) * (static_cast<float>(bg.a) / 255) * (1 - (static_cast<float>(emcima.a) / 255)) 
		/ (static_cast<float>(r.a) / 255))*255;

	return r;
}

void GuiBase::PosicionarRet() {

	//Quinas retas e sem bordas
	if (m_raioDaQuina == 0 && m_bordaGrossura == 0) {
		m_retangulos[0].setPosition(sf::Vector2f(m_PosicaoX, m_PosicaoY));
	}
	//Quinas retas e com bordas
	else if (m_raioDaQuina == 0 && m_bordaGrossura > 0) {
		m_retangulos[0].setPosition(sf::Vector2f(m_PosicaoX, m_PosicaoY));
	}
	//Quinas em circulo e sem bordas
	else if (m_raioDaQuina > 0 && m_bordaGrossura == 0) {

		for (int i = 0, k = 0; i > -2; i--)
			for (int j = 0; j > -2; j--) {
				m_quinas[k].setPosition(m_PosicaoX + m_raioDaQuina * (i == -1 ? -2 : 0) + m_TamanhoX * i *-1,
					m_PosicaoY + m_raioDaQuina * (j == -1 ? -2 : 0) + m_TamanhoY * j*-1);
				k++;
			}

		m_retangulos[0].setPosition(sf::Vector2f(m_PosicaoX + m_raioDaQuina, m_PosicaoY));
		m_retangulos[1].setPosition(sf::Vector2f(m_PosicaoX, m_PosicaoY + m_raioDaQuina));
		m_retangulos[2].setPosition(sf::Vector2f(m_PosicaoX + m_TamanhoX - m_raioDaQuina, m_PosicaoY + m_raioDaQuina));
	}
	//Quinas em circulo e com bordas FALTA TERMINAR
	else if (m_raioDaQuina > 0 && m_bordaGrossura > 0) {

		for (int i = 0, k = 0; i > -2; i--)
			for (int j = 0; j > -2; j--) {
				m_quinas[k].setPosition(m_PosicaoX + m_raioDaQuina * (i == -1 ? -2 : 0) + m_TamanhoX * i *-1,
					m_PosicaoY + m_raioDaQuina * (j == -1 ? -2 : 0) + m_TamanhoY * j*-1);
				k++;
			}

		m_retangulos[0].setPosition(sf::Vector2f(m_PosicaoX + m_raioDaQuina, m_PosicaoY));
		m_retangulos[1].setPosition(sf::Vector2f(m_PosicaoX, m_PosicaoY + m_raioDaQuina));
		m_retangulos[2].setPosition(sf::Vector2f(m_PosicaoX + m_TamanhoX - m_raioDaQuina, m_PosicaoY + m_raioDaQuina));

		m_retangulos[3].setPosition(sf::Vector2f(m_PosicaoX + m_raioDaQuina, m_PosicaoY - m_bordaGrossura));
		m_retangulos[4].setPosition(sf::Vector2f(m_PosicaoX + m_TamanhoX, m_PosicaoY + m_raioDaQuina));
		m_retangulos[5].setPosition(sf::Vector2f(m_PosicaoX + m_raioDaQuina, m_PosicaoY + m_TamanhoY));
		m_retangulos[6].setPosition(sf::Vector2f(m_PosicaoX - m_bordaGrossura, m_PosicaoY + m_raioDaQuina));

	}
}

//Cria as partes (retangulo(s) e circulos em casos de borda curva) do container 
// e seta suas cores e bordas
void GuiBase::criarPartes() {

	//Quinas retas e sem bordas
	if (m_raioDaQuina == 0 && m_bordaGrossura == 0) {
		m_retangulos.resize(1, sf::RectangleShape(sf::Vector2f(m_TamanhoX, m_TamanhoY)));
		m_retangulos[0].setFillColor(m_cor);
	}
	//Quinas retas e com bordas
	else if (m_raioDaQuina == 0 && m_bordaGrossura > 0) {
		m_retangulos.resize(1, sf::RectangleShape(sf::Vector2f(m_TamanhoX, m_TamanhoY)));
		m_retangulos[0].setFillColor(m_cor);
		m_retangulos[0].setOutlineThickness(m_bordaGrossura);
		m_retangulos[0].setOutlineColor(m_corDaBorda);
	}
	//Quinas em circulo e sem bordas
	else if (m_raioDaQuina > 0 && m_bordaGrossura == 0) {
		m_retangulos.resize(3);
		m_quinas.resize(4);

		//Definindo raio e cor das quinas
		for (int i = 0; i < 4; i++) {
			m_quinas[i].setRadius(m_raioDaQuina);
			m_quinas[i].setFillColor(m_cor);

		}

		m_retangulos[0].setSize(sf::Vector2f(m_TamanhoX - 2 * m_raioDaQuina, m_TamanhoY));
		for (int i = 1; i < 3; i++)
			m_retangulos[i].setSize(sf::Vector2f(m_raioDaQuina, m_TamanhoY - 2 * m_raioDaQuina));
		//Cores

		for (int i = 0; i < 3; i++)
			m_retangulos[i].setFillColor(m_cor);
	}
	//Quinas em circulo e com bordas
	else if (m_raioDaQuina > 0 && m_bordaGrossura > 0) {
		m_retangulos.resize(7);
		m_quinas.resize(4);

		//Definindo raio cor e bordas das quinas
		for (int i = 0; i < 4; i++) {
			m_quinas[i].setRadius(m_raioDaQuina);
			m_quinas[i].setFillColor(m_cor);
			m_quinas[i].setOutlineThickness(m_bordaGrossura);
			m_quinas[i].setOutlineColor(m_corDaBorda);
		}


		//Setando tamanho e cor das bordas e retangulos interiores
		for (int i = 0; i < 7; i++) {
			if (i == 0)
				m_retangulos[i].setSize(sf::Vector2f(m_TamanhoX - 2 * m_raioDaQuina, m_TamanhoY));
			else if (i == 1 || i == 2)
				m_retangulos[i].setSize(sf::Vector2f(m_raioDaQuina, m_TamanhoY - 2 * m_raioDaQuina));
			else if (i == 3 || i == 5)
				m_retangulos[i].setSize(sf::Vector2f(m_TamanhoX - 2 * m_raioDaQuina, m_bordaGrossura));
			else if (i == 4 || i == 6)
				m_retangulos[i].setSize(sf::Vector2f(m_bordaGrossura, m_TamanhoY - 2 * m_raioDaQuina));

			if (i < 3)
				m_retangulos[i].setFillColor(m_cor);
			else
				m_retangulos[i].setFillColor(m_corDaBorda);
		}

	}

	PosicionarRet();
}





void GuiBase::setColor(sf::Color cor, sf::Color corDaBorda) {
	m_cor = cor;
	m_corDaBorda = cor;

	if (m_raioDaQuina == 0 && m_bordaGrossura == 0) {
		m_retangulos[0].setFillColor(m_cor);
	}
	//Quinas retas e com bordas
	else if (m_raioDaQuina == 0 && m_bordaGrossura > 0) {
		m_retangulos[0].setFillColor(m_cor);
		m_retangulos[0].setOutlineColor(m_corDaBorda);
	}
	//Quinas em circulo e sem bordas
	else if (m_raioDaQuina > 0 && m_bordaGrossura == 0) {

		//Definindo cor das quinas
		for (int i = 0; i < 4; i++) {
			m_quinas[i].setFillColor(m_cor);
		}

		for (int i = 1; i < 3; i++)
			m_retangulos[i].setFillColor(m_cor);
	}

	//Quinas em circulo e com bordas
	else if (m_raioDaQuina > 0 && m_bordaGrossura > 0) {

		//Definindo cor e bordas das quinas
		for (int i = 0; i < 4; i++) {
			m_quinas[i].setFillColor(m_cor);
			m_quinas[i].setOutlineColor(m_corDaBorda);
		}

		//Setando tamanho e cor das bordas e retangulos interiores
		for (int i = 0; i < 7; i++) {
			if (i < 3)
				m_retangulos[i].setFillColor(m_cor);
			else
				m_retangulos[i].setFillColor(m_corDaBorda);
		}

	}
}

GuiBase::GuiBase(sf::Color cor, sf::Color corDaBorda, float x, float y, float TamanhoX, float TamanhoY,
				float borda, float quina): m_PosicaoX(x), m_PosicaoY(y), m_TamanhoX(TamanhoX), m_TamanhoY(TamanhoY), 
	m_cor(cor), m_corDaBorda(corDaBorda), m_bordaGrossura(borda), m_raioDaQuina(quina)
{
}


GuiBase::GuiBase(float x, float y, float tamanhoX, float tamanhoY)
	:m_PosicaoX(x), m_PosicaoY(y), m_TamanhoX(tamanhoX), m_TamanhoY(tamanhoY)
{
}
GuiBase::GuiBase()
{
}


GuiBase::~GuiBase()
{
}
