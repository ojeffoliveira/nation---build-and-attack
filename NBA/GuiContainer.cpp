#include "stdafx.h"
#include "GuiContainer.h"
#include "GuiBotao.h"
#include "GuiImagem.h"
#include "GuiLabel.h"

//
//sf::VertexArray GuiContainer::QuinaSuperiorEsquerda(float x, float y) {
//	
//	sf::Vector2f corte(0, 0);
//	sf::Vector2f &raio = m_quinaSuperiorEsquerda_;
//	sf::Vector2f pos(x, y);
//	
//	sf::VertexArray retornar(sf::LineStrip);
//
//	if (m_bordaGrossura == 0) {
//
//		//retornar.resize((raio.y - corte.y - (raio.y - (raio.y)*sin(acos(corte.x / raio.x)))) * 4 + ((corte.x == 0) ? 0 : -1));
//		int index = 0;
//
//		for (int i = 0; i < (raio.y - corte.y); i++) {
//
//			float largura = (raio.y) * cos(asin((raio.y - 0.5 - i) / raio.y));
//			largura = (largura * raio.x) / raio.y;
//
//			if ((largura - corte.x) <= 0)
//				continue;
//			
//
//			else {
//				retornar.resize(index + 4);
//				retornar[index] = (sf::Vertex(sf::Vector2f(pos.x + raio.x - largura,
//					pos.y + raio.y - (raio.y - 0.5 - i)), sf::Color::Transparent));
//
//				retornar[index + 1] = (sf::Vertex(sf::Vector2f(pos.x + raio.x - largura,
//					pos.y + raio.y - (raio.y - 0.5 - i)), m_cor));
//
//				retornar[index + 2] = (sf::Vertex(sf::Vector2f(pos.x + raio.x - corte.x,
//					pos.y + raio.y - (raio.y - 0.5 - i)), m_cor));
//
//				retornar[index + 3] = (sf::Vertex(sf::Vector2f(pos.x + raio.x - corte.x,
//					pos.y + raio.y - (raio.y - 0.5 - i)), sf::Color::Transparent));
//
//				index += 4;
//			}
//		}
//
//	}
//
//	else {
//		float raioMaior = raio.y + m_bordaGrossura;
//		int index = 0;
//		float limiteDaBorda = raioMaior - (raio.y) * sin(acos(corte.x / raio.x));
//		//retornar.resize(((raioMaior - limiteDaBorda - corte.y) * 6) + ((m_bordaGrossura * 4)));
//
//
//		for (int i = 0; i < (raioMaior - corte.y); i++) {
//
//
//			float larguraBorda = (raioMaior)* cos(asin((raioMaior - 0.5 - i) / raioMaior));
//			larguraBorda = (larguraBorda * (raio.x + m_bordaGrossura)) / (raio.y + m_bordaGrossura);
//
//			if ((larguraBorda - corte.x) < 0)
//				continue;
//
//			if (i >= limiteDaBorda) {
//				retornar.resize(index + 6);
//				float largura = (raio.y) * cos(asin((raio.y - 0.5 - (i - m_bordaGrossura)) / raio.y));
//				largura = (largura * raio.x) / raio.y;
//
//				retornar[index] = (sf::Vertex(sf::Vector2f(pos.x + raio.x - larguraBorda, pos.y + raio.y -
//					(raioMaior - 0.5 - i)), sf::Color::Transparent));
//
//				retornar[index + 1] = (sf::Vertex(sf::Vector2f(pos.x + raio.x - larguraBorda, pos.y + raio.y -
//					(raioMaior - 0.5 - i)), m_corDaBorda));
//
//				retornar[index + 2] = (sf::Vertex(sf::Vector2f((pos.x + raio.x - largura),
//					pos.y + raio.y - (raioMaior - 0.5 - i)), m_corDaBorda));
//
//				retornar[index + 3] = (sf::Vertex(sf::Vector2f((pos.x + raio.x - largura),
//					pos.y + raio.y - (raioMaior - 0.5 - i)), m_cor));
//
//				retornar[index + 4] = (sf::Vertex(sf::Vector2f((pos.x + raio.x - corte.x),
//					pos.y + raio.y - (raioMaior - 0.5 - i)), m_cor));
//
//				retornar[index + 5] = (sf::Vertex(sf::Vector2f(pos.x + raio.x - corte.x,
//					pos.y + raio.y - (raioMaior - 0.5 - i)), sf::Color::Transparent));
//
//				index += 6;
//
//			}
//			else {
//				retornar.resize(index + 4);
//
//				retornar[index] = (sf::Vertex(sf::Vector2f(pos.x + raio.x - larguraBorda, pos.y + raio.y -
//					(raioMaior - 0.5 - i)), sf::Color::Transparent));
//
//				retornar[index + 1] = (sf::Vertex(sf::Vector2f(pos.x + raio.x - larguraBorda, pos.y + raio.y -
//					(raioMaior - 0.5 - i)), m_corDaBorda));
//
//				retornar[index + 2] = (sf::Vertex(sf::Vector2f((pos.x + raio.x - corte.x), pos.y + raio.y -
//					(raioMaior - 0.5 - i)), m_corDaBorda));
//
//				retornar[index + 3] = (sf::Vertex(sf::Vector2f(pos.x + raio.x - corte.x, pos.y + raio.y -
//					(raioMaior - 0.5 - i)), sf::Color::Transparent));
//
//				index += 4;
//			}
//		}
//	}
//
//	return retornar; 
//}
//
////
////sf::VertexArray GuiContainer::QuinaSuperiorEsquerda(float x, float y) {
////	sf::VertexArray retornar(sf::LineStrip);
////	float &raio = m_quinaSuperiorEsquerda;
////	if (m_bordaGrossura == 0) {
////		retornar.resize(raio * 4);
////		for (int i = 0; i < raio; i++) {
////			float largura = (raio)*cos(asin((raio - 0.5 - i) / raio));
////
////			{
////				retornar[i * 4] = (sf::Vertex(sf::Vector2f(x + raio - largura, y + raio - (raio - 0.5 - i)), sf::Color::Transparent));
////				retornar[i * 4 + 1] = (sf::Vertex(sf::Vector2f(x + raio - largura, y + raio - (raio - 0.5 - i)), m_cor));
////				retornar[i * 4 + 2] = (sf::Vertex(sf::Vector2f(x + raio, y + raio - (raio - 0.5 - i)), m_cor));
////				retornar[i * 4 + 3] = (sf::Vertex(sf::Vector2f(x + raio, y + raio - (raio - 0.5 - i)), sf::Color::Transparent));
////			}
////			
////		}
////
////	}
////
////	else {
////		retornar.resize((raio * 6) + (m_bordaGrossura * 4));
////		float raioMaior = raio + m_bordaGrossura;
////
////		for (int i = 0; i < raioMaior; i++) {
////
////			float larguraBorda = (raioMaior)*cos(asin((raioMaior - 0.5 - i) / raioMaior));
////
////			if (i >= m_bordaGrossura) {
////
////				int aux = i - m_bordaGrossura;
////				int ultimoPonto = (m_bordaGrossura * 4) + (i - m_bordaGrossura) * 6 - 1;
////				float largura = (raio)*cos(asin((raio - 0.5 - (i - m_bordaGrossura)) / raio));
////				{
////
////					retornar[ultimoPonto + 1] = (sf::Vertex(sf::Vector2f(x + raio - larguraBorda, y + raio -
////						(raioMaior - 0.5 - i)), sf::Color::Transparent));
////					retornar[ultimoPonto + 2] = (sf::Vertex(sf::Vector2f(x + raio - larguraBorda, y + raio -
////						(raioMaior - 0.5 - i)), m_corDaBorda));
////					retornar[ultimoPonto + 3] = (sf::Vertex(sf::Vector2f(x + raio - largura, y + raio - (raioMaior - 0.5 - i)), m_corDaBorda));
////					retornar[ultimoPonto + 4] = (sf::Vertex(sf::Vector2f(x + raio - largura, y + raio - (raioMaior - 0.5 - i)), m_cor));
////					retornar[ultimoPonto + 5] = (sf::Vertex(sf::Vector2f(x + raio, y + raio - (raioMaior - 0.5 - i)), m_cor));
////					retornar[ultimoPonto + 6] = (sf::Vertex(sf::Vector2f(x + raio, y + raio - (raioMaior - 0.5 - i)), sf::Color::Transparent));
////				}
////				
////			}
////			else {
////
////				
////					retornar[i * 4] = (sf::Vertex(sf::Vector2f(x + raio - larguraBorda, y + raio -
////						(raioMaior - 0.5 - i)), sf::Color::Transparent));
////					retornar[i * 4 + 1] = (sf::Vertex(sf::Vector2f(x + raio - larguraBorda, y + raio -
////						(raioMaior - 0.5 - i)), m_corDaBorda));
////					retornar[i * 4 + 2] = (sf::Vertex(sf::Vector2f(x + raio, y + raio -
////						(raioMaior - 0.5 - i)), m_corDaBorda));
////					retornar[i * 4 + 3] = (sf::Vertex(sf::Vector2f(x + raio, y + raio -
////						(raioMaior - 0.5 - i)), sf::Color::Transparent));
////			}
////		}
////
////
////	}
////	return retornar;
////}
//
//sf::VertexArray GuiContainer::QuinaSuperiorDireita(float x, float y) {
//
//
//	//float pixel = 1.0f / zoom;
//	sf::Vector2f &raio = m_quinaSuperiorDireita_;
//	sf::Vector2f corte(0, 0);
//	sf::Vector2f pos(x, y);
//	
//	sf::VertexArray retornar(sf::LineStrip);
//
//	if (m_bordaGrossura == 0) {
//		//(raio.y - corte.y - (raio.y - (raio.y) * sin(acos(corte.x / raio.x)))) * 4 + (corte.x == 0 ? 0 : 1);
//
//		int index = 0;
//
//		for (int i = 0; i < (raio.y - corte.y); i++) {
//
//			float largura = (raio.y) * cos(asin((raio.y - 0.5 - i) / raio.y));
//			largura = (largura * raio.x) / raio.y;
//
//			if ((largura - corte.x) < 0)
//				continue;
//
//			else {
//				retornar.resize(index + 4);
//
//				retornar[index] = (sf::Vertex(sf::Vector2f(pos.x + corte.x,
//					pos.y + raio.y - (raio.y - 0.5 - i)), sf::Color::Transparent));
//
//				retornar[index + 1] = (sf::Vertex(sf::Vector2f(pos.x + corte.x,
//					pos.y + raio.y - (raio.y - 0.5 - i)), m_cor));
//
//				retornar[index + 2] = (sf::Vertex(sf::Vector2f(pos.x + largura,
//					pos.y + raio.y - (raio.y - 0.5 - i)), m_cor));
//
//				retornar[index + 3] = (sf::Vertex(sf::Vector2f(pos.x + largura,
//					pos.y + raio.y - (raio.y - 0.5 - i)), sf::Color::Transparent));
//
//				index += 4;
//			}
//		}
//
//
//
//	}
//
//	else {
//		float raioMaior = raio.y + m_bordaGrossura;
//		int index = 0;
//		float limiteDaBorda = raioMaior - (raio.y) * sin(acos(corte.x / raio.x));
//		//retornar.resize(((raioMaior - limiteDaBorda - corte.y) * 6) + ((m_bordaGrossura * 4)));
//
//		for (int i = 0; i < (raioMaior - corte.y); i++) {
//
//			float larguraBorda = (raioMaior)* cos(asin((raioMaior - 0.5 - i) / raioMaior));
//			larguraBorda = (larguraBorda * (raio.x + m_bordaGrossura)) / (raio.y + m_bordaGrossura);
//
//			if ((larguraBorda - corte.x) < 0)
//				continue;
//
//			if (i >= limiteDaBorda) {
//
//				retornar.resize(index + 6);
//
//				float largura = (raio.y) * cos(asin((raio.y - 0.5 - (i - m_bordaGrossura)) / raio.y));
//				largura = (largura * raio.x) / raio.y;
//
//				retornar[index] = (sf::Vertex(sf::Vector2f(pos.x + larguraBorda, pos.y + raio.y -
//					(raioMaior - 0.5 - i)), sf::Color::Transparent));
//
//				retornar[index + 1] = (sf::Vertex(sf::Vector2f(pos.x + larguraBorda, pos.y + raio.y -
//					(raioMaior - 0.5 - i)), m_corDaBorda));
//
//				retornar[index + 2] = (sf::Vertex(sf::Vector2f((pos.x + largura),
//					pos.y + raio.y - (raioMaior - 0.5 - i)), m_corDaBorda));
//
//				retornar[index + 3] = (sf::Vertex(sf::Vector2f((pos.x + largura),
//					pos.y + raio.y - (raioMaior - 0.5 - i)), m_cor));
//
//				retornar[index + 4] = (sf::Vertex(sf::Vector2f((pos.x + corte.x),
//					pos.y + raio.y - (raioMaior - 0.5 - i)), m_cor));
//
//				retornar[index + 5] = (sf::Vertex(sf::Vector2f((pos.x + corte.x),
//					pos.y + raio.y - (raioMaior - 0.5 - i)), sf::Color::Transparent));
//
//				index += 6;
//
//			}
//			else {
//				retornar.resize(index + 4);
//
//				retornar[index] = (sf::Vertex(sf::Vector2f(pos.x + corte.x, pos.y + raio.y -
//					(raioMaior - 0.5 - i)), sf::Color::Transparent));
//
//				retornar[index + 1] = (sf::Vertex(sf::Vector2f(pos.x + corte.x, pos.y + raio.y -
//					(raioMaior - 0.5 - i)), m_corDaBorda));
//
//				retornar[index + 2] = (sf::Vertex(sf::Vector2f((pos.x + larguraBorda), pos.y + raio.y -
//					(raioMaior - 0.5 - i)), m_corDaBorda));
//
//				retornar[index + 3] = (sf::Vertex(sf::Vector2f(pos.x + larguraBorda, pos.y + raio.y -
//					(raioMaior - 0.5 - i)), sf::Color::Transparent));
//
//				index += 4;
//			}
//
//		}
//	}
//
//	return retornar; 
//	
//}
//
////
////sf::VertexArray GuiContainer::QuinaSuperiorDireita(float x, float y) {
////	sf::VertexArray retornar(sf::LineStrip);
////	float raio = m_quinaSuperiorDireita;
////	if (m_bordaGrossura == 0) {
////		retornar.resize(raio * 4);
////		for (int i = 0; i < raio; i++) {
////			float largura = (raio)*cos(asin((raio - 0.5 - i) / raio));
////			{
////				retornar[i * 4] = (sf::Vertex(sf::Vector2f(x, y + raio - (raio - 0.5 - i)), sf::Color::Transparent));
////				retornar[i * 4 + 1] = (sf::Vertex(sf::Vector2f(x, y + raio - (raio - 0.5 - i)), m_cor));
////				retornar[i * 4 + 2] = (sf::Vertex(sf::Vector2f(x + largura, y + raio - (raio - 0.5 - i)), m_cor));
////				retornar[i * 4 + 3] = (sf::Vertex(sf::Vector2f(x + largura, y + raio - (raio - 0.5 - i)), sf::Color::Transparent));
////			}
////			
////		}
////	}
////
////	else {
////		retornar.resize((raio * 6) + (m_bordaGrossura * 4));
////		float raioMaior = raio + m_bordaGrossura;
////
////		for (int i = 0; i < raioMaior; i++) {
////
////			float larguraBorda = (raioMaior)*cos(asin((raioMaior - 0.5 - i) / raioMaior));
////
////			if (i >= m_bordaGrossura) {
////
////				int aux = i - m_bordaGrossura;
////				int ultimoPonto = (m_bordaGrossura * 4) + (i - m_bordaGrossura) * 6 - 1;
////				float largura = (raio)*cos(asin((raio - 0.5 - (i - m_bordaGrossura)) / raio));
////				{
////
////					retornar[ultimoPonto + 1] = (sf::Vertex(sf::Vector2f(x + larguraBorda, y + raio -
////						(raioMaior - 0.5 - i)), sf::Color::Transparent));
////					retornar[ultimoPonto + 2] = (sf::Vertex(sf::Vector2f(x + larguraBorda, y + raio -
////						(raioMaior - 0.5 - i)), m_corDaBorda));
////					retornar[ultimoPonto + 3] = (sf::Vertex(sf::Vector2f(x + largura, y + raio - (raioMaior - 0.5 - i)), m_corDaBorda));
////					retornar[ultimoPonto + 4] = (sf::Vertex(sf::Vector2f(x + largura, y + raio - (raioMaior - 0.5 - i)), m_cor));
////					retornar[ultimoPonto + 5] = (sf::Vertex(sf::Vector2f(x, y + raio - (raioMaior - 0.5 - i)), m_cor));
////					retornar[ultimoPonto + 6] = (sf::Vertex(sf::Vector2f(x, y + raio - (raioMaior - 0.5 - i)), sf::Color::Transparent));
////
////				}
////
////			}
////			else {
////				retornar[i * 4] = (sf::Vertex(sf::Vector2f(x, y + raio -
////					(raioMaior - 0.5 - i)), sf::Color::Transparent));
////				retornar[i * 4 + 1] = (sf::Vertex(sf::Vector2f(x, y + raio -
////					(raioMaior - 0.5 - i)), m_corDaBorda));
////				retornar[i * 4 + 2] = (sf::Vertex(sf::Vector2f(x + larguraBorda, y + raio -
////					(raioMaior - 0.5 - i)), m_corDaBorda));
////				retornar[i * 4 + 3] = (sf::Vertex(sf::Vector2f(x + larguraBorda, y + raio -
////					(raioMaior - 0.5 - i)), sf::Color::Transparent));
////			}
////
////		}
////
////	}
////
////	return retornar;
////}
//
//sf::VertexArray GuiContainer::QuinaInferiorEsquerda(float x, float y) {
//
//	sf::Vector2f& raio = m_quinaInferiorEsquerda_;
//	sf::Vector2f corte(0, 0);
//	sf::Vector2f pos(x, y);
//
//	sf::VertexArray retornar(sf::LineStrip);
//
//	if (m_bordaGrossura == 0) {
//
//		//retornar.resize((raio.y - corte.y - (raio.y - (raio.y) * sin(acos(corte.x / raio.x)))) * 4 + ((corte.x == 0) ? 0 : 2));
//		int index = 0;
//
//
//		for (int i = 0; i < (raio.y - corte.y); i++) {
//
//			float largura = (raio.y) * cos(asin((raio.y - 0.5 - i) / raio.y));
//			largura = (largura * raio.x) / raio.y;
//
//			if ((largura - corte.x) <= 0)
//				continue;
//
//			else {
//				retornar.resize(index + 4);
//				retornar[index] = (sf::Vertex(sf::Vector2f(pos.x + raio.x - largura,
//					pos.y + (raio.y - 0.5 - i)), sf::Color::Transparent));
//
//				retornar[index + 1] = (sf::Vertex(sf::Vector2f(pos.x + raio.x - largura,
//					pos.y + (raio.y - 0.5 - i)), m_cor));
//
//				retornar[index + 2] = (sf::Vertex(sf::Vector2f(pos.x + raio.x - corte.x,
//					pos.y + (raio.y - 0.5 - i)), m_cor));
//
//				retornar[index + 3] = (sf::Vertex(sf::Vector2f(pos.x + raio.x - corte.x,
//					pos.y + (raio.y - 0.5 - i)), sf::Color::Transparent));
//
//				index += 4;
//			}
//		}
//
//	}
//
//	else {
//		float raioMaior = raio.y + m_bordaGrossura;
//		int index = 0;
//		float limiteDaBorda = raioMaior - (raio.y) * sin(acos(corte.x / raio.x));
//		//retornar.resize(((raioMaior - limiteDaBorda - corte.y) * 6) + ((m_bordaGrossura * 4)));
//
//
//		for (int i = 0; i < (raioMaior - corte.y); i++) {
//
//
//			float larguraBorda = (raioMaior)* cos(asin((raioMaior - 0.5 - i) / raioMaior));
//			larguraBorda = (larguraBorda * (raio.x + m_bordaGrossura)) / (raio.y + m_bordaGrossura);
//
//			if ((larguraBorda - corte.x) < 0)
//				continue;
//
//			if (i >= limiteDaBorda) {
//				retornar.resize(index + 6);
//				float largura = (raio.y) * cos(asin((raio.y - 0.5 - (i - m_bordaGrossura)) / raio.y));
//				largura = (largura * raio.x) / raio.y;
//
//				retornar[index] = (sf::Vertex(sf::Vector2f(pos.x + raio.x - larguraBorda, pos.y +
//					(raioMaior - 0.5 - i)), sf::Color::Transparent));
//
//				retornar[index + 1] = (sf::Vertex(sf::Vector2f(pos.x + raio.x - larguraBorda, pos.y +
//					(raioMaior - 0.5 - i)), m_corDaBorda));
//
//				retornar[index + 2] = (sf::Vertex(sf::Vector2f((pos.x + raio.x - largura),
//					pos.y + (raioMaior - 0.5 - i)), m_corDaBorda));
//
//				retornar[index + 3] = (sf::Vertex(sf::Vector2f((pos.x + raio.x - largura),
//					pos.y + (raioMaior - 0.5 - i)), m_cor));
//
//				retornar[index + 4] = (sf::Vertex(sf::Vector2f((pos.x + raio.x - corte.x),
//					pos.y + (raioMaior - 0.5 - i)), m_cor));
//
//				retornar[index + 5] = (sf::Vertex(sf::Vector2f(pos.x + raio.x - corte.x,
//					pos.y + (raioMaior - 0.5 - i)), sf::Color::Transparent));
//
//				index += 6;
//
//			}
//			else {
//				retornar.resize(index + 4);
//				retornar[index] = (sf::Vertex(sf::Vector2f(pos.x + raio.x - larguraBorda, pos.y +
//					(raioMaior - 0.5 - i)), sf::Color::Transparent));
//
//				retornar[index + 1] = (sf::Vertex(sf::Vector2f(pos.x + raio.x - larguraBorda, pos.y +
//					(raioMaior - 0.5 - i)), m_corDaBorda));
//
//				retornar[index + 2] = (sf::Vertex(sf::Vector2f((pos.x + raio.x - corte.x), pos.y +
//					(raioMaior - 0.5 - i)), m_corDaBorda));
//
//				retornar[index + 3] = (sf::Vertex(sf::Vector2f(pos.x + raio.x - corte.x, pos.y +
//					(raioMaior - 0.5 - i)), sf::Color::Transparent));
//
//				index += 4;
//			}
//		}
//	}
//
//	return retornar; 
//
//}
//
////sf::VertexArray GuiContainer::QuinaInferiorEsquerda( float x, float y) {
////	sf::VertexArray retornar(sf::LineStrip);
////	float raio = m_quinaInferiorEsquerda;
////
////	if (m_bordaGrossura == 0) {
////		retornar.resize(raio * 4);
////		for (int i = 0; i < raio; i++) {
////			float largura = (raio)*cos(asin((raio - 0.5 - i) / raio));
////			
////			 {
////				retornar[i * 4] = (sf::Vertex(sf::Vector2f(x + raio - largura, y + (raio - 0.5 - i)), sf::Color::Transparent));
////				retornar[i * 4 + 1] = (sf::Vertex(sf::Vector2f(x + raio - largura, y + (raio - 0.5 - i)), m_cor));
////				retornar[i * 4 + 2] = (sf::Vertex(sf::Vector2f(x + raio, y + (raio - 0.5 - i)), m_cor));
////				retornar[i * 4 + 3] = (sf::Vertex(sf::Vector2f(x + raio, y + (raio - 0.5 - i)), sf::Color::Transparent));
////			}
////		}
////
////	}
////
////	else {
////		retornar.resize((raio * 6) + (m_bordaGrossura * 4));
////		float raioMaior = raio + m_bordaGrossura;
////
////		for (int i = 0; i < raioMaior; i++) {
////
////			float larguraBorda = (raioMaior)*cos(asin((raioMaior - 0.5 - i) / raioMaior));
////
////			if (i >= m_bordaGrossura) {
////
////				int aux = i - m_bordaGrossura;
////				int ultimoPonto = (m_bordaGrossura * 4) + (i - m_bordaGrossura) * 6 - 1;
////				float largura = (raio)*cos(asin((raio - 0.5 - (i - m_bordaGrossura)) / raio));
////				 {
////
////					retornar[ultimoPonto + 1] = (sf::Vertex(sf::Vector2f(x + raio - larguraBorda, y +
////						(raioMaior - 0.5 - i)), sf::Color::Transparent));
////					retornar[ultimoPonto + 2] = (sf::Vertex(sf::Vector2f(x + raio - larguraBorda, y +
////						(raioMaior - 0.5 - i)), m_corDaBorda));
////					retornar[ultimoPonto + 3] = (sf::Vertex(sf::Vector2f(x + raio - largura, y + (raioMaior - 0.5 - i)), m_corDaBorda));
////					retornar[ultimoPonto + 4] = (sf::Vertex(sf::Vector2f(x + raio - largura, y + (raioMaior - 0.5 - i)), m_cor));
////					retornar[ultimoPonto + 5] = (sf::Vertex(sf::Vector2f(x + raio, y + (raioMaior - 0.5 - i)), m_cor));
////					retornar[ultimoPonto + 6] = (sf::Vertex(sf::Vector2f(x + raio, y + (raioMaior - 0.5 - i)), sf::Color::Transparent));
////				}
////
////				
////			}
////			else {
////				retornar[i * 4] = (sf::Vertex(sf::Vector2f(x + raio - larguraBorda, y +
////					(raioMaior - 0.5 - i)), sf::Color::Transparent));
////				retornar[i * 4 + 1] = (sf::Vertex(sf::Vector2f(x + raio - larguraBorda, y +
////					(raioMaior - 0.5 - i)), m_corDaBorda));
////				retornar[i * 4 + 2] = (sf::Vertex(sf::Vector2f(x + raio, y +
////					(raioMaior - 0.5 - i)), m_corDaBorda));
////				retornar[i * 4 + 3] = (sf::Vertex(sf::Vector2f(x + raio, y +
////					(raioMaior - 0.5 - i)), sf::Color::Transparent));
////			}
////		}
////
////	}
////
////	return retornar;
////}
//
//sf::VertexArray GuiContainer::QuinaInferiorDireita(float x, float y) {
//	sf::Vector2f& raio = m_quinaInferiorDireita_;
//	sf::Vector2f corte(0, 0);
//	sf::Vector2f pos(x, y);
//
//	sf::VertexArray retornar(sf::LineStrip);
//
//	if (m_bordaGrossura == 0) {
//
//		//retornar.resize((raio.y - corte.y - (raio.y - (raio.y) * sin(acos(corte.x / raio.x)))) * 4 + ((corte.x == 0) ? 0 : 2));
//		int index = 0;
//
//		for (int i = 0; i < (raio.y - corte.y); i++) {
//
//			float largura = (raio.y) * cos(asin((raio.y - 0.5 - i) / raio.y));
//			largura = (largura * raio.x) / raio.y;
//
//			if ((largura - corte.x) < 0)
//				continue;
//
//			else {
//				retornar.resize(index + 4);
//				retornar[index] = (sf::Vertex(sf::Vector2f(pos.x + corte.x,
//					pos.y + (raio.y - 0.5 - i)), sf::Color::Transparent));
//
//				retornar[index + 1] = (sf::Vertex(sf::Vector2f(pos.x + corte.x,
//					pos.y + (raio.y - 0.5 - i)), m_cor));
//
//				retornar[index + 2] = (sf::Vertex(sf::Vector2f(pos.x + largura,
//					pos.y + (raio.y - 0.5 - i)), m_cor));
//
//				retornar[index + 3] = (sf::Vertex(sf::Vector2f(pos.x + largura,
//					pos.y + (raio.y - 0.5 - i)), sf::Color::Transparent));
//
//				index += 4;
//			}
//		}
//
//	}
//
//	else {
//		float raioMaior = raio.y + m_bordaGrossura;
//		int index = 0;
//		float limiteDaBorda = raioMaior - (raio.y) * sin(acos(corte.x / raio.x));
//		//retornar.resize(((raioMaior - limiteDaBorda - corte.y) * 6) + ((m_bordaGrossura * 4)));
//
//		for (int i = 0; i < (raioMaior - corte.y); i++) {
//
//			float larguraBorda = (raioMaior)* cos(asin((raioMaior - 0.5 - i) / raioMaior));
//			larguraBorda = (larguraBorda * (raio.x + m_bordaGrossura)) / (raio.y + m_bordaGrossura);
//
//			if ((larguraBorda - corte.x) < 0)
//				continue;
//
//			if (i >= limiteDaBorda) {
//				retornar.resize(index + 6);
//				float largura = (raio.y) * cos(asin((raio.y - 0.5 - (i - m_bordaGrossura)) / raio.y));
//				largura = (largura * raio.x) / raio.y;
//
//				retornar[index] = (sf::Vertex(sf::Vector2f(pos.x + larguraBorda, pos.y +
//					(raioMaior - 0.5 - i)), sf::Color::Transparent));
//
//				retornar[index + 1] = (sf::Vertex(sf::Vector2f(pos.x + larguraBorda, pos.y +
//					(raioMaior - 0.5 - i)), m_corDaBorda));
//
//				retornar[index + 2] = (sf::Vertex(sf::Vector2f((pos.x + largura),
//					pos.y + (raioMaior - 0.5 - i)), m_corDaBorda));
//
//				retornar[index + 3] = (sf::Vertex(sf::Vector2f((pos.x + largura),
//					pos.y + (raioMaior - 0.5 - i)), m_cor));
//
//				retornar[index + 4] = (sf::Vertex(sf::Vector2f((pos.x + corte.x),
//					pos.y + (raioMaior - 0.5 - i)), m_cor));
//
//				retornar[index + 5] = (sf::Vertex(sf::Vector2f((pos.x + corte.x),
//					pos.y + (raioMaior - 0.5 - i)), sf::Color::Transparent));
//
//				index += 6;
//
//			}
//			else {
//				retornar.resize(index + 4);
//
//				retornar[index] = (sf::Vertex(sf::Vector2f(pos.x + corte.x, pos.y +
//					(raioMaior - 0.5 - i)), sf::Color::Transparent));
//
//				retornar[index + 1] = (sf::Vertex(sf::Vector2f(pos.x + corte.x, pos.y +
//					(raioMaior - 0.5 - i)), m_corDaBorda));
//
//				retornar[index + 2] = (sf::Vertex(sf::Vector2f((pos.x + larguraBorda), pos.y +
//					(raioMaior - 0.5 - i)), m_corDaBorda));
//
//				retornar[index + 3] = (sf::Vertex(sf::Vector2f(pos.x + larguraBorda, pos.y +
//					(raioMaior - 0.5 - i)), sf::Color::Transparent));
//
//				index += 4;
//			}
//
//		}
//	}
//
//	return retornar;
//	
//}


void GuiContainer::updateTransparencia(unsigned char acumulado)
{
	float novo_acumulado = (m_transparencia / 100) * (acumulado / 100);

	m_cor.a = static_cast<sf::Uint8>(255.0 * novo_acumulado);
	m_corDaBorda.a = static_cast<sf::Uint8>(255.0 * novo_acumulado);

	for (int i = 0; i < m_numeroDeMembros; i++) {
		m_membros[i]->updateTransparencia(static_cast<unsigned char>(novo_acumulado*100));
	}
}

//Vers�o chamada no loop para passar o valor de m_transparencia para a cor 
//do container. Depois, chama a outra vers�o sobrecarregada dessa fun��o para 
//fazer o mesmo com os filhos do container inicial. Precisa do updateCor depois
//para que a cor  seja repassada para as partes
void GuiContainer::updateTransparencia()
{
	if (m_pai->m_transparencia > 0)
		m_pai->updateTransparencia();
	else {
		m_cor.a = static_cast<sf::Uint8>(255.0 * static_cast<float>(m_transparencia / 100));
		m_corDaBorda.a = static_cast<sf::Uint8>(255.0 * static_cast<float>(m_transparencia / 100));

		for (int i = 0; i < m_numeroDeMembros; i++) {
			m_membros[i]->updateTransparencia(m_transparencia);
		}
	}

}

//Pega valores do design atual e passam eles para o design atual, reestabelecendo o design
void GuiContainer::updateDesign(const std::string &designDestino)
{
	if(designDestino=="")
		m_design_real.updateDesignReal(*m_design_atual);
	else {
		setDesignAtual(designDestino);
		m_design_real.updateDesignReal(*m_design_atual);
	}
}

//Atualiza a cor das partes. Deve ser a �ltima chamada no loop.
void GuiContainer::updateCor()
{
	//Cor dos retangulos
	int tamanho = m_retangulos.size();
	for (int i = 0; i < tamanho; i++) {
		m_retangulos[i].setFillColor(m_cor);
	}

	bool ha_quinas = false;
	
	if (m_design_atual->m_quinaSuperior>0 || m_design_atual->m_quinaDireita > 0 ||
		m_design_atual->m_quinaInferior> 0 || m_design_atual->m_quinaEsquerda > 0)
		ha_quinas = true;
	
	//Cor das bordas se houverem quinas
	if (ha_quinas) {
		int tamanho = m_bordas.size();
		for (int i = 0; i < tamanho; i++) {
			m_bordas[i].setFillColor(m_corDaBorda);
		}
	}
	else {
		m_retangulos[0].setOutlineColor(m_corDaBorda);
	}
	
	//Cor das quinas
	if (ha_quinas) {
		for (int i = 0; i < m_quinas.size(); i++) {
			m_quinas[i].setCor(m_cor, m_corDaBorda);
		}
	}
}

//Atualiza a transpar�ncia do container. Importante lembrar que 
//a transpar�ncia tamb�m atualiza a transpar�ncia dos filhos.
//Deve passar o

void GuiContainer::setDesignAtual(const std::string& nome)
{
	if (nome != "") {
		int tamanho = m_meus_designs.size();
		for (int i = 0; i < tamanho; i++) {
			if (m_meus_designs[i]->getName() == nome) {
				m_design_atual = m_meus_designs[i];
			}
		}
	}

}

//sf::VertexArray GuiContainer::QuinaInferiorDireita(float x, float y) {
//	sf::VertexArray retornar(sf::LineStrip);
//	float raio = m_quinaInferiorDireita;
//
//	if (m_bordaGrossura == 0) {
//		retornar.resize(raio * 4);
//		for (int i = 0; i < raio; i++) {
//			float largura = (raio)*cos(asin((raio - 0.5 - i) / raio));
//			
//			{
//				retornar[i * 4] = (sf::Vertex(sf::Vector2f(x + largura, y + (raio - 0.5 - i)), sf::Color::Transparent));
//				retornar[i * 4 + 1] = (sf::Vertex(sf::Vector2f(x + largura, y + (raio - 0.5 - i)), m_cor));
//				retornar[i * 4 + 2] = (sf::Vertex(sf::Vector2f(x, y + (raio - 0.5 - i)), m_cor));
//				retornar[i * 4 + 3] = (sf::Vertex(sf::Vector2f(x, y + (raio - 0.5 - i)), sf::Color::Transparent));
//			}
//		}
//
//	}
//
//	else {
//		retornar.resize((raio * 6) + (m_bordaGrossura * 4));
//		float raioMaior = raio + m_bordaGrossura;
//
//		for (int i = 0; i < raioMaior; i++) {
//
//			float larguraBorda = (raioMaior)*cos(asin((raioMaior - 0.5 - i) / raioMaior));
//
//			if (i >= m_bordaGrossura) {
//
//				int aux = i - m_bordaGrossura;
//				int ultimoPonto = (m_bordaGrossura * 4) + (i - m_bordaGrossura) * 6 - 1;
//				float largura = (raio)*cos(asin((raio - 0.5 - (i - m_bordaGrossura)) / raio));
//				 {
//					retornar[ultimoPonto + 1] = (sf::Vertex(sf::Vector2f(x + larguraBorda, y +
//						(raioMaior - 0.5 - i)), sf::Color::Transparent));
//					retornar[ultimoPonto + 2] = (sf::Vertex(sf::Vector2f(x + larguraBorda, y +
//						(raioMaior - 0.5 - i)), m_corDaBorda));
//					retornar[ultimoPonto + 3] = (sf::Vertex(sf::Vector2f(x + largura, y + (raioMaior - 0.5 - i)), m_corDaBorda));
//					retornar[ultimoPonto + 4] = (sf::Vertex(sf::Vector2f(x + largura, y + (raioMaior - 0.5 - i)), m_cor));
//					retornar[ultimoPonto + 5] = (sf::Vertex(sf::Vector2f(x, y + (raioMaior - 0.5 - i)), m_cor));
//					retornar[ultimoPonto + 6] = (sf::Vertex(sf::Vector2f(x, y + (raioMaior - 0.5 - i)), sf::Color::Transparent));
//				}
//			}
//			else {
//				retornar[i * 4] = (sf::Vertex(sf::Vector2f(x + larguraBorda, y +
//					(raioMaior - 0.5 - i)), sf::Color::Transparent));
//				retornar[i * 4 + 1] = (sf::Vertex(sf::Vector2f(x + larguraBorda, y +
//					(raioMaior - 0.5 - i)), m_corDaBorda));
//				retornar[i * 4 + 2] = (sf::Vertex(sf::Vector2f(x, y +
//					(raioMaior - 0.5 - i)), m_corDaBorda));
//				retornar[i * 4 + 3] = (sf::Vertex(sf::Vector2f(x, y +
//					(raioMaior - 0.5 - i)), sf::Color::Transparent));
//			}
//		}
//
//	}
//
//	return retornar;
//}


//Precisa ser mudada
bool GuiContainer::estaDentro(sf::Vector2i posicao) {
	

	//Mudar isso para checar todas as quinas
	if (m_raioDaQuina == 0)
		if (m_PosicaoX < posicao.x && posicao.x < m_PosicaoX + m_TamanhoX
			&& m_PosicaoY < posicao.y && posicao.y < m_PosicaoY + m_TamanhoY)
			return true;

		else {
			if (m_PosicaoX + m_raioDaQuina <= posicao.x && posicao.x <= m_PosicaoX + m_TamanhoX - m_raioDaQuina
				&& m_PosicaoY < posicao.y && posicao.y < m_PosicaoY + m_TamanhoY)
				return true;
			else if (m_PosicaoX < posicao.x && posicao.x <= m_PosicaoX + m_raioDaQuina
				&& m_PosicaoY + m_raioDaQuina <= posicao.y && posicao.y <= m_PosicaoY + m_TamanhoY - m_raioDaQuina)
				return true;
			else if (m_PosicaoX < posicao.x && posicao.x <= m_PosicaoX + m_raioDaQuina
				&& m_PosicaoY + m_raioDaQuina <= posicao.y && posicao.y <= m_PosicaoY + m_TamanhoY - m_raioDaQuina)
				return true;
			else if (m_PosicaoX + m_TamanhoX - m_raioDaQuina <= posicao.x && posicao.x < m_PosicaoX + m_TamanhoX
				&& m_PosicaoY + m_raioDaQuina <= posicao.y && posicao.y <= m_PosicaoY + m_TamanhoY - m_raioDaQuina)
				return true;
			else
				for (int i{ 0 }; i < 2; i++) {
					for (int j{ 0 }; j < 2; j++) {
						if (distanceFrom(static_cast<sf::Vector2f>(posicao),
							sf::Vector2f(m_PosicaoX + m_raioDaQuina + (i*(m_TamanhoX - 2 * m_raioDaQuina)),
								m_PosicaoY + m_raioDaQuina + (j*(m_TamanhoY - 2 * m_raioDaQuina)))) < m_raioDaQuina) {
							return true;
						}
					}
				}
		}
	//Checar se h� irm�os onplace que estariam por cima dele 
	
	//Se a ordem do pai for sobreposta, checar irm�os 
	//Se a ordem do pai n�o for sobreposta, checar o av� e etc

}

float GuiContainer::distanceFrom(sf::Vector2f a, sf::Vector2f b) {
	return sqrtf((pow((b.x - a.x), 2)) + (pow((b.y - a.y), 2)));
}

sf::Color GuiContainer::addColors(const sf::Color &bg, const sf::Color &emcima) {
	sf::Color r;

	r.a = (1 - (1 - (static_cast<float>(emcima.a) / 255)) * (1 - (static_cast<float>(bg.a) / 255))) * 255;
	if (((r.a) / 255) < 1.0e-6) return r; // Fully transparent -- R,G,B not important
	r.r = ((static_cast<float>(emcima.r) / 255) * (static_cast<float>(emcima.a) / 255) / (static_cast<float>(r.a) / 255)
		+ (static_cast<float>(bg.r) / 255) * (static_cast<float>(bg.a) / 255) * (1 - (static_cast<float>(emcima.a) / 255))
		/ (static_cast<float>(r.a) / 255)) * 255;
	r.g = ((static_cast<float>(emcima.g) / 255) * (static_cast<float>(emcima.a) / 255) / (static_cast<float>(r.a) / 255)
		+ (static_cast<float>(bg.g) / 255) * (static_cast<float>(bg.a) / 255) * (1 - (static_cast<float>(emcima.a) / 255))
		/ (static_cast<float>(r.a) / 255)) * 255;
	r.b = ((static_cast<float>(emcima.b) / 255) * (static_cast<float>(emcima.a) / 255) / (static_cast<float>(r.a) / 255)
		+ (static_cast<float>(bg.b) / 255) * (static_cast<float>(bg.a) / 255) * (1 - (static_cast<float>(emcima.a) / 255))
		/ (static_cast<float>(r.a) / 255)) * 255;

	return r;
}

//Criar quinas
void GuiContainer::criarQuinas() {
	sf::Vector2f corte{ 0,0 };
	
	
	/*m_quinas.resize(1,GuiQuina(GuiQuina::vertice::SUPESQ));
	m_quinas.resize(2, GuiQuina(GuiQuina::vertice::SUPDIR));
	m_quinas.resize(3, GuiQuina(GuiQuina::vertice::INFDIR));
	m_quinas.resize(4, GuiQuina(GuiQuina::vertice::INFESQ));*/

	{
		int quinaAtual = 0;
		//Quinas
		//Falta implementar os cortes
		

		if (m_quinaEsquerda > 0 || m_quinaSuperior > 0) {
			sf::Vector2f raio= sf::Vector2f(m_quinaEsquerda, m_quinaSuperior);
			sf::Vector2f pos = sf::Vector2f(m_PosicaoX, m_PosicaoY);

			m_quinas.resize(quinaAtual+1, GuiQuina(GuiQuina::vertice::SUPESQ));
			m_quinas[quinaAtual].setTamanho(raio, corte);
			m_quinas[quinaAtual].setBorda(m_bordaGrossura);
			m_quinas[quinaAtual].setCor(m_cor, m_corDaBorda);
			quinaAtual++;
		}
		if (m_quinaDireita > 0 || m_quinaSuperior >0) {
			sf::Vector2f raio = sf::Vector2f(m_quinaDireita,m_quinaSuperior);
			sf::Vector2f pos = sf::Vector2f(m_PosicaoX + m_TamanhoX - m_quinaDireita, m_PosicaoY);

			m_quinas.resize(quinaAtual + 1, GuiQuina(GuiQuina::vertice::SUPDIR));
			m_quinas[quinaAtual].setTamanho(raio, corte);
			m_quinas[quinaAtual].setBorda(m_bordaGrossura);
			m_quinas[quinaAtual].setCor(m_cor, m_corDaBorda);
			quinaAtual++;
		}
		if (m_quinaDireita > 0 || m_quinaInferior > 0) {
			sf::Vector2f raio = sf::Vector2f(m_quinaDireita, m_quinaInferior);
			sf::Vector2f pos = sf::Vector2f(m_PosicaoX + m_TamanhoX - m_quinaDireita,
				m_PosicaoY + m_TamanhoY - m_quinaInferior);

			m_quinas.resize(quinaAtual + 1, GuiQuina(GuiQuina::vertice::INFDIR));
			m_quinas[quinaAtual].setTamanho(raio, corte);
			m_quinas[quinaAtual].setBorda(m_bordaGrossura);
			m_quinas[quinaAtual].setCor(m_cor, m_corDaBorda);
			quinaAtual++;
		}
		if (m_quinaEsquerda > 0 || m_quinaInferior >0 ) {
			sf::Vector2f raio = sf::Vector2f(m_quinaEsquerda, m_quinaInferior);
			sf::Vector2f pos = sf::Vector2f(m_PosicaoX, m_PosicaoY + m_TamanhoY - m_quinaInferior);

			m_quinas.resize(quinaAtual + 1, GuiQuina(GuiQuina::vertice::INFESQ));
			m_quinas[quinaAtual].setTamanho(raio, corte);
			m_quinas[quinaAtual].setBorda(m_bordaGrossura);
			m_quinas[quinaAtual].setCor(m_cor, m_corDaBorda);
			quinaAtual++;
		}
		

	}


}

void GuiContainer::finalizarUpdate()
{
	for (int i = 0; i < m_quinas.size(); i++) {
		m_quinas[i].update();
	}
}

void GuiContainer::updateTamanho()
{
	//Aqui em cima eu mudo os valores no guicontainer
	
	switch (m_ReacaoTamanhoX) {
		case ReacoesMedidas::FLEXIVEL_PERCENTUAL: //No caso da quina isso representa um percentual do tamanho
			if (m_pai != NULL) {
				m_Tamanho.x = m_pai->m_Tamanho.x * (m_design_real.m_tamanho->x);
			}
			else
				m_Tamanho.x = 0;

			break;

			//N�o tenho certeza se j� soma a margem e os espa�os entre
		case ReacoesMedidas::PEGAR_DOS_FILHOS:
			if (m_numeroDeMembros>0) {
				m_Tamanho.x = m_somaMembrosHorizontal;
			}
			else
				m_Tamanho.x = 0;

			break;
	//Vou criar
	//um somaMembrosFixo_FlexivelHorizontal/Vert
	//e um numeroMembrosHerdeirosHorizontal/Vert
		case ReacoesMedidas::PEGAR_DO_PAI:
			if (m_pai != NULL) {
				//m_Tamanho.x = m_pai->m_Tamanho.x /m_num_herdeiros;
			}
			else
				m_Tamanho.x = 0;

			break;
		default:
			break;
	}





	//QUINAS
	//A cada checada precisa ver se o container n�o est� on-place para 
	//n�o fazer corre��es incorretas
	//S� deve entrar se n�o estiver on place change
	{
		switch (m_ReacaoQuinaSup) {
		case ReacoesMedidas::FLEXIVEL_PERCENTUAL: //No caso da quina isso representa um percentual do tamanho
			m_quinaSuperior = (*(m_design_real.m_quinaSuperior) * m_Tamanho.y);

			break;
		case ReacoesMedidas::PEGAR_DOS_FILHOS:
			if (m_numeroDeMembros > 0) {
				m_quinaSuperior = m_membros[0]->m_quinaSuperior;
			}
			else
				m_quinaSuperior = 0;
			break;
		case ReacoesMedidas::PEGAR_DO_PAI:
			if (m_pai != NULL) {
				m_quinaSuperior = m_pai->m_quinaSuperior;
			}
			else
				m_quinaSuperior = 0;
			break;
		default:
			break;
		}

		switch (m_ReacaoQuinaDir) {
		case ReacoesMedidas::FLEXIVEL_PERCENTUAL: //No caso da quina isso representa um percentual do tamanho
			m_quinaDireita = (*(m_design_real.m_quinaDireita) * m_Tamanho.x);

			break;
		case ReacoesMedidas::PEGAR_DOS_FILHOS:
			if (m_numeroDeMembros > 0) {
				m_quinaDireita = m_membros[0]->m_quinaDireita;
			}
			else
				m_quinaDireita = 0;
			break;
		case ReacoesMedidas::PEGAR_DO_PAI:
			if (m_pai != NULL) {
				m_quinaDireita = m_pai->m_quinaDireita;
			}
			else
				m_quinaDireita = 0;
			break;
		default:
			break;
		}

		switch (m_ReacaoQuinaInf) {
		case ReacoesMedidas::FLEXIVEL_PERCENTUAL: //No caso da quina isso representa um percentual do tamanho
			m_quinaInferior = (*(m_design_real.m_quinaInferior) * m_Tamanho.y);

			break;
		case ReacoesMedidas::PEGAR_DOS_FILHOS:
			if (m_numeroDeMembros > 0) {
				m_quinaInferior = m_membros[0]->m_quinaInferior;
			}
			else
				m_quinaDireita = 0;
			break;
		case ReacoesMedidas::PEGAR_DO_PAI:
			if (m_pai != NULL) {
				m_quinaInferior = m_pai->m_quinaInferior;
			}
			else
				m_quinaInferior = 0;
			break;
		default:
			break;
		}

		switch (m_ReacaoQuinaEsq) {
		case ReacoesMedidas::FLEXIVEL_PERCENTUAL: //No caso da quina isso representa um percentual do tamanho
			m_quinaEsquerda = (*(m_design_real.m_quinaEsquerda) * m_Tamanho.x);

			break;
		case ReacoesMedidas::PEGAR_DOS_FILHOS:
			if (m_numeroDeMembros > 0) {
				m_quinaEsquerda = m_membros[0]->m_quinaEsquerda;
			}
			else
				m_quinaEsquerda = 0;
			break;
		case ReacoesMedidas::PEGAR_DO_PAI:
			if (m_pai != NULL) {
				m_quinaEsquerda = m_pai->m_quinaEsquerda;
			}
			else
				m_quinaEsquerda = 0;
			break;
		default:
			break;
		}
	}

	//TAMANHO

	switch (m_ReacaoTamanhoX) {
	case ReacoesMedidas::FLEXIVEL_PERCENTUAL: //No caso da quina isso representa um percentual do tamanho
		

		break;
	case ReacoesMedidas::PEGAR_DOS_FILHOS:

		break;
	case ReacoesMedidas::PEGAR_DO_PAI:

		break;
	default:
		break;
	}

	if (m_ReacaoTamanhoY == ReacoesMedidas::FIXA) {


	}
	else if (m_ReacaoTamanhoY == ReacoesMedidas::FLEXIVEL_PERCENTUAL) {


	}
	else if (m_ReacaoTamanhoY == ReacoesMedidas::PEGAR_DO_PAI) {


	}
	else if (m_ReacaoTamanhoY == ReacoesMedidas::PEGAR_DOS_FILHOS) {


	}

	//PASSANDO DO CONTAINER PARA AS PARTES
	//Quinas
	if (m_quinas.size() > 0) {
		//Calcula os cortes
		//S�o s� dois, um vertical para superior e 
		//inferior, outro horizontal


		sf::Vector2f corte(0, 0);
		/*m_quinas[0].setTamanho(sf::Vector2f(m_quinaEsquerda, m_quinaSuperior), sf::Vector2f());
		m_quinas[1].setTamanho(sf::Vector2f(m_quinaDireita, m_quinaSuperior), sf::Vector2f());
		m_quinas[2].setTamanho(sf::Vector2f(m_quinaDireita, m_quinaInferior), sf::Vector2f());
		m_quinas[3].setTamanho(sf::Vector2f(m_quinaEsquerda, m_quinaInferior), sf::Vector2f());
		*/
		m_quinas[0].setBorda(m_bordaGrossura);
		m_quinas[1].setBorda(m_bordaGrossura);
		m_quinas[2].setBorda(m_bordaGrossura);
		m_quinas[3].setBorda(m_bordaGrossura);
	}

	//Retangulos
	//Se houve mudan�as no tamanho ou nas quinas, mudar
	//Mudar o m_tamanho ali
	{
		float tamanhoXRetangulo0 = m_TamanhoX;
		float tamanhoYRetangulo0 = m_TamanhoY;

		if (m_quinaEsquerda > 0) {
			tamanhoXRetangulo0 = m_quinaEsquerda;
			tamanhoYRetangulo0 = m_TamanhoY - m_quinaSuperior - m_quinaInferior;
		}
		else if (m_quinaDireita > 0)
			tamanhoXRetangulo0 = m_TamanhoX - m_quinaDireita;

		m_retangulos[0].setSize(sf::Vector2f(tamanhoXRetangulo0, tamanhoYRetangulo0));

		if (m_quinaSuperior == 0 && m_quinaInferior == 0 && m_quinaEsquerda == 0 && m_quinaDireita == 0)
			m_retangulos[0].setOutlineThickness(m_bordaGrossura);
	}
	//Cria o segundo retangulo caso exista
	if (m_quinaDireita > 0 || m_quinaEsquerda > 0) {
		float tamanhoXRetangulo1 = m_TamanhoX - m_quinaEsquerda - m_quinaDireita;
		float tamanhoYRetangulo1 = m_TamanhoY;
		if (m_quinaDireita > 0 && m_quinaEsquerda == 0) {
			tamanhoXRetangulo1 = m_quinaDireita;
			tamanhoYRetangulo1 = m_TamanhoY - m_quinaInferior - m_quinaSuperior;
		}
		m_retangulos[1].setSize(sf::Vector2f(tamanhoXRetangulo1, tamanhoYRetangulo1));
	}
	//Cria o terceiro retangulo caso exista
	if (m_quinaEsquerda > 0 && m_quinaDireita > 0) {
		float tamanhoXRetangulo2 = m_quinaDireita;
		float tamanhoYRetangulo2 = m_TamanhoY - m_quinaInferior - m_quinaSuperior;

		m_retangulos[2].setSize(sf::Vector2f(tamanhoXRetangulo2, tamanhoYRetangulo2));
	}

	//Bordas em caso de mudan�as
	//Se houve mudan�as no tamanho ou nas quinas, mudar
	if (!(m_quinaSuperior == 0 && m_quinaInferior == 0 && m_quinaEsquerda == 0 && m_quinaDireita == 0) && m_bordaGrossura > 0) {
		m_bordas.resize(4);

		{
			float TamanhoX = m_TamanhoX - m_quinaEsquerda - m_quinaDireita
				+ ((m_quinaEsquerda > 0 || m_quinaSuperior > 0) ? 0 : m_bordaGrossura);
			float TamanhoY = m_bordaGrossura;
			m_bordas[0].setSize(sf::Vector2f(TamanhoX, TamanhoY));
		}
		{
			float TamanhoX = m_bordaGrossura;
			float TamanhoY = m_TamanhoY - m_quinaSuperior - m_quinaInferior +
				((m_quinaDireita > 0 || m_quinaSuperior > 0) ? 0 : m_bordaGrossura);
			m_bordas[1].setSize(sf::Vector2f(TamanhoX, TamanhoY));
		}
		{
			float TamanhoX = m_TamanhoX - m_quinaEsquerda - m_quinaDireita +
				((m_quinaDireita > 0 || m_quinaInferior > 0) ? 0 : m_bordaGrossura);
			float TamanhoY = m_bordaGrossura;
			m_bordas[2].setSize(sf::Vector2f(TamanhoX, TamanhoY));

		}
		{
			float TamanhoX = m_bordaGrossura;
			float TamanhoY = m_TamanhoY - m_quinaSuperior - m_quinaInferior +
				((m_quinaEsquerda > 0 || m_quinaInferior > 0) ? 0 : m_bordaGrossura);
			m_bordas[3].setSize(sf::Vector2f(TamanhoX, TamanhoY));
		}
	}
	else {

		m_bordas[3].setSize(sf::Vector2f(0,0));
		m_bordas[2].setSize(sf::Vector2f(0, 0));
		m_bordas[1].setSize(sf::Vector2f(0, 0));
		m_bordas[0].setSize(sf::Vector2f(0, 0));
	}

	

	//Checando pais e filhos e irm�os
		//Atualizar pai caso seja PEGAR_DOS_FILHOS
	if (m_pai->m_ReacaoTamanhoY == ReacoesMedidas::PEGAR_DOS_FILHOS ||
		m_pai->m_ReacaoTamanhoX == ReacoesMedidas::PEGAR_DOS_FILHOS ||
		m_pai->m_ReacaoQuinaSup == ReacoesMedidas::PEGAR_DOS_FILHOS ||
		m_pai->m_ReacaoQuinaDir == ReacoesMedidas::PEGAR_DOS_FILHOS ||
		m_pai->m_ReacaoQuinaInf == ReacoesMedidas::PEGAR_DOS_FILHOS ||
		m_pai->m_ReacaoQuinaEsq == ReacoesMedidas::PEGAR_DOS_FILHOS) {
		
		m_pai->updateTamanho();
	}
	//Checar irm�os que sejam PEGAR_DO_PAI e o pai FIXO ou, FLEXIVEL e o Pai PEGAR_DOS_FILHOS

	if (m_pai->m_ReacaoTamanhoY == ReacoesMedidas::PEGAR_DOS_FILHOS ||
		m_pai->m_ReacaoTamanhoX == ReacoesMedidas::PEGAR_DOS_FILHOS ) {
		//m_pai.checarFilhosFlexiveis();
	}

	////////////////////////////////////////////
	//m_pai.checarFilhosPegarDoPai();

	
	//Atualizar todos os filhos
	for (int i = 0; i < m_numeroDeMembros; i++) {
		m_membros[i]->updateTamanho();
	}

	//Pensar se mantenho isso aqui ou se me responsabilizo por chamar depois
	//Acho que o segundo 
	//chamando o pai para atualizar posi��es
	m_pai->updateMembers();
	//Essa fun��o acima chamar� updatePosicao
}


//Falta posi��o das quinas

//Falta onplace changes
void GuiContainer::updatePosicao() {
	
	
	//Retangulos do corpo
		//Ret 1
	{
		m_retangulos[0].setPosition(sf::Vector2f(m_PosicaoX, m_PosicaoY + m_quinaSuperior));
	}
		//Ret 2
	if(m_quinaEsquerda>0 || m_quinaDireita>0){
		float PosicaoX = m_PosicaoX+m_quinaEsquerda;
		float PosicaoY = m_PosicaoY;

		if (m_quinaEsquerda == 0)
			PosicaoX = m_PosicaoX + m_TamanhoX - m_quinaDireita;

		if (m_quinaEsquerda == 0 && m_quinaDireita != 0)
			PosicaoY = PosicaoY + m_quinaSuperior;

		m_retangulos[1].setPosition(sf::Vector2f(PosicaoX,PosicaoY));
	}
		
		//Ret 3
	if (m_quinaEsquerda > 0 && m_quinaDireita > 0) {
		float PosicaoY = m_PosicaoY;
		if (m_quinaSuperior > 0)
			PosicaoY = PosicaoY + m_quinaSuperior;
		m_retangulos[2].setPosition(sf::Vector2f(m_PosicaoX+m_TamanhoX-m_quinaDireita, PosicaoY));
	}



	//Retangulos da borda caso houver
	if (m_bordaGrossura > 0) {
		//Superior
		m_bordas[0].setPosition(sf::Vector2f(m_PosicaoX + m_quinaEsquerda
																		-((m_quinaEsquerda>0 || m_quinaSuperior > 0)?0 : m_bordaGrossura), m_PosicaoY- m_bordaGrossura));
		//Direita
		m_bordas[1].setPosition(sf::Vector2f(m_PosicaoX + m_TamanhoX, m_PosicaoY + m_quinaSuperior
																		 -((m_quinaDireita > 0 || m_quinaSuperior > 0)? 0 : m_bordaGrossura)));
		//Inferior
		m_bordas[2].setPosition(sf::Vector2f(m_PosicaoX + m_quinaEsquerda, m_PosicaoY + m_TamanhoY));
		//Esquerda
		m_bordas[3].setPosition(sf::Vector2f(m_PosicaoX - m_bordaGrossura, m_PosicaoY + m_quinaSuperior));
	}
		
	if (m_quinas.size() > 0) {

		m_quinas[0].setPos(sf::Vector2f(m_PosicaoX, m_PosicaoY));
		m_quinas[1].setPos(sf::Vector2f(m_PosicaoX + m_TamanhoX - m_quinaDireita, m_PosicaoY));
		m_quinas[2].setPos(sf::Vector2f(m_PosicaoX + m_TamanhoX - m_quinaDireita,
							m_PosicaoY + m_TamanhoY - m_quinaInferior));
		m_quinas[3].setPos(sf::Vector2f(m_PosicaoX, m_PosicaoY + m_TamanhoY - m_quinaInferior));
	}

}

//Cria as partes (retangulo(s) e circulos em casos de borda curva) do container 
// e seta suas cores e bordas

void GuiContainer::criarPartes() {
	
	
	//retangulos
	//Cria o primeiro retangulo
	{
		float tamanhoXRetangulo0 = m_TamanhoX;
		float tamanhoYRetangulo0 = m_TamanhoY;

		if (m_quinaEsquerda > 0) {
			tamanhoXRetangulo0 = m_quinaEsquerda;
			tamanhoYRetangulo0 = m_TamanhoY - m_quinaSuperior - m_quinaInferior;
		}
		else if (m_quinaDireita > 0)
			tamanhoXRetangulo0 = m_TamanhoX - m_quinaDireita;

		m_retangulos.resize(m_retangulos.size()+1, sf::RectangleShape(sf::Vector2f(tamanhoXRetangulo0, tamanhoYRetangulo0)));
		m_retangulos[0].setFillColor(m_cor);

		if (m_quinaSuperior == 0 && m_quinaInferior == 0 && m_quinaEsquerda == 0 && m_quinaDireita == 0) {
			m_retangulos[0].setOutlineThickness(m_bordaGrossura);
			m_retangulos[0].setOutlineColor(m_corDaBorda);
		}

	}
	//Cria o segundo retangulo caso exista
	if (m_quinaDireita > 0 || m_quinaEsquerda > 0) {
		float tamanhoXRetangulo1 = m_TamanhoX - m_quinaEsquerda - m_quinaDireita;
		float tamanhoYRetangulo1 = m_TamanhoY;
		if (m_quinaDireita > 0 && m_quinaEsquerda == 0) {
			tamanhoXRetangulo1 = m_quinaDireita;
			tamanhoYRetangulo1 = m_TamanhoY- m_quinaInferior - m_quinaSuperior;
		}
		m_retangulos.resize(m_retangulos.size() + 1, sf::RectangleShape(sf::Vector2f(tamanhoXRetangulo1, tamanhoYRetangulo1)));
		m_retangulos[1].setFillColor(m_cor);
	}
	//Cria o terceiro retangulo caso exista
	if (m_quinaEsquerda>0 && m_quinaDireita > 0) {
		float tamanhoXRetangulo2 = m_quinaDireita;
		float tamanhoYRetangulo2 = m_TamanhoY- m_quinaInferior - m_quinaSuperior;

		m_retangulos.resize(m_retangulos.size() + 1, sf::RectangleShape(sf::Vector2f(tamanhoXRetangulo2, tamanhoYRetangulo2)));
		m_retangulos[2].setFillColor(m_cor);
	}


	//m_retangulos.resize(3);
	criarQuinas();
	//m_bordas.resize(4);

	//if (!(m_quinaSuperior == 0 && m_quinaInferior == 0 && m_quinaEsquerda == 0 && m_quinaDireita == 0) && m_bordaGrossura > 0) 

	//Bordas
		if (!(m_quinaSuperior == 0 && m_quinaInferior == 0 && m_quinaEsquerda == 0 && m_quinaDireita == 0) && m_bordaGrossura>0) {
			m_bordas.resize(4);			
				
				{
					float TamanhoX = m_TamanhoX - m_quinaEsquerda - m_quinaDireita
									+((m_quinaEsquerda > 0 || m_quinaSuperior > 0)? 0 : m_bordaGrossura);
					float TamanhoY = m_bordaGrossura;
					m_bordas[0].setSize(sf::Vector2f(TamanhoX, TamanhoY));
				}
				{
					float TamanhoX = m_bordaGrossura;
					float TamanhoY = m_TamanhoY - m_quinaSuperior - m_quinaInferior +
									((m_quinaDireita > 0 || m_quinaSuperior > 0) ? 0 : m_bordaGrossura);
					m_bordas[1].setSize(sf::Vector2f(TamanhoX, TamanhoY));
				}
				{
					float TamanhoX = m_TamanhoX - m_quinaEsquerda - m_quinaDireita +
									((m_quinaDireita >0 || m_quinaInferior > 0)?0 : m_bordaGrossura);
					float TamanhoY = m_bordaGrossura;
					m_bordas[2].setSize(sf::Vector2f(TamanhoX, TamanhoY));

				}
				{
					float TamanhoX = m_bordaGrossura; 
					float TamanhoY = m_TamanhoY - m_quinaSuperior - m_quinaInferior +
									((m_quinaEsquerda > 0 || m_quinaInferior >0)? 0 : m_bordaGrossura);
					m_bordas[3].setSize(sf::Vector2f(TamanhoX, TamanhoY));
				}
				m_bordas[3].setFillColor(m_corDaBorda);
				m_bordas[2].setFillColor(m_corDaBorda);
				m_bordas[1].setFillColor(m_corDaBorda);
				m_bordas[0].setFillColor(m_corDaBorda);
		}
	

	updatePosicao();
}

 //DELETAR
void GuiContainer::setColor(sf::Color cor, sf::Color corDaBorda) {
	m_cor = cor;
	m_corDaBorda = cor;

	if (m_raioDaQuina == 0 && m_bordaGrossura == 0) {
		m_retangulos[0].setFillColor(m_cor);
	}
	//Quinas retas e com bordas
	else if (m_raioDaQuina == 0 && m_bordaGrossura > 0) {
		m_retangulos[0].setFillColor(m_cor);
		m_retangulos[0].setOutlineColor(m_corDaBorda);
	}
	//Quinas em circulo e sem bordas
	else if (m_raioDaQuina > 0 && m_bordaGrossura == 0) {

		//Definindo cor das quinas
		for (int i = 0; i < 4; i++) {
			//m_quinas[i].setFillColor(m_cor);
		}

		for (int i = 1; i < 3; i++)
			m_retangulos[i].setFillColor(m_cor);
	}

	//Quinas em circulo e com bordas
	//else if (m_raioDaQuina > 0 && m_bordaGrossura > 0) {

	//	//Definindo cor e bordas das quinas
	//	for (int i = 0; i < 4; i++) {
	//		m_quinas[i].setFillColor(m_cor);
	//		m_quinas[i].setOutlineColor(m_corDaBorda);
	//	}

	//	//Setando tamanho e cor das bordas e retangulos interiores
	//	for (int i = 0; i < 7; i++) {
	//		if (i < 3)
	//			m_retangulos[i].setFillColor(m_cor);
	//		else
	//			m_retangulos[i].setFillColor(m_corDaBorda);
	//	}

	//}
}

void GuiContainer::drawMe(sf::RenderWindow *myWindow)  {
	if (m_corDaBorda.a > 0 || m_cor.a > 0) {
		for (int i = 0; i < m_quinas.size(); i++)
			myWindow->draw(m_quinas[i].getPontos());
	}
	if (m_cor.a > 0) {
		for (int i = 0; i < m_retangulos.size(); i++)
			myWindow->draw(m_retangulos[i]);
	}
	if (m_corDaBorda.a > 0) {
		for (int i = 0; i < m_bordas.size(); i++)
			myWindow->draw(m_bordas[i]);
	}
	
	for (int i = 0; i < m_numeroDeMembros; i++) {
		m_membros[i]->drawMe(myWindow);
	}
	//Falta incluir o pulo nos filhos que estiverem on_place
}

void GuiContainer::updateMe() {
	updateTamanho();
	updatePosicao();
	updateTransparencia();
	updateCor();
	updateMembers();
}

void GuiContainer::updateMyTamanho() {
	//Se ele acabou de ser criado - faz diferen�a se foi criado?
	//Tamanho do container

		//xFixa
		//xFlexivel
	
	
		//xPreencher

		//xInflado
		
		//yFixa
	
		//yFlexivel
		
		//yPreencher
		
		//yInflado


	//Tamanhos das quinas - se o tamanho mudar precisa criar as quinas de novo 

	//Atualiza��o rotineira = passa de gui para sfml 


	

}


//ATUALIZA A POSICAO DA MEMBRO INDEX DE M_MEMBROS COM BASE NA POSICAO DO PAI, OU SEJA
//DA PROPRIA INSTANCIA. ESSA FUNCAO PRESSUPOE QUE OS MEMBROS ANTERIORES NO VECTOR ESTAO
//DEVIDAMENTE POSICIONADOS
void GuiContainer::updatePosicaoDoMembro(int index) {

	if (this->m_ordem == ORDEM_SOBREPOSTA) {
		
		//CASO DE ALINHAMENTO HORIZONTAL A DIREITA OU A ESQUERDA
		if (m_alinhamentoHorizontal != AlinharHorizon::ALINHAR_CENTRAL_HORIZONTAL) {
			m_membros[index]->setX(m_PosicaoX + m_margemHorizontal * (m_alinhamentoHorizontal == 0 ? 1 : -1)
									+ m_TamanhoX * m_alinhamentoHorizontal 
									- m_membros[index]->getTamanhoX()*m_alinhamentoHorizontal);
		}
		
		//CASO DE ALINHAMENTO HORIZONTAL CENTRAL
		else {
			m_membros[index]->setX((m_PosicaoX + (m_TamanhoX / 2)) - (m_membros[index]->getTamanhoX() / 2));
		}

		//CASO DE ALINHAMENTO VERTICAL ACIMA OU ABAIXO
		if (m_alinhamentoVertical != AlinharVert::ALINHAR_CENTRAL_VERTICAL) {
			m_membros[index]->setY(m_PosicaoY + m_margemVertical * (m_alinhamentoVertical == 0 ? 1 : -1)
				+ m_TamanhoY * m_alinhamentoVertical -
				m_membros[index]->getTamanhoY()*(m_alinhamentoVertical == 0 ? 0 : -1));

		}
		//CASO DE ALINHAMENTO VERTICAL CENTRAL
		else {
			m_membros[index]->setY((m_PosicaoY + (m_TamanhoY / 2)) - (m_membros[index]->getTamanhoY() / 2));
		}
		//Termina ORDEM SOBREPOSTA//
	}

	else if (m_ordem == ORDEM_HORIZONTAL && index<m_numeroDeMembros) {
		
		if (m_alinhamentoHorizontal != AlinharHorizon::ALINHAR_CENTRAL_HORIZONTAL) {
			if (index == 0)
				m_membros[index]->setX(m_PosicaoX + m_margemHorizontal *(m_alinhamentoHorizontal==0?1:-1)
									   + m_TamanhoX*m_alinhamentoHorizontal 
									   - m_membros[index]->getTamanhoX()*m_alinhamentoHorizontal);
			else 
				m_membros[index]->setX( (m_membros[index-1]->getX()+
										m_membros[index-1]->getTamanhoX()*(m_alinhamentoHorizontal == 0 ? 1 : 0))
										+ m_espacoEntreMembros * (m_alinhamentoHorizontal == 0 ? 1 : -1)
										- m_membros[index]->getTamanhoX()*m_alinhamentoHorizontal);
		}
		else {
			if (index == 0)
				m_membros[index]->setX((m_PosicaoX + (m_TamanhoX / 2))
					- ((m_somaMembrosHorizontal) / 2));
			else
				m_membros[index]->setX((m_membros[index - 1]->getX()
										+ m_membros[index - 1]->getTamanhoX() 
										+ m_espacoEntreMembros));
		}

		if (m_alinhamentoVertical != AlinharVert::ALINHAR_CENTRAL_VERTICAL) {
			m_membros[index]->setY(m_PosicaoY + m_margemVertical * (m_alinhamentoVertical == 0 ? 1 : -1)
									+ m_TamanhoY * m_alinhamentoVertical
									- m_membros[index]->getTamanhoY()*m_alinhamentoVertical);
		}

		else
			m_membros[index]->setY((m_PosicaoY + (m_TamanhoY / 2)) - (m_membros[index]->getTamanhoY() / 2));

	}
	
	else if (this->m_ordem == ORDEM_VERTICAL && index < m_numeroDeMembros) {

		if (m_alinhamentoHorizontal != AlinharHorizon::ALINHAR_CENTRAL_HORIZONTAL) {
			m_membros[index]->setX(m_PosicaoX + m_margemHorizontal * (m_alinhamentoHorizontal == 0 ? 1 : -1)
				+ m_TamanhoX * m_alinhamentoHorizontal
				- m_membros[index]->getTamanhoX()*m_alinhamentoHorizontal);
		}

		else
			m_membros[index]->setX((m_PosicaoX + (m_TamanhoX / 2)) - (m_membros[index]->getTamanhoX() / 2));

		if (m_alinhamentoVertical != AlinharVert::ALINHAR_CENTRAL_VERTICAL) {
			if (index == 0)
				m_membros[index]->setY(m_PosicaoY + m_margemVertical * (m_alinhamentoVertical == 0 ? 1 : -1)
					+ m_TamanhoY * m_alinhamentoVertical
					- m_membros[index]->getTamanhoY()*m_alinhamentoVertical);
			else
				m_membros[index]->setY((m_membros[index - 1]->getY() +
					m_membros[index - 1]->getTamanhoY()*(m_alinhamentoVertical == 0 ? 1 : 0))
					+ m_espacoEntreMembros * (m_alinhamentoVertical == 0 ? 1 : -1)
					- m_membros[index]->getTamanhoY()*m_alinhamentoVertical);
		}

		else {
			if (index == 0)
				m_membros[index]->setY((m_PosicaoY + (m_TamanhoY / 2))
					- ((m_somaMembrosVertical) / 2));
			else
				m_membros[index]->setY((m_membros[index - 1]->getY()
					+ m_membros[index - 1]->getTamanhoY()
					+ m_espacoEntreMembros));
		}

	}

	m_membros[index]->updatePosicao();
	m_membros[index]->finalizarUpdate();

}

void GuiContainer::updateMembers() {
	for (int i = 0; i < this->m_numeroDeMembros; i++) {
		updatePosicaoDoMembro(i);
		m_membros[i]->updateMembers();
	}
}


								//FUNCOES QUE ADICIONAM COMPONENTES NO CONTAINER
//Fazer template 

void GuiContainer::addImagem(const GuiImagem &NovoMembro) {
	m_membros.resize(static_cast<std::vector<GuiContainer*>::size_type>(m_numeroDeMembros + 1));
	m_numeroDeMembros++;
	m_membros[m_numeroDeMembros - 1] = new GuiImagem(NovoMembro);
	FinalizarAdd(m_numeroDeMembros - 1);
}
//FUNCOES QUE ADICIONAM COMPONENTES NO CONTAINER
void GuiContainer::addLabel(const GuiLabel &NovoMembro) {
	m_membros.resize(static_cast<std::vector<GuiContainer*>::size_type>(m_numeroDeMembros + 1));
	m_numeroDeMembros++;
	m_membros[m_numeroDeMembros - 1] = new GuiLabel(NovoMembro);
	FinalizarAdd(m_numeroDeMembros - 1);
}
//FUNCOES QUE ADICIONAM COMPONENTES NO CONTAINER
void GuiContainer::addBotao(const GuiBotao &NovoMembro) {
	m_membros.resize(static_cast<std::vector<GuiContainer*>::size_type>(m_numeroDeMembros + 1));
	m_numeroDeMembros++;
	m_membros[m_numeroDeMembros - 1] = new GuiBotao(NovoMembro);
	FinalizarAdd(m_numeroDeMembros - 1);
}
//FUNCOES QUE ADICIONAM COMPONENTES NO CONTAINER
void GuiContainer::FinalizarAdd(int indexNovo) {
	if (m_ordem == ORDEM_HORIZONTAL) {
		m_somaMembrosHorizontal += m_membros[indexNovo]->getTamanhoX();
		if (m_numeroDeMembros > 1)
			m_somaMembrosHorizontal += m_espacoEntreMembros;
		if (m_somaMembrosVertical < m_membros[indexNovo]->getTamanhoY())
			m_somaMembrosVertical = m_membros[indexNovo]->getTamanhoY();
	}
	else if (m_ordem == ORDEM_VERTICAL) {
		m_somaMembrosVertical += m_membros[indexNovo]->getTamanhoY();
		if (m_numeroDeMembros > 1)
			m_somaMembrosVertical += m_espacoEntreMembros;
		if (m_somaMembrosHorizontal < m_membros[indexNovo]->getTamanhoX())
			m_somaMembrosHorizontal = m_membros[indexNovo]->getTamanhoX();
	}
	else if (m_ordem == ORDEM_SOBREPOSTA) {
		if (m_somaMembrosVertical < m_membros[indexNovo]->getTamanhoY())
			m_somaMembrosVertical = m_membros[indexNovo]->getTamanhoY();
		if (m_somaMembrosHorizontal < m_membros[indexNovo]->getTamanhoX())
			m_somaMembrosHorizontal = m_membros[indexNovo]->getTamanhoX();
	}
	m_membros[indexNovo]->addPai(this);
	updateMembers();

}
//FUNCAO PARA ADICIONAR UM NOVO CONTAINER
void GuiContainer::addContainer(const GuiContainer &NovoMembro) {
	
	m_membros.resize(static_cast<std::vector<GuiContainer*>::size_type>(m_numeroDeMembros + 1));
	m_numeroDeMembros++;
	m_membros[m_numeroDeMembros - 1] = new GuiContainer(NovoMembro);
	
	FinalizarAdd(m_numeroDeMembros - 1);
}



										//CONSTRUTORES

GuiContainer& GuiContainer::operator= (const GuiContainer& container) 
{
	if (&container == this) {
		return *this;
	}
	if (m_pai) delete m_pai;

	for (int i = 0; i < m_numeroDeMembros; i++) {
		if (m_membros[i]) delete m_membros[i];
	}
	m_membros.resize(container.m_numeroDeMembros);

	for (int i = 0; i < container.m_numeroDeMembros; i++) {
		m_membros[i] = new GuiContainer(*(container.m_membros[i]));
	}
	
	m_pai = NULL;

	m_TamanhoYConst = container.m_TamanhoYConst;
	m_TamanhoXConst = container.m_TamanhoXConst;

	m_espacoEntreMembros = container.m_espacoEntreMembros;
	m_margemVertical = container.m_margemVertical;
	m_margemHorizontal = container.m_margemHorizontal;
	m_somaMembrosHorizontal = container.m_somaMembrosHorizontal;
	m_somaMembrosVertical = container.m_somaMembrosVertical;

	m_alinhamentoHorizontal = container.m_alinhamentoHorizontal;
	m_alinhamentoVertical = container.m_alinhamentoVertical;
	m_ordem = container.m_ordem;
	m_numeroDeMembros = container.m_numeroDeMembros;

	m_retangulos = container.m_retangulos;
	m_quinas = container.m_quinas;
	m_bordas = container.m_bordas;

	m_transparencia = container.m_transparencia;
	m_cor = container.m_cor;
	m_corDaBorda = container.m_corDaBorda;
	m_bordaGrossura = container.m_bordaGrossura;

	m_raioDaQuina = container.m_raioDaQuina;
	
	m_PosicaoX = container.m_PosicaoX;
	m_PosicaoY = container.m_PosicaoY;
	m_TamanhoX = container.m_TamanhoX;
	m_TamanhoY = container.m_TamanhoY;
	
	m_Posicao = container.m_Posicao;
	m_Tamanho = container.m_Tamanho;
	m_quinaSuperior= container.m_quinaSuperior;
	m_quinaEsquerda = container.m_quinaEsquerda;
	m_quinaDireita = container.m_quinaDireita;
	m_quinaInferior = container.m_quinaInferior;

	m_ReacaoTamanhoX = container.m_ReacaoTamanhoX;
	m_ReacaoTamanhoY = container.m_ReacaoTamanhoY;
	m_ReacaoQuinaSup = container.m_ReacaoQuinaSup;
	m_ReacaoQuinaDir = container.m_ReacaoQuinaDir;
	m_ReacaoQuinaEsq = container.m_ReacaoQuinaEsq;
	m_ReacaoQuinaInf = container.m_ReacaoQuinaInf;
	
	return *this;
}

// TODO: inserir instru��o de retorno aqui
GuiContainer* GuiContainer::operator[](const std::string& name)
{
	if (m_numeroDeMembros = 0) {
		return NULL;
	}
	else{
		for (int i = 0; i < m_numeroDeMembros;i++) {
			if (m_membros[i]->m_nome == name) {
				return m_membros[i];
			}
		}
	}
}

////////////////////////////////////CONSTRUTORES

GuiContainer::GuiContainer(const GuiContainer& container)
{
	*this = container;
}

GuiContainer::GuiContainer(sf::Color cor, sf::Color corDaBorda, GuiContainer::Ordenacao ordem,
	float TamanhoX, float TamanhoY,	float borda, float quinaSup, float quinaDir, 
	float quinaInf, float quinaEsq)
	:m_PosicaoX(0), m_PosicaoY(0), m_TamanhoX(TamanhoX), m_TamanhoY(TamanhoY),
	m_cor(cor),m_corDaBorda(corDaBorda), m_ordem(ordem),m_bordaGrossura(borda), m_quinaSuperior(quinaSup),
	m_quinaDireita(quinaDir), m_quinaInferior(quinaInf), m_quinaEsquerda(quinaEsq),m_pai(NULL)
{		
		criarPartes();
		finalizarUpdate();
}
	
GuiContainer::GuiContainer(Ordenacao ordem, std::vector<GuiDesign*> designs): m_meus_designs(designs),
m_ordem(ordem), m_pai(NULL)
{

}

GuiContainer::GuiContainer(sf::Color cor, sf::Color corDaBorda, GuiContainer::Ordenacao ordem, float TamanhoX,
	float TamanhoY, float quina, float borda)
	: m_cor(cor), m_corDaBorda(corDaBorda), m_bordaGrossura(borda), m_TamanhoX(TamanhoX), m_TamanhoY(TamanhoY),
	m_ordem(ordem), m_PosicaoX(0), m_PosicaoY(0), m_quinaSuperior(quina),
	m_quinaDireita(quina), m_quinaInferior(quina), m_quinaEsquerda(quina), m_pai(NULL)
{
	criarPartes();
	finalizarUpdate();
}

////////////////////////FIM DOS CONTRUTORES, abaixo, Fun��es auxiliares a constru��o////////////////////////////

					//AUXILIAR PARA CONSTRU��O - DEFINE VAR RELACIONADAS AOS MEMBROS

void GuiContainer::setVarMembros(float margemVertical, float margemHorizontal,
	GuiContainer::AlinharHorizon alinhamentoHorizontal, GuiContainer::AlinharVert alinhamentoVertical,
	float espacoentre)
{
	m_margemVertical = margemVertical;
	m_margemHorizontal = margemHorizontal;
	m_alinhamentoHorizontal = alinhamentoHorizontal;
	m_alinhamentoVertical = alinhamentoVertical;
	m_espacoEntreMembros = espacoentre;
	updateMembers();
}

					//AUXILIAR PARA CONSTRU��O - DEFINE A REA��O DAS MEDIDAS

void GuiContainer::setVarMedidas(const ReacoesMedidas &tamanhox, const ReacoesMedidas& tamanhoy, const ReacoesMedidas &quinaSup,
	const ReacoesMedidas &quinaDir, const ReacoesMedidas &quinaEsq,
	const ReacoesMedidas &quinaInf)
{
	//m_tamanho = tamanho;
	m_ReacaoQuinaSup = quinaSup;
	m_ReacaoQuinaDir = quinaDir;
	m_ReacaoQuinaEsq = quinaEsq;
	m_ReacaoQuinaInf = quinaInf;
	updateTamanho();
	updatePosicao();
	updateMembers();
}


GuiContainer::GuiContainer()
{
}


GuiContainer::~GuiContainer()
{
}
