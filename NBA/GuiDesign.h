#pragma once
class GuiDesign
{
protected:
	std::string m_design_nome = "";

	sf::Vector2f *m_tamanho;
	
	float *m_quinaSuperior;
	float *m_quinaEsquerda;
	float *m_quinaDireita;
	float *m_quinaInferior;


	//Deixar de utilizar os abaixo
	sf::Vector2f m_quinaSuperiorEsquerda_;
	sf::Vector2f m_quinaSuperiorDireita_;
	sf::Vector2f m_quinaInferiorEsquerda_;
	sf::Vector2f m_quinaInferiorDireita_;
	
	float *m_bordaGrossura = 0;
	
	sf::Color *m_corDaBorda = new sf::Color(sf::Color::Transparent);
	sf::Color *m_cor = new sf::Color(sf::Color::Transparent);
	//Fazer dois arrays para guardar os devios dessas cores?

	// o m_transparencia se aplica a transparencia da cor
	//e da cordaborda dos filhos tbm
	//varia entre 0 e 100
	unsigned char *m_transparencia = new unsigned char(100);
	
	float *m_espacoEntreMembros = 0;
	
	//A Margem colocada acima ou abaixo quando o
	//o alinhamento vertical n�o � central
	float *m_margemVertical = 0;

	//A Margem colocada a direita ou a esquerda quando 
	//o alinhamento horizontal n�o � central
	float *m_margemHorizontal = 0;

public:


	void updateDesignReal(const GuiDesign& d);

	GuiDesign& operator= (const GuiDesign& d);

	std::string getName() { return m_design_nome; }


	GuiDesign(const std::string &nome, const  sf::Vector2f &Tamanho, const float &QuinaSup, const float &QuinaDir, const float &QuinaInf,
		const float &QuinaEsq, const float &borda, const sf::Color &cor, const sf::Color &corDaBorda,
		const float &espacoEntreMembros, const float &margemVert, const float &margemHoriz);

	
	GuiDesign(const std::string &nome, const sf::Vector2f &Tamanho, const float &quinas, const float &borda, 
		const sf::Color &cor, const sf::Color &corDaBorda, const float &espacoEntreMembros,
		const float &margemVert, const float &margemHoriz);

	
	GuiDesign();
	~GuiDesign();
	friend class GuiContainer;
};

