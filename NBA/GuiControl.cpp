#include "stdafx.h"
#include "GuiControl.h"


GuiControl::GuiControl(const sf::RenderWindow& window, const sf::View& view, bool possuiBackground,
	const sf::Vector2f& tamanhoInicial, const sf::Vector2f& tamanhoMinimo, const std::string& TelaInicial, 
	const sf::Color& corBordaMae, const sf::Color& corMae, float bordaMae):m_janela(window), 
	m_view(view), m_tamanho_inicial(tamanhoInicial), m_tamanho_minimo(tamanhoMinimo),
	m_tela_inicial(TelaInicial), m_mae(GuiContainer())
{
	//window.setFramerateLimit(30);
	
	//window.setIcon();
	

}

GuiControl::GuiControl()
{
}


GuiControl::~GuiControl()
{
}

void GuiControl::rodarInteratividades()
{
	int tamanho = m_intera_rodando.size();

	for (int i = 0; i < tamanho; i++) {
		m_intera_rodando[i]->rodarInteratividade(*this);
		if (!(m_intera_rodando[i]->esta_rodando())){
			//m_intera_rodando.erase(m_intera_rodando[i]);		esta linha precisa deletar a interacao do vector
		}
	}
}

causa GuiControl::convertToCausa(const sf::Event& tipo)
{
	causa retornar;
	switch (tipo.type) {
		case sf::Event::EventType::TextEntered:
			retornar = causa::TEXTO_ESCRITO;
			break;
		case sf::Event::EventType:
			retornar = causa::CLICK_DIREITO;
			break;
		case sf::Event::EventType:
			retornar = causa::CLICK_ESQUERDO;
			break;
		case sf::Event::EventType:
			retornar = causa::CLICK_MEIO;
			break;
		case sf::Event::EventType:
			retornar = causa::KEY_PRESSED;
			break;
		case sf::Event::EventType:
			retornar = causa::KEY_RELEASED;
			break;
		case sf::Event::EventType:
			retornar = causa::MOUSE_SCROLL_DOWN;
			break;
		case sf::Event::EventType:
			retornar = causa::MOUSE_SCROLL_UP;
			break;
		case sf::Event::EventType:
			retornar = causa::PONTEIRO_MOVEU;
			break;
		case sf::Event::EventType:
			retornar = causa::PONTEIRO_NAO_SOBRE;
			break;
		case sf::Event::EventType:
			retornar = causa::PONTEIRO_SOBRE;
			break;
	}

	return retornar;
}

void GuiControl::Loop()
{
	while (janela->isOpen()) {
		sf::Event event;
		while (janela->pollEvent(event)) {

			if (event.type = event.Resized) {

			}
			

			//checarInteratividades(convertToCausa(event);
			//GuiTransicao &trans = checarTrans();
			//rodarTransicao(trans); // isso ta errado a transicao precisa ser feita aqui para manter a interatividade!! 
			//rodarInteratividades();
		}

		//update quem precisa



	}



}
