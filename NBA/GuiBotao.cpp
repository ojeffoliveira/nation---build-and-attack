//////////////////////////////////////////////////////////////
//
// NEXT UP
// IMPORTANTE - criar um novo estado de clicado. Selecionado e clicado 
// s�o estados diferentes. Isso vai totalizar 3 estados para o botao
//
// finalizar os construtores
// 
// definir os estados que existirao - criar funcoes 
// para definilos
//
// Fazer as outras fun��es virtuais
//////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "GuiBotao.h"



void GuiBotao::mudarEstado(GuiComponente::Estado estado) {
	if (m_meuEstado == estado)
		return;
	else {
		m_meuEstado = estado;
		m_transicao = 0;
	}
}
//void GuiBotao::criarComponente(sf::String TextureFilename, sf::String label) {
//	//if (!(TextureFilename.isEmpty()))
//		
//
//	//if (!(label.isEmpty()))
//
//
//}
//void GuiBotao::posicionarComponente() {
//	if (!(m_texto.getString().isEmpty()) && !m_possuiImagem)
//		m_texto.setPosition(sf::Vector2f(((m_PosicaoX + (m_TamanhoX / 2)) - (m_texto.getLocalBounds().width / 2)),
//							(m_PosicaoY + (m_TamanhoY / 2)) - (m_texto.getLocalBounds().height / 2)));
//	else if (!(m_texto.getString().isEmpty()) && m_possuiImagem)
//		;
//	else if((m_texto.getString().isEmpty()) && m_possuiImagem)
//			m_imagem.setPosition(sf::Vector2f((m_PosicaoX + (m_TamanhoX / 2) - (m_imagem.getTexture()->getSize().x) / 2), 
//												(m_PosicaoY + (m_TamanhoY / 2) - (m_imagem.getTexture()->getSize().y) / 2)));
//
//}


//void drawMe(sf::RenderWindow *myWindow);
//void updatePosicao(int index);
//void PosicionarRet();
//void updateMembers();

bool estaDentro(sf::Vector2i);



							//CONSTRUTORES

//ALINHAMENTO AUTOMATICO: AMBOS CENTRAIS
		//COM TEXTO SEM IMAGEM
GuiBotao::GuiBotao(sf::Color cor, sf::Color corDaBorda, sf::Color corTexto, sf::Text texto,
	float TamanhoX, float TamanhoY, float borda, float quina)
	: GuiComponente(cor, corDaBorda, GuiContainer::Ordenacao::ORDEM_SOBREPOSTA,TamanhoX,TamanhoY,borda,quina)
{
	setVarMembros(0, 0, GuiContainer::AlinharHorizon::ALINHAR_CENTRAL_HORIZONTAL,
		GuiContainer::AlinharVert::ALINHAR_CENTRAL_VERTICAL, 0);
	addLabel(GuiLabel(texto.getString(), corTexto, *(texto.getFont()), texto.getCharacterSize()));
}


//COM TEXTO COM IMAGEM, IMAGEM PRIMEIRO
GuiBotao::GuiBotao(sf::Color cor, sf::Color corDaBorda, sf::Color corTexto, sf::String filename,
	sf::Text texto, GuiContainer::Ordenacao ordem, float TamanhoX,
	float TamanhoY, float espacoEntre,
	float borda, float quina)
	: GuiComponente(cor, corDaBorda, ordem, TamanhoX, TamanhoY, borda, quina)
{


}
//COM TEXTO COM IMAGEM, TEXTO PRIMEIRO
GuiBotao::GuiBotao(sf::Color cor, sf::Color corDaBorda, sf::Color corTexto, sf::Text texto,
	sf::String filename, GuiContainer::Ordenacao ordem, float TamanhoX,
	float TamanhoY, float espacoEntre, float borda, float quina)
	: GuiComponente(cor, corDaBorda, ordem, TamanhoX, TamanhoY, borda, quina)
{


}

//SEM TEXTO COM IMAGEM
GuiBotao::GuiBotao(sf::Color cor, sf::Color corDaBorda, sf::String filename,
				float TamanhoX, float TamanhoY, float borda, float quina)
	: GuiComponente(cor, corDaBorda, GuiContainer::Ordenacao::ORDEM_SOBREPOSTA, TamanhoX, TamanhoY, borda, quina)
{
	setVarMembros(0, 0, GuiContainer::AlinharHorizon::ALINHAR_CENTRAL_HORIZONTAL,
		GuiContainer::AlinharVert::ALINHAR_CENTRAL_VERTICAL, 0);
	addImagem(GuiImagem(filename));
}

//DEFINI��O PELO USUARIO DOS ALINHAMENTOS
	//COM TEXTO SEM IMAGEM
GuiBotao::GuiBotao(AlinharHorizon horizontal, AlinharVert Vertical, sf::Color cor, float margemHorizontal,
	float margemVertical, sf::Color corDaBorda, sf::Color corTexto, sf::Text texto,
	float TamanhoX, float TamanhoY, float borda, float quina)
	: GuiComponente(cor, corDaBorda, GuiContainer::Ordenacao::ORDEM_VERTICAL, TamanhoX, TamanhoY, borda, quina)
{
	setVarMembros(margemVertical, margemHorizontal, horizontal, Vertical, 0);
	addLabel(GuiLabel(texto.getString(), corTexto, *(texto.getFont()), TamanhoY));
}

GuiBotao::GuiBotao(AlinharHorizon horizontal, AlinharVert Vertical, float margemHorizontal,
	float margemVertical, sf::Color cor, sf::Color corDaBorda,
	sf::Color corTexto, sf::String filename, sf::Text texto,
	GuiContainer::Ordenacao ordem, float TamanhoX,
	float TamanhoY, float espacoEntre, float borda, float quina)
	: GuiComponente(cor, corDaBorda, ordem, TamanhoX, TamanhoY, borda, quina)
{

}

GuiBotao::GuiBotao(AlinharHorizon horizontal, AlinharVert Vertical, float margemHorizontal,
	float margemVertical, sf::Color cor, sf::Color corDaBorda,
	sf::Color corTexto, sf::Text texto, sf::String filename,
	GuiContainer::Ordenacao ordem, float TamanhoX,
	float TamanhoY, float espacoEntre, float borda, float quina)
	: GuiComponente(cor, corDaBorda, ordem, TamanhoX, TamanhoY, borda, quina)
{

}

GuiBotao::GuiBotao(AlinharHorizon horizontal, AlinharVert Vertical, float margemHorizontal,
	float margemVertical, sf::Color cor, sf::Color corDaBorda, sf::String filename,
	float TamanhoX, float TamanhoY, float borda, float quina)
	: GuiComponente(cor, corDaBorda, GuiContainer::Ordenacao::ORDEM_VERTICAL, TamanhoX, TamanhoY, borda, quina)
{
	setVarMembros(margemVertical, margemHorizontal, horizontal, Vertical, 0);
	addImagem(GuiImagem(filename));
}

GuiBotao::~GuiBotao()
{
}
