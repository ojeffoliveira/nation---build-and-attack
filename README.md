Nation Build and Attack será um jogo de estratégia baseado em turnos. 

Para desenvolvê-lo comecei antes a criar uma framework para minha interface 
gráfica, que, para minha surpresa de novato, se mostrou bem mais trabalhosa 
do que eu esperava.

Em breve vou criar um novo projeto para manter essa framework e para 
possibilitar que outros a utilizem como queiram, e manter este projeto apenas 
para o jogo. 

Aponto que nem o jogo nem a framework se encontram prontas no momento. 

Para mais informações sobre a framework da interface gráfica acesse o arquivo 
Interface Gráfica de Usuário Documentação.
